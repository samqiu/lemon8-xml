<?php
/* SVN FILE: $Id: footer.php,v 1.2 2009/01/23 18:13:18 mj Exp $ */
/**
 * Short description for file.
 *
 * Long description for file
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) Tests <https://trac.cakephp.org/wiki/Developement/TestSuite>
 * Copyright 2005-2008, Cake Software Foundation, Inc. (http://www.cakefoundation.org)
 *
 *  Licensed under The Open Group Test Suite License
 *  Redistributions of files must retain the above copyright notice.
 *
 * @filesource
 * @copyright     Copyright 2005-2008, Cake Software Foundation, Inc. (http://www.cakefoundation.org)
 * @link          https://trac.cakephp.org/wiki/Developement/TestSuite CakePHP(tm) Tests
 * @package       cake
 * @subpackage    cake.cake.tests.lib
 * @since         CakePHP(tm) v 1.2.0.4433
 * @version       $Revision: 1.2 $
 * @modifiedby    $LastChangedBy: gwoo $
 * @lastmodified  $Date: 2009/01/23 18:13:18 $
 * @license       http://www.opensource.org/licenses/opengroup.php The Open Group Test Suite License
 */
?>	</div>
		</div>
 		<div id="footer">
 			<!--PLEASE USE ONE OF THE POWERED BY CAKEPHP LOGO-->
 			<a href="http://www.cakephp.org/" target="_blank">
 				<img src="<?php echo $baseUrl; ?>img/cake.power.gif" alt="CakePHP(tm) :: Rapid Development Framework" /></a></p>
 		</div>
	</div>
</body>
</html>
<?php

/**
 * functions.php
 *
 * Copyright (c) 2008-2009 MJ Suhonos
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Useful application-wide functions (also available to XSL)
 */

	// stuff to detect/extract from author names
	// expressions ported from CiteULike author plugin (in TCL): http://svn.citeulike.org/svn/plugins/author.tcl
	define('REGEX_TITLE', "(?:His (?:Excellency|Honou?r)\s+|Her (?:Excellency|Honou?r)\s+|The Right Honou?rable\s+|The Honou?rable\s+|Right Honou?rable\s+|The Rt\.? Hon\.?\s+|The Hon\.?\s+|Rt\.? Hon\.?\s+|Mr\.?\s+|Ms\.?\s+|M\/s\.?\s+|Mrs\.?\s+|Miss\.?\s+|Dr\.?\s+|Sir\s+|Dame\s+|Prof\.?\s+|Professor\s+|Doctor\s+|Mister\s+|Mme\.?\s+|Mast(?:\.|er)?\s+|Lord\s+|Lady\s+|Madam(?:e)?\s+|Priv\.-Doz\.\s+)+");
	define('REGEX_DEGREES', "(,\s+(?:[A-Z\.]+))+");
	define('REGEX_FORENAME', "(?:[^ \t\n\r\f\v,.]{2,}|[^ \t\n\r\f\v,.;]{2,}\-[^ \t\n\r\f\v,.;]{2,})");
	define('REGEX_INITIALS', "(?:(?:[A-Z]\.\s){1,4})|(?:[A-Z]{1,4}\s)|(?:(?:[A-Z]\.-?){1,4}\s)|(?:(?:[A-Z]-){1,3}[A-Z]\s)|(?:(?:[A-Z]\s){1,4})|(?:(?:[A-Z] ){1,3}[A-Z]\.\s)|(?:[A-Z]-(?:[A-Z]\.){1,3}\s)");
	define('REGEX_PREFIX', "Dell(?:[a|e])?\s|Dalle\s|D[a|e]ll\'\s|Dela\s|Del\s|[Dd]e (?:La |Los )?\s|[Dd]e\s|[Dd][a|i|u]\s|L[a|e|o]\s|[D|L|O]\'|St\.?\s|San\s|[Dd]en\s|[Vv]on\s(?:[Dd]er\s)?|(?:[Ll][ea] )?[Vv]an\s(?:[Dd]e(?:n|r)?\s)?");

//	define('REGEX_EMAIL', "[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]");
//	define('REGEX_PHONE', "((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,5})|(\(?\d{2,6}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}");

	// parse an author name string using regex, return a key-value array of elements
	function parse_author($author, $degrees = false, $title = false) {

		$regexSurname = "(?:".REGEX_PREFIX.")?(?:".REGEX_FORENAME.")";

		if ($title && preg_match('/^('.REGEX_TITLE.')/iu', $author, $results)) {
			$array['title'] = trim($results[1], ',:; ');
			$author = preg_replace('/^('.REGEX_TITLE.')/iu', '', $author);
		}

		if ($degrees && preg_match('/('.REGEX_DEGREES.')$/iu', $author, $results)) {
			$degreesArray = preg_split('/[:;,]/', trim($results[1], ',:; '), -1, PREG_SPLIT_NO_EMPTY );
			$array['degrees'] = implode('; ', $degreesArray);
			$array['degrees'] = strtr($array['degrees'], array('.' => ''));

			$author = preg_replace('/('.REGEX_DEGREES.')$/iu', '', $author);
		}

		if (
			preg_match('/^(?P<forename>'.REGEX_FORENAME.') (?P<initials>'.REGEX_INITIALS.')?(?P<surname>'.$regexSurname.')$/iu', $author, $results) ||
			preg_match('/^(?P<surname>'.$regexSurname.'), (?P<initials>'.REGEX_INITIALS.')(?P<forename>'.REGEX_FORENAME.')$/iu', $author, $results) ||
			preg_match('/^(?P<initials>'.REGEX_INITIALS.')(?P<forename>'.REGEX_FORENAME.') (?P<surname>'.$regexSurname.')$/iu', $author, $results) ||
//			preg_match('/^(?P<forename>'.REGEX_FORENAME.') (?P<forename_2>'.REGEX_FORENAME.') (?P<surname>'.$regexSurname.')$/iu', $author, $results) ||
			preg_match('/^(?P<surname>'.$regexSurname.'), ?(?P<forename>'.REGEX_FORENAME.')(?P<initials> [A-Z]+)?/iu', $author, $results) ||
			preg_match('/^(?P<surname>'.$regexSurname.'),? ?(?P<initials>'.REGEX_INITIALS.')$/iu', $author, $results) ||
			preg_match('/^(?P<initials>'.REGEX_INITIALS.')?(?P<surname>'.$regexSurname.')$/iu', $author, $results) ||
			preg_match('/^(?P<initials>([A-Z]\\\.){1,3})(?P<surname>'.$regexSurname.')$/iu', $author, $results) ||
			preg_match('/^(?P<surname>'.$regexSurname.')$/iu', $author, $results) ||
			preg_match('/(?P<surname>'.$regexSurname.')/iu', $author, $results))
		{
			$array['forename'] = isset($results['forename']) ? trim($results['forename']) : '';
			$array['initials'] = isset($results['initials']) ? trim($results['initials']) : '';
			$array['surname'] = isset($results['surname']) ? trim($results['surname']) : '';

			// check in case initials/surname are reversed
			if ($results['surname'] == strtoupper($results['surname']) && empty($results['initials']) && !empty($results['forename'])) {
				$array['surname'] = $results['forename'];
				$array['forename'] = $results['surname'];
			}

			return $array;
		} else {
			return array('forename' => '', 'initials' => '', 'surname' => '');
		}
	}


	// wrapper to handle URL caching for web services
	// FIXME: getCachedURL should handle socket timeouts cleanly
	function getCachedURL($url, $postOptions = array()) {

		$result = Cache::read(sha1($url . implode($postOptions)));

		if ($result === false) {

			// if we have POST options, use CURL
			if (!empty($postOptions)) {
				// TODO: CURL requirement; disclose or fix/replace
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: text/xml, */*'));
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $postOptions);

				// relax timeout a little bit for slow servers
				curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);

				// make the actual POST call
				$result = curl_exec($ch);
				curl_close($ch);

			} else {
				// contents of URL are not cached, get them dynamically
				ini_set('default_socket_timeout', 120);
				$result = file_get_contents($url);
			}

			// fix string that can cause premature end of cache
			$result = strtr($result, array('";' => '" ;'));
			Cache::write(sha1($url . implode($postOptions)), $result, array("config" => "default", "duration" => "+1 day"));
		}

		return stripslashes($result);
	}

// take a nested array and convert it to an xml string
function array_to_xml($array, $name = "array")
{
	$xml = '';
	foreach($array as $key => $value)
	{
		// numeric elements are not allowed
		if (is_numeric($key)) $key = $name .'_'. $key;
		$xml .= "<$key>";

		if(is_array($value)) {
			$xml .= array_to_xml($value, $key);
		} else {
			$xml .= htmlspecialchars($value);
		}

		$xml .= "</$key>";
	}
	return $xml;
}

// take an XML node and generate a nested array
// (optional) discard empty elements
function xml_to_array($xmlNode, $keepEmpty = true) {

	$array = array();

	foreach ($xmlNode->childNodes as $childNode) {

		if ($childNode->nodeType == 1) {

			if ($childNode->childNodes->length > 1) {
				$array[$childNode->nodeName] = xml_to_array($childNode);
			} elseif ( ($childNode->nodeValue == '' && $keepEmpty) || ($childNode->nodeValue != '') ) {
					$array[$childNode->nodeName] = $childNode->nodeValue;
			}
		}
	}

	return $array;
}

// partially-ported version of the PKP core String::utf8_normalize function
function utf8_normalize($string) {
	// load the phputf8 library
	App::import('Vendor', 'phputf8/utils/ascii');
	App::import('Vendor', 'phputf8/utils/validation');

	if (!utf8_compliant($string)) {
		App::import('Vendor', 'phputf8/utils/bad');

		// replace non-UTF8 characters
		$string = utf8_bad_replace($string);
	}

	// remove ASCII codes which may break XML parsing
	$string = utf8_strip_ascii_ctrl($string);

	return $string;
}

// take a key and compare it to an array of words
// do a fuzzy match on their similarity
// return whether or not we consider them the same (bool)
function same_word($needle, $key) {

	$keys = similar_words();
	$needle = trim(strtolower($needle), ' ,.;:!?');
	$soundex_1 = soundex($needle);
	$metaphone_1 = metaphone($needle);

	// loop through the array looking for a matching string
	foreach ($keys as $stored_key => $values) {
		foreach ($values as $value) {

			$levenshtein = levenshtein($needle, $value);
			if (0 == round($levenshtein) && $key == $stored_key) return true;

			$metaphone_2 = metaphone($value);
			if ($metaphone_1 == $metaphone_2 && $key == $stored_key) return true;

			$soundex_2 = soundex($value);
			if ($soundex_1 == $soundex_2 && $key == $stored_key) return true;

			$similar_text = '';
			similar_text($needle, $value, $similar_text);
			if (100 == round($similar_text) && $key == $stored_key) return true;
		}
	}

	return false;
}

// recursive function to assign parent-child hierarchy to sections
// modifies the array by reference, adds 'parent' key
function array_nest(&$sections, $start = 0, $level = 1, $parent_key = null) {

	$sections[$start]['parent'] = $parent_key;
	for ($i = $start + 1; $i < count($sections); $i++) {

		if ($sections[$i]['level'] > $level && empty($sections[$i]['parent'])) {
			// call recursively starting here
			$result = array_nest($sections, $i, $sections[$i]['level'], $start);

		} elseif ($sections[$i]['level'] == $level) {
			// continue at next top level
			if (1 == $level) $result = array_nest($sections, $i);
			return;
		}
	}
}

// remove empty elements from an array
function array_clean($array) {
	if (!is_array($array)) return null;
	return array_filter($array, create_function('$o', 'return !empty($o);'));
}

// recursive count for multidimensional arrays
function rcount ($array) {
  $count = 0;
  if (is_array($array)) {
    foreach($array as $id=>$sub) {
    if (!is_array($sub)) { $count++; }
     else { $count = ($count + rcount($sub)); }
    }
    return $count;
  }
  return FALSE;
}

// key-based natsort
function knatsort(&$arrIn)
{
   $key_array = array();
   $arrOut = array();

   foreach ( $arrIn as $key=>$value ) {
       $key_array[]=$key;
   }
  natsort( $key_array);
  foreach ( $key_array as $key=>$value ) {
      $arrOut[$value]=$arrIn[$value];
  }
  $arrIn=$arrOut;

}

// case-insensitive in_array
function in_iarray($search, &$array) {
  $search = strtolower($search);
  foreach ($array as $item)
    if (strtolower($item) == $search)
      return TRUE;
  return FALSE;
}

// reused trim punctuation
function trim_punc($string) {
	return trim($string, ' ,.;:!?()[]\\/');
}

// remove all punctuation from a string
// from: http://snipplr.com/view.php?codeview&id=5373
function strip_punc( $text )
{
    $urlbrackets    = '\[\]\(\)';
    $urlspacebefore = ':;\'_\*%@&?!' . $urlbrackets;
    $urlspaceafter  = '\.,:;\'\-_\*@&\/\\\\\?!#' . $urlbrackets;
    $urlall         = '\.,:;\'\-_\*%@&\/\\\\\?!#' . $urlbrackets;

    $specialquotes  = '\'"\*<>';

    $fullstop       = '\x{002E}\x{FE52}\x{FF0E}';
    $comma          = '\x{002C}\x{FE50}\x{FF0C}';
    $arabsep        = '\x{066B}\x{066C}';
    $numseparators  = $fullstop . $comma . $arabsep;

    $numbersign     = '\x{0023}\x{FE5F}\x{FF03}';
    $percent        = '\x{066A}\x{0025}\x{066A}\x{FE6A}\x{FF05}\x{2030}\x{2031}';
    $prime          = '\x{2032}\x{2033}\x{2034}\x{2057}';
    $nummodifiers   = $numbersign . $percent . $prime;

	// Checks if PCRE is compiled with UTF-8 and Unicode support
	if (!@preg_match('/\pL/u', 'a')) {
		// POSIX named classes are not supported, use alternative expression
	    return trim(preg_replace(
        	array(
    	    // Remove separator, control, formatting, surrogate,
	        // open/close quotes.
    	        '/[\'"]/u',
	        // Remove other punctuation except special cases
    	        '/[,\.;:!\?\(\)\[\]\/](?<![' . $specialquotes .
	                $numseparators . $urlall . $nummodifiers . '])/u',
    	    // Remove non-URL open/close brackets, except URL brackets.
	            '/[\{\}](?<![' . $urlbrackets . '])/u',
    	    // Remove special quotes, dashes, connectors, number
	        // separators, and URL characters followed by a space
        	    '/[' . $specialquotes . $numseparators . $urlspaceafter .
    	            '-]+((?= )|$)/u',
	        // Remove special quotes, connectors, and URL characters
        	// preceded by a space
    	        '/((?<= )|^)[' . $specialquotes . $urlspacebefore . ']+/u',
	        // Remove dashes preceded by a space, but not followed by a number
        	    '/((?<= )|^)-+(?![\d\$])/u',
    	    // Remove consecutive spaces
	            '/ +/',
        	),
    	    ' ',
	        $text ));
	} else {
		// Unicode safe filter for the value
	    return trim(preg_replace(
        	array(
    	    // Remove separator, control, formatting, surrogate,
	        // open/close quotes.
    	        '/[\p{Z}\p{Cc}\p{Cf}\p{Cs}\p{Pi}\p{Pf}]/u',
	        // Remove other punctuation except special cases
    	        '/\p{Po}(?<![' . $specialquotes .
	                $numseparators . $urlall . $nummodifiers . '])/u',
    	    // Remove non-URL open/close brackets, except URL brackets.
	            '/[\p{Ps}\p{Pe}](?<![' . $urlbrackets . '])/u',
    	    // Remove special quotes, dashes, connectors, number
	        // separators, and URL characters followed by a space
        	    '/[' . $specialquotes . $numseparators . $urlspaceafter .
    	            '\p{Pd}\p{Pc}]+((?= )|$)/u',
	        // Remove special quotes, connectors, and URL characters
        	// preceded by a space
    	        '/((?<= )|^)[' . $specialquotes . $urlspacebefore . '\p{Pc}]+/u',
	        // Remove dashes preceded by a space, but not followed by a number
        	    '/((?<= )|^)\p{Pd}+(?![\p{N}\p{Sc}])/u',
    	    // Remove consecutive spaces
	            '/ +/',
        	),
    	    ' ',
	        $text ));
	}


}

// convert a string to proper title case
function title_case($string) {
        $smallwordsarray = array(
            'of','a','the','and','an','or','nor','but','is','if','then', 'else','when',
            'at','from','by','on','off','for','in','out', 'over','to','into','with');

        $words = explode(' ', $string);
        foreach ($words as $key => $word)
        {
            if ($key == 0 or !in_array(strtolower($word), $smallwordsarray)) {
            	$words[$key] = ucwords(strtolower($word));
            } else {
            	$words[$key] = strtolower($word);
            }
        }

        $newtitle = implode(' ', $words);
        return $newtitle;
}

// return an array of similar/translated words for a key
// (key) is optional, if ommitted, returns an array with all keys
function similar_words($key = '') {

	// define array of keys (UTF-8) in various languages
	// TODO: add languages, eg. spanish, french, etc.
	$trans = array('references' => array('references', 'références', 'referências', 'referenties', 'referencias',
																	'list of references', 'works cited', 'bibliography', 'literatura'),
							 'abstract' => array('abstract', 'résumé', 'resumen'),
							 'correspond' => array('correspondence', 'corresponding author', 'correspondencia'),
							 'keywords' => array('keywords', 'key words', 'palabras claves'),
							 'conflicts' => array('conflicts', 'conflicts of interest', 'competing interests'),
	// portugues? Introdução , Conclusão
	// spanish:  Introducción, Conclusión
							// these are lists, not similar words
							 'institution' => array('school', 'department', 'college', 'institution', 'university', 'center', 'centre'),
							 'paper type' => array('commentary', 'brief report', 'editorial', 'correction', 'letter', 'original research',
																	'original paper', 'research article', 'review'));

	if (array_key_exists($key, $trans)) return $trans[$key];
	else return $trans;
}

// return a key-value array of countries and their codes
// NB: not yet localized, english-only
function country_list() {
	return array(
			'Andorra'			=>			'AD',
			'United Arab Emirates'			=>			'AE',
			'Afghanistan'			=>			'AF',
			'Antigua and Barbuda'			=>			'AG',
			'Anguilla'			=>			'AI',
			'Albania'			=>			'AL',
			'Armenia'			=>			'AM',
			'Netherlands Antilles'			=>			'AN',
			'Angola'			=>			'AO',
			'Antarctica'			=>			'AQ',
			'Argentina'			=>			'AR',
			'American Samoa'			=>			'AS',
			'Austria'			=>			'AT',
			'Australia'			=>			'AU',
			'Aruba'			=>			'AW',
			'Aland Islands'			=>			'AX',
			'Azerbaijan'			=>			'AZ',
			'Bosnia and Herzegovina'			=>			'BA',
			'Barbados'			=>			'BB',
			'Bangladesh'			=>			'BD',
			'Belgium'			=>			'BE',
			'Burkina Faso'			=>			'BF',
			'Bulgaria'			=>			'BG',
			'Bahrain'			=>			'BH',
			'Burundi'			=>			'BI',
			'Benin'			=>			'BJ',
			'Saint Barthélemy'			=>			'BL',
			'Bermuda'			=>			'BM',
			'Brunei'			=>			'BN',
			'Bolivia'			=>			'BO',
			'British Antarctic Territory'			=>			'BQ',
			'Brazil'			=>			'BR',
			'Bahamas'			=>			'BS',
			'Bhutan'			=>			'BT',
			'Bouvet Island'			=>			'BV',
			'Botswana'			=>			'BW',
			'Belarus'			=>			'BY',
			'Belize'			=>			'BZ',
			'Canada'			=>			'CA',
			'Cocos Islands'			=>			'CC',
			'Congo - Kinshasa'			=>			'CD',
			'Central African Republic'			=>			'CF',
			'Congo - Brazzaville'			=>			'CG',
			'Switzerland'			=>			'CH',
			'Ivory Coast'			=>			'CI',
			'Cook Islands'			=>			'CK',
			'Chile'			=>			'CL',
			'Cameroon'			=>			'CM',
			'China'			=>			'CN',
			'Colombia'			=>			'CO',
			'Costa Rica'			=>			'CR',
			'Serbia and Montenegro'			=>			'CS',
			'Canton and Enderbury Islands'			=>			'CT',
			'Cuba'			=>			'CU',
			'Cape Verde'			=>			'CV',
			'Christmas Island'			=>			'CX',
			'Cyprus'			=>			'CY',
			'Czech Republic'			=>			'CZ',
			'East Germany'			=>			'DD',
			'Germany'			=>			'DE',
			'Djibouti'			=>			'DJ',
			'Denmark'			=>			'DK',
			'Dominica'			=>			'DM',
			'Dominican Republic'			=>			'DO',
			'Algeria'			=>			'DZ',
			'Ecuador'			=>			'EC',
			'Estonia'			=>			'EE',
			'Egypt'			=>			'EG',
			'Western Sahara'			=>			'EH',
			'Eritrea'			=>			'ER',
			'Spain'			=>			'ES',
			'Ethiopia'			=>			'ET',
			'Finland'			=>			'FI',
			'Fiji'			=>			'FJ',
			'Falkland Islands'			=>			'FK',
			'Micronesia'			=>			'FM',
			'Faroe Islands'			=>			'FO',
			'French Southern and Antarctic Territories'			=>			'FQ',
			'France'			=>			'FR',
			'Metropolitan France'			=>			'FX',
			'Gabon'			=>			'GA',
			'United Kingdom'			=>			'GB',
			'Grenada'			=>			'GD',
			'Georgia'			=>			'GE',
			'French Guiana'			=>			'GF',
			'Guernsey'			=>			'GG',
			'Ghana'			=>			'GH',
			'Gibraltar'			=>			'GI',
			'Greenland'			=>			'GL',
			'Gambia'			=>			'GM',
			'Guinea'			=>			'GN',
			'Guadeloupe'			=>			'GP',
			'Equatorial Guinea'			=>			'GQ',
			'Greece'			=>			'GR',
			'South Georgia and the South Sandwich Islands'			=>			'GS',
			'Guatemala'			=>			'GT',
			'Guam'			=>			'GU',
			'Guinea-Bissau'			=>			'GW',
			'Guyana'			=>			'GY',
			'Hong Kong SAR China'			=>			'HK',
			'Heard Island and McDonald Islands'			=>			'HM',
			'Honduras'			=>			'HN',
			'Croatia'			=>			'HR',
			'Haiti'			=>			'HT',
			'Hungary'			=>			'HU',
			'Indonesia'			=>			'ID',
			'Ireland'			=>			'IE',
			'Israel'			=>			'IL',
			'Isle of Man'			=>			'IM',
			'India'			=>			'IN',
			'British Indian Ocean Territory'			=>			'IO',
			'Iraq'			=>			'IQ',
			'Iran'			=>			'IR',
			'Iceland'			=>			'IS',
			'Italy'			=>			'IT',
			'Jersey'			=>			'JE',
			'Jamaica'			=>			'JM',
			'Jordan'			=>			'JO',
			'Japan'			=>			'JP',
			'Johnston Island'			=>			'JT',
			'Kenya'			=>			'KE',
			'Kyrgyzstan'			=>			'KG',
			'Cambodia'			=>			'KH',
			'Kiribati'			=>			'KI',
			'Comoros'			=>			'KM',
			'Saint Kitts and Nevis'			=>			'KN',
			'North Korea'			=>			'KP',
			'South Korea'			=>			'KR',
			'Kuwait'			=>			'KW',
			'Cayman Islands'			=>			'KY',
			'Kazakhstan'			=>			'KZ',
			'Laos'			=>			'LA',
			'Lebanon'			=>			'LB',
			'Saint Lucia'			=>			'LC',
			'Liechtenstein'			=>			'LI',
			'Sri Lanka'			=>			'LK',
			'Liberia'			=>			'LR',
			'Lesotho'			=>			'LS',
			'Lithuania'			=>			'LT',
			'Luxembourg'			=>			'LU',
			'Latvia'			=>			'LV',
			'Libya'			=>			'LY',
			'Morocco'			=>			'MA',
			'Monaco'			=>			'MC',
			'Moldova'			=>			'MD',
			'Montenegro'			=>			'ME',
			'Madagascar'			=>			'MG',
			'Saint Martin'			=>			'MF',
			'Marshall Islands'			=>			'MH',
			'Midway Islands'			=>			'MI',
			'Macedonia'			=>			'MK',
			'Mali'			=>			'ML',
			'Myanmar'			=>			'MM',
			'Mongolia'			=>			'MN',
			'Macao SAR China'			=>			'MO',
			'Northern Mariana Islands'			=>			'MP',
			'Martinique'			=>			'MQ',
			'Mauritania'			=>			'MR',
			'Montserrat'			=>			'MS',
			'Malta'			=>			'MT',
			'Mauritius'			=>			'MU',
			'Maldives'			=>			'MV',
			'Malawi'			=>			'MW',
			'Mexico'			=>			'MX',
			'Malaysia'			=>			'MY',
			'Mozambique'			=>			'MZ',
			'Namibia'			=>			'NA',
			'New Caledonia'			=>			'NC',
			'Niger'			=>			'NE',
			'Norfolk Island'			=>			'NF',
			'Nigeria'			=>			'NG',
			'Nicaragua'			=>			'NI',
			'Netherlands'			=>			'NL',
			'Norway'			=>			'NO',
			'Nepal'			=>			'NP',
			'Dronning Maud Land'			=>			'NQ',
			'Nauru'			=>			'NR',
			'Neutral Zone'			=>			'NT',
			'Niue'			=>			'NU',
			'New Zealand'			=>			'NZ',
			'Oman'			=>			'OM',
			'Panama'			=>			'PA',
			'Pacific Islands Trust Territory'			=>			'PC',
			'Peru'			=>			'PE',
			'French Polynesia'			=>			'PF',
			'Papua New Guinea'			=>			'PG',
			'Philippines'			=>			'PH',
			'Pakistan'			=>			'PK',
			'Poland'			=>			'PL',
			'Saint Pierre and Miquelon'			=>			'PM',
			'Pitcairn'			=>			'PN',
			'Puerto Rico'			=>			'PR',
			'Palestinian Territory'			=>			'PS',
			'Portugal'			=>			'PT',
			'U.S. Miscellaneous Pacific Islands'			=>			'PU',
			'Palau'			=>			'PW',
			'Paraguay'			=>			'PY',
			'Panama Canal Zone'			=>			'PZ',
			'Qatar'			=>			'QA',
			'Outlying Oceania'			=>			'QO',
			'European Union'			=>			'QU',
			'Reunion'			=>			'RE',
			'Romania'			=>			'RO',
			'Serbia'			=>			'RS',
			'Russia'			=>			'RU',
			'Rwanda'			=>			'RW',
			'Saudi Arabia'			=>			'SA',
			'Solomon Islands'			=>			'SB',
			'Seychelles'			=>			'SC',
			'Sudan'			=>			'SD',
			'Sweden'			=>			'SE',
			'Singapore'			=>			'SG',
			'Saint Helena'			=>			'SH',
			'Slovenia'			=>			'SI',
			'Svalbard and Jan Mayen'			=>			'SJ',
			'Slovakia'			=>			'SK',
			'Sierra Leone'			=>			'SL',
			'San Marino'			=>			'SM',
			'Senegal'			=>			'SN',
			'Somalia'			=>			'SO',
			'Suriname'			=>			'SR',
			'Sao Tome and Principe'			=>			'ST',
			'Union of Soviet Socialist Republics'			=>			'SU',
			'El Salvador'			=>			'SV',
			'Syria'			=>			'SY',
			'Swaziland'			=>			'SZ',
			'Turks and Caicos Islands'			=>			'TC',
			'Chad'			=>			'TD',
			'French Southern Territories'			=>			'TF',
			'Togo'			=>			'TG',
			'Thailand'			=>			'TH',
			'Tajikistan'			=>			'TJ',
			'Tokelau'			=>			'TK',
			'East Timor'			=>			'TL',
			'Turkmenistan'			=>			'TM',
			'Tunisia'			=>			'TN',
			'Tonga'			=>			'TO',
			'Turkey'			=>			'TR',
			'Trinidad and Tobago'			=>			'TT',
			'Tuvalu'			=>			'TV',
			'Taiwan'			=>			'TW',
			'Tanzania'			=>			'TZ',
			'Ukraine'			=>			'UA',
			'Uganda'			=>			'UG',
			'United States Minor Outlying Islands'			=>			'UM',
			'United States'			=>			'US',
			'Uruguay'			=>			'UY',
			'Uzbekistan'			=>			'UZ',
			'Vatican'			=>			'VA',
			'Saint Vincent and the Grenadines'			=>			'VC',
			'North Vietnam'			=>			'VD',
			'Venezuela'			=>			'VE',
			'British Virgin Islands'			=>			'VG',
			'U.S. Virgin Islands'			=>			'VI',
			'Vietnam'			=>			'VN',
			'Vanuatu'			=>			'VU',
			'Wallis and Futuna'			=>			'WF',
			'Wake Island'			=>			'WK',
			'Samoa'			=>			'WS',
			'People\'s Democratic Republic of Yemen'			=>			'YD',
			'Yemen'			=>			'YE',
			'Mayotte'			=>			'YT',
			'South Africa'			=>			'ZA',
			'Zambia'			=>			'ZM',
			'Zimbabwe'			=>			'ZW',
			);
}

?>
<?php

/**
 * citations_controller.php
 *
 * Copyright (c) 2008-2009 MJ Suhonos
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Controller for citations
 */

class CitationsController extends AppController {

    var $name = 'Citations';
	var $othAuthRestrictions = "*";				// list of restrictions that require authentication

	var $components = array('RequestHandler', 'CitationParser');
	var $helpers = array('Ajax');

	// redirect to login
    function index()
    {
     	$this->redirect('/users');
    }

	// edit citations using the fancy GUI
	function edit($citationId = null)
	{
		// validate citation ID and get citation
		$this->_checkID($citationId);

		// get metadata for this citation
		$this->metadata = $this->Citation->loadMetadata($citationId);

		// pass metadata through to view
		$this->pageTitle = 'Edit Citation';
		$this->set('citation', $this->citation);
		$this->set('metadata', $this->metadata);

		if ($this->RequestHandler->isAjax()) {
			$this->autoRender=false;
			$this->render('edit', 'ajax');
		}
	}

	// save updates to a citation made through the GUI
	function save($citationId = null) {
		// validate citation ID and get citation
		$this->_checkID($citationId);

		// save each of the POST variables to the DB as metadata elements
		$this->Citation->saveMetadata($this->data['Metadata']);

		// update citation modification date
		$this->Citation->save($this->data);

		//FIXME:  I think I am not organizing the ajax updates very well
		// it seems to me that its messy to sprinkle these updates in random actions like this one
		if ($this->RequestHandler->isAjax()) {
			$this->autoRender=false;
			echo $this->data['Citation']['marked_contents'];
		} else {
			// redirect to the edit view
			$this->redirect('/citations/edit/'.$citationId);
		}
	}

	// move a citation up in the list
	function moveup($citationId = null, $delta = 1){
        $citation = $this->Citation->findById($citationId);
        $this->Citation->id = $citation['Citation']['id'];

		$this->Citation->moveup($this->Citation->id, abs($delta));

		$this->redirect('/documents/citations_edit/'.$citation['Document']['id']);
    }

	// move a citation down in the list
	function movedown($citationId = null, $delta = 1){
        $citation = $this->Citation->findById($citationId);
        $this->Citation->id = $citation['Citation']['id'];

		$this->Citation->movedown($this->Citation->id, abs($delta));

		$this->redirect('/documents/citations_edit/'.$citation['Document']['id']);
    }

	// remove a citation from the list
	function remove($citationId = null){
        $citation = $this->Citation->findById($citationId);
        $this->Citation->id = $citation['Citation']['id'];

		$this->Citation->removefromtree($this->Citation->id, true);

        $this->redirect('/documents/citations_edit/'.$citation['Document']['id']);
    }

	function updateContent($citationId = null) {

		if ($this->RequestHandler->isAjax() && isset($_POST['full_reference'])) {
			$this->autoRender = false;
			$citation = $_POST['full_reference'];
			// update contents and modification date
			$this->data['Citation']['contents'] = $citation;
			$this->Citation->save($this->data);
			echo $citation;

		} else {
			// this is an ajax only view
			 $this->flash('This is an AJAX only view');
		}
	}

	// pass a citation through the parser, update citation metadata
	function parse($citationId = null, $smallView = false)
	{
		// validate citation ID and get citation
		$this->_checkID($citationId);

		// get the data to parse
		if (isset($_POST['full_reference']) && $_POST['full_reference'] != '') $citation = $_POST['full_reference'];
		elseif ($this->citation['Citation']['contents'] != '') $citation = $this->citation['Citation']['contents'];
		else $this->flash('No citation data to parse', '/citations/edit/'.$citationId);

		// pass string to the parser to make magic
		$results = $this->CitationParser->parse($citation);

		// if the score is above the threshold, save and use these values
		if (round($results['parse_score']) >= $this->Session->read('Settings.parse_margin')) {
			$this->Citation->saveMetadata($results['best']);
		}

		// update contents and modification date
		$this->citation['Citation']['parse_score'] = round($results['parse_score']);
		$this->data['Citation']['parse_score'] = round($results['parse_score']);
		$this->data['Citation']['contents'] = $citation;
		$this->Citation->save($this->data);

		// get metadata for this citation
		$this->metadata = $this->Citation->loadMetadata($citationId);

		// display small AJAX result if specified
		if ($smallView) {

			// automatically lookup as well if this is the initial parse
			if (0 == $this->citation['Citation']['lookup_score']) {
				$this->redirect('/citations/lookup/'.$citationId.'/1');
				return;
			}

			$this->set('citation', $this->citation);
			$this->autoRender=false;
			$this->render('parse', 'ajax');
			return;
		}

		$this->set('metadata', $this->metadata);
		$this->set('citation', $this->citation);
		$this->set('options', $results['elements']);

		$this->pageTitle = 'Parse Citation';
		$this->set('message', 'Citation Parsed ('.round($results['parse_score']).'%)');

		if ($this->RequestHandler->isAjax()) {
			$this->autoRender=false;
			$this->render('edit', 'ajax');
		} else {
			$this->render('edit');
		}

		return;
	}

	// take a parsed citation, look it up via some web services,
	// and if similar results are found, suggest corrections
	function lookup($citationId = null, $smallView = false)
	{
		// validate citation ID and get citation
		$this->_checkID($citationId);

		// get metadata for this citation from the database
		$this->Citation->Metadata->unbindAll();

		// get metadata for this citation
		$this->metadata = $this->Citation->loadMetadata($citationId);

		// if metadata is empty, the citation is unparsed; redirect to parser
		if (empty($this->metadata) && !$smallView) $this->redirect('/citations/parse/'.$citationId);

		// pass string to the lookup components to make magic
		$results = $this->CitationParser->lookup($this->metadata);

		// if the score is above the threshold, save and use these values
		if (round($results['lookup_score']) >= $this->Session->read('Settings.lookup_margin')) {

			foreach (array_merge($results['best'], $this->metadata) as $element => $array) {

				if (!empty($results['best'][$element])) {
					$value = $results['best'][$element];
				} else {
					$value = $array['value'];
				}

				if (is_array($array)) {
					$newmeta[$element.'_'.$array['id']] = $value;
				} else {
					$newmeta[$element] = $value;
				}
			}

			$this->Citation->saveMetadata($newmeta);

			// update modification & validation date
			$this->citation['Citation']['lookup_score'] = round($results['lookup_score']);
			$this->data['Citation']['lookup_score'] = round($results['lookup_score']);
			$this->data['Citation']['validated'] = date('Y-m-d H:i:s');
			$this->Citation->save($this->data);

			$this->set('message',  'Citation Found ('.round($results['lookup_score']).'%)');
		} else {
			$this->set('message',  'Citation Not Found ('.round($results['lookup_score']).'%)');
		}

		// display small AJAX result if specified
		if ($smallView) {
			$this->citation['Citation']['lookup_score'] = round($results['lookup_score']);
			$this->citation['Citation']['validated'] = date('Y-m-d H:i:s');
			$this->set('citation', $this->citation);
			$this->autoRender=false;
			$this->render('lookup', 'ajax');
			return;
		}

		$this->set('metadata', $this->metadata);
		$this->set('citation', $this->citation);
		$this->set('options', $results['elements']);

		$this->pageTitle = 'Lookup Citation';

		if ($this->RequestHandler->isAjax()) {
			$this->autoRender=false;
			$this->render('edit', 'ajax');
		} else {
			$this->render('edit');
		}

		return;
	}

	//
	// ========== Internal methods below here ==========
	//

	// ensure we have a valid citation, given a citation ID
	function _checkID($citationId) {

		// check valid citation ID is provided
		if (is_numeric($citationId)) {

			// unbind document from citation
			$this->Citation->unbindAll(array('hasMany'=>array('Metadata')));

			if ($citation =& $this->Citation->findById($citationId))
				$this->citation =& $citation;
			else
				// otherwise: couldn't find a citation in the DB with that ID
				$this->flash('Unable to find a citation with that ID', '/');

		// otherwise: non-numeric or empty ID provided, return with error
		} else $this->flash('Please provide a valid citation ID', '/');
	}

}

?>
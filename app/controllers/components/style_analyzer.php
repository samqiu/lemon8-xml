<?php

/**
 * style_analyzer.php
 *
 * Copyright (c) 2008-2009 MJ Suhonos
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Analyze the style information in an ODT XML document,
 * and combine an analysis of defined styles
 *
 */

class StyleAnalyzerComponent extends Object
{
 	var $components = array('Session');

	// analyze the style XML for layout cues
	function analyzeStyles($styleXML, $contentXML) {

		// create a temporary DOM document for loading XSL
		$tempDOM = new DOMDocument();
		$tempDOM->recover = true;			// try to handle non-well-formed responses

		// get XSL stylesheet to simplify styles
		$proc = new XsltProcessor();
	 	$xsl = dirname(__FILE__) . DS . 'stylesheets' . DS . 'odt-styles.xsl';
		$tempDOM->load($xsl);
		$proc->importStylesheet($tempDOM);

		// transform the style and content XML into simplified DOMs
		ini_set('max_execution_time', 60);

		$tempDOM->loadXML($styleXML);
		$simpleStyleDOM =& $proc->transformToDoc($tempDOM);

		$tempDOM->loadXML($contentXML);
		$simpleContentDOM =& $proc->transformToDoc($tempDOM);

		// transform the DOMs and into a single style array
		$styles = array_merge($this->generateStyleArray($simpleStyleDOM),
												$this->generateStyleArray($simpleContentDOM));

		// go through the list of styles and build lists of important types
		$styleArray = $this->extractStyles($styles);

		// get XSL stylesheet to clean content
	 	$xsl = dirname(__FILE__) . DS . 'stylesheets' . DS . 'odt-content.xsl';
		$tempDOM->load($xsl);
		$proc->importStylesheet($tempDOM);

		foreach ($styleArray as $param => $value) $proc->setParameter('', $param, $value);

		// normalize content for further saving/processing
		$tempDOM->loadXML($contentXML);
		$content = $proc->transformToXML($tempDOM);

		return $content;
	}

	// Make an array of important style elements
	function generateStyleArray($styleDOM) {
		$styleArray = array();

		foreach ($styleDOM->getElementsByTagName('style') as $styleNode) {
			$style = array();
			foreach ($styleNode->childNodes as $styleAttribute) {

				// normalize sizes to pixels
				if ($styleAttribute->nodeName == 'size') {
					if (strpos($styleAttribute->nodeValue, 'pt') !== false)
						$styleAttribute->nodeValue = str_replace('pt', '', $styleAttribute->nodeValue);
					elseif (strpos($styleAttribute->nodeValue, '%') !== false)
						$styleAttribute->nodeValue = $this->Session->read('Settings.default_size') *
						((int) str_replace('pt', '', $styleAttribute->nodeValue))/100;
				}

				// append the attribute to the array
				if (isset($style[$styleAttribute->nodeName]))
					$style[$styleAttribute->nodeName] .= '; '.$styleAttribute->nodeValue;
				else
					$style[$styleAttribute->nodeName] = $styleAttribute->nodeValue;
			}
			$styleArray[$style['name']] = $style;
		}

		return $styleArray;
	}

	// perform analytics; determine base style elements, italics, bolds, etc
	function extractStyles($styleArray) {
		$headings = array(array(), array(), array(), array(), array(), array());
		$sizes = $bolds = $italics = $underlines = $superscripts = $subscripts = array();

		foreach ($styleArray as &$style) {

			// make all styles inherit their parent's size if they don't have one set and have text children
			if (!isset($style['size']) && isset($style['parent']) && isset($styleArray[$style['parent']]['size']) && $style['text-children'] > 0) {
				$style['size'] = $styleArray[$style['parent']]['size'];
			}

			if (isset($style['size'])) $sizes[] = $style['size'];
			else $sizes[] = $this->Session->read('Settings.default_size');

			// add styles to the bold array (inherit)
			if (isset($style['parent']) && isset($styleArray[$style['parent']]['weight'])
					&& $styleArray[$style['parent']]['weight'] == 'bold') $style['weight'] = 'bold';
			if (isset($style['weight']) && $style['weight'] == 'bold') $bolds[] = $style['name'];

			// add styles to the italics array (inherit)
			if (isset($style['parent']) && isset($styleArray[$style['parent']]['italic'])
					&& $styleArray[$style['parent']]['italic'] == 'italic') $style['italic'] = 'italic';
			if (isset($style['italic']) && $style['italic'] == 'italic') $italics[] = $style['name'];

			// add styles to the underlines array (inherit)
			if (isset($style['parent']) && isset($styleArray[$style['parent']]['underline'])
					&& $styleArray[$style['parent']]['underline'] != 'none') $style['underline'] = 'underline';
			if (isset($style['underline']) && $style['underline'] != 'none') $underlines[] = $style['name'];

			// add styles to the superscripts array
			if (isset($style['position']) && strpos($style['position'],'super') !== false) $superscripts[] = $style['name'];

			// add styles to the subscripts array
			if (isset($style['position']) && strpos($style['position'],'sub') !== false) $subscripts[] = $style['name'];
		}

		// calculate the mean and mode font size
		$sizes = array_clean($sizes);
		$sum = array_sum($sizes);
		$mean = $sum/count($sizes);

		$mode = array_count_values($sizes);
		arsort($mode);	reset($mode);
		$mode = key($mode);

		// the "base" size is the rounded average between the two
		$baseSize = round(($mean + $mode)/2);

		// search through the array and try to determine headings on the basis of style
		foreach ($styleArray as $style) {
			$types = 0;

			// each of these things adds (weighted) likelihood for heading
			if (isset($style['size']) && $style['size'] >= $baseSize) {
				$types = $types + $style['size'] - $baseSize;
			} else {
				// set the size to base size for visual reference
				$style['size'] = $baseSize;
			}

			if (in_array($style['name'], $bolds)) $types = $types + 2;	// 2 points for bold
			if (in_array($style['name'], $underlines)) $types++;				// 1 point for underline
			if (in_array($style['name'], $italics)) $types++;						// 1 point for italic
			if (isset($style['family']) && $style['family'] == 'paragraph') $types++;		// 1 point for a paragraph

			// TODO: possibly find a better way to assign heading level
			// NB: current calculation is too greedy
			$types = round($types);

			if (isset($style['level']) && $style['level'] <= 5) {
				$headings[(int)$style['level']][] = $style['name'];

			} elseif ($types >= 3 && preg_match('/_[1-5]$/', $style['name'])) {
				switch (substr($style['name'], -1)) {
					case 1: $headings[1][] = $style['name']; break;
					case 2: $headings[2][] = $style['name']; break;
					case 3: $headings[3][] = $style['name']; break;
					case 4: $headings[4][] = $style['name']; break;
					case 5: $headings[5][] = $style['name']; break;
				}

			} elseif ($types >= 5) {
				$headings[1][] = $style['name'];

			} elseif ($types >= 4) {
				$headings[2][] = $style['name'];

			} elseif ($types >= 3) {
				$headings[3][] = $style['name'];
			}

			/*
			// TODO:  REFACTOR / REEXAMINE THIS CODE

			if ( isset($style['type'])  &&	preg_match('/text:list-style: (\w*)/i', $style['type'], $matches) ) {
				if ( $matches[1] == 'bullet' )
					$this->bulletList[] = $style['name'];
				elseif ( $matches[1] == 'number' )
					$this->numberList[] = $style['name'];
				else
					$this->otherList[] = $style['name'];
			}
			*/
		}

		// NB: we add an extra padded , on the end of the string to make XSL easier
		$compactedHeadingStyles = array(
			'headings_1' => implode(",", $headings[1]) . "," ,
			'headings_2' => implode(",", $headings[2]) . "," ,
			'headings_3' => implode(",", $headings[3]) . "," ,
			'headings_4' => implode(",", $headings[4]) . "," ,
			'headings_5' => implode(",", $headings[5]) . "," ,
			'bolds' => implode(",", $bolds) . "," ,
			'italics' => implode(",", $italics) . "," ,
			'underlines' => implode(",", $underlines) . "," ,
			'superscripts' => implode(",", $superscripts) . "," ,
			'subscripts' => implode(",", $subscripts) . ","
			/*
			'bulletList' => implode(",", $this->bulletList) . "," ,
			'numberList' => implode(",", $this->numberList) . "," ,
			*/
			);

		return $compactedHeadingStyles;
	}
}

?>
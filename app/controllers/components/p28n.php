<?php

/*
 * p28n.php - persistent internationalization
 *
 * By Jason Chow aka "p0windah" (chow.man@gmail.com)
 * 
 * Copied freely from: http://bakery.cakephp.org/articles/view/p28n-the-top-to-bottom-persistent-internationalization-tutorial
 */

class P28nComponent extends Object {
	var $components = array('Session', 'Cookie');

	function startup() {
		if (!$this->Session->check('Config.language')) {
			$this->change(($this->Cookie->read('lang') ? $this->Cookie->read('lang') : 'DEFAULT_LANGUAGE'));
		}
	}

	function change($lang = null) {
		if (!empty($lang)) {
			$this->Session->write('Config.language', $lang);
			$this->Cookie->write('lang', $lang, null, '+350 day'); 
		}
	}
}

?>
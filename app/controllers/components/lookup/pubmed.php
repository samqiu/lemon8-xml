<?php

/**
 * pubmed.php
 *
 * Copyright (c) 2008-2009 MJ Suhonos
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Uses the Pubmed/NCBI eUtils web services to search for citation metadata
 *
 */

class PubmedComponent extends Object
{
	// array of settings required for use
	var $settings = array('email' => 'Email address for Pubmed');

	// types of citation that can be processed
	// NB: proceedings haven't been well verified
	var $types = array('journal', 'proceedings');

	function lookup($metadata, $email) {

		//	do a webservice lookup based on the given metadata array to find a PMID
		// if the array already has a PMID, skip straight to eFetch; otherwise, try to get one from eSearch
		if (!isset($metadata['pmid']) or $metadata['pmid'] == '') {

			// create a temporary DOM document
			$tempDOM = new DOMDocument();
			$tempDOM->recover = true;			// try to handle non-well-formed responses

			// 1) try a "loose" search based on author list (this works surprisingly well for pubmed)
			$url = "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=pubmed";
			$url .= "&tool=Lemon8-XML&email=".urlencode($email);
			$url .= "&term=".urlencode($metadata['authors']);

			// call the eSearch URL and get an XML result
			$tempDOM->loadXML(utf8_normalize(getCachedURL($url)));
			$resultDOM =& $tempDOM;

			// loop through any results we have and add them to a PMID array
			$pmidArray_authors = array();
			foreach ($resultDOM->getElementsByTagName('Id') as $idNode) $pmidArray_authors[] = $idNode->textContent;

			// 2) try a search based on the article title
			$url = "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=pubmed";
			$url .= "&tool=Lemon8-XML&email=".urlencode($email);
			$url .= "&term=".urlencode($metadata['atitle']);

			// call the eSearch URL and get an XML result
			$tempDOM->loadXML(utf8_normalize(getCachedURL($url)));
			$resultDOM =& $tempDOM;

			// loop through any results we have and add them to a PMID array
			$pmidArray_title = array();
			foreach ($resultDOM->getElementsByTagName('Id') as $idNode) $pmidArray_title[] = $idNode->textContent;

			// 3) try a "strict" search based on as much information as possible
			$url = "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=pubmed";
			$url .= "&tool=Lemon8-XML&email=".urlencode($email);
			$url .= "&term=".urlencode($metadata['atitle']);
//			$url .= "+AND+".urlencode($metadata['aulast'])."+".urlencode(substr($metadata['aufirst'], 0, 1))."[Auth]";
			$url .= "+AND+".urlencode($metadata['title'])."[Jour]";
			if (trim($metadata['year']) != '') $url .= "+AND+".urlencode($metadata['year'])."[DP]";
			if (trim($metadata['volume']) != '') $url .= "+AND+".urlencode($metadata['volume'])."[VI]";
			if (trim($metadata['issue']) != '') $url .= "+AND+".urlencode($metadata['issue'])."[IP]";
			if (trim($metadata['spage']) != '') $url .= "+AND+".urlencode($metadata['spage'])."[PG]";

			// call the eSearch URL and get an XML result
			$tempDOM->loadXML(utf8_normalize(getCachedURL($url)));
			$resultDOM =& $tempDOM;

			// loop through any results we have and add them to a PMID array
			$pmidArray_strict = array();
			foreach ($resultDOM->getElementsByTagName('Id') as $idNode) $pmidArray_strict[] = $idNode->textContent;

			// TODO add another search like strict, but without article title
			// eg:  http://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=pubmed&term=Baumgart+Dc[Auth]+AND+Lancet[Jour]+AND+2005[DP]+AND+366[VI]+AND+9492[IP]+AND+1210[PG]&tool=Lemon8-XML&email=

			// compare the arrays to try to narrow it down to one PMID

			// first: check the strict search for a single result
			if (count($pmidArray_strict) == 1) {
				$pmid = $pmidArray_strict[0];

			} elseif (count($pmid_3way = array_intersect($pmidArray_title, $pmidArray_authors, $pmidArray_strict)) == 1) {
				// otherwise, look for a 3-way union
				$pmid = current($pmid_3way);

			} elseif (count($pmid_2way1 = array_intersect($pmidArray_title, $pmidArray_strict)) == 1) {
				// look for a 2-way union: title / strict
				$pmid = current($pmid_2way1);

			} elseif (count($pmid_2way2 = array_intersect($pmidArray_authors, $pmidArray_strict)) == 1) {
				// look for a 2-way union: authors / strict
				$pmid = current($pmid_2way2);

			} elseif (count($pmid_2way3 = array_intersect($pmidArray_authors, $pmidArray_title)) == 1) {
				// look for a 2-way union: authors / title
				$pmid = current($pmid_2way3);

			} elseif (count($pmidArray_title) == 1) {
				// we only have one result for title
				$pmid = $pmidArray_title[0];

			} elseif (count($pmidArray_authors) == 1) {
				// we only have one result for authors
				$pmid = $pmidArray_authors[0];

			} else $pmid = ''; // we were unable to find a PMID

		} else $pmid = $metadata['pmid'];

		// if we have a PMID, get a metadata array for it
		if ($pmid) {
			$pubmedMetadata = $this->extractPubmed($pmid, $email);

			// trim extra stuff from all elements
			$pubmedMetadata = array_map('trim_punc', $pubmedMetadata);
			return $pubmedMetadata;

		} else return array();

	}

	// take a PMID, return an array mapped to internal elements
	function extractPubmed($pmid, $email) {

		// use eFetch to get XML metadata for the given PMID
		$url = "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=pubmed&mode=xml";
		$url .= "&tool=Lemon8-XML&email=".urlencode($email);
		$url .= "&id=".urlencode($pmid);

		// create a temporary DOM document
		$tempDOM = new DOMDocument();

		// call the eFetch URL and get an XML result
		// TODO: this code should handle an empty return value
		$tempDOM->loadXML(utf8_normalize(getCachedURL($url)));
		$resultDOM =& $tempDOM;

		// ====
		// TODO: this could be replaced by an XSL mapping
		// ====

		$pubmed_metadata['pmid'] = $pmid;
		$pubmed_metadata['comment'] = '';

		$pubmed_metadata['atitle'] = $resultDOM->getElementsByTagName("ArticleTitle")->item(0)->textContent;
		$pubmed_metadata['title'] = $resultDOM->getElementsByTagName("MedlineTA")->item(0)->textContent;
		if ($resultDOM->getElementsByTagName("Volume")->length > 0) $pubmed_metadata['volume'] = $resultDOM->getElementsByTagName("Volume")->item(0)->textContent;
		if ($resultDOM->getElementsByTagName("Issue")->length > 0) $pubmed_metadata['issue'] = $resultDOM->getElementsByTagName("Issue")->item(0)->textContent;

		// get list of author full names
		$pubmed_metadata['authors'] = '';
		foreach ($resultDOM->getElementsByTagName("Author") as $authorNode) {
/*
			if ($pubmed_metadata['authors'] == '') {

				if ($authorNode->getElementsByTagName("FirstName")->length > 0) {
					$pubmed_metadata['aufirst'] = $authorNode->getElementsByTagName("FirstName")->item(0)->textContent;
				} elseif ($authorNode->getElementsByTagName("FirstName")->length > 0) {
					$pubmed_metadata['aufirst'] = $authorNode->getElementsByTagName("ForeName")->item(0)->textContent;
				}

				$pubmed_metadata['aulast'] = $authorNode->getElementsByTagName("LastName")->item(0)->textContent;
			}
*/
			$author_name = $authorNode->getElementsByTagName("LastName")->item(0)->textContent;

			if ($authorNode->getElementsByTagName("FirstName")->length > 0) {
				$author_name .= ' '.$authorNode->getElementsByTagName("FirstName")->item(0)->textContent;
			} else if ($authorNode->getElementsByTagName("ForeName")->length > 0) {
				$author_name .= ' '.$authorNode->getElementsByTagName("ForeName")->item(0)->textContent;
			}

			// include collective names
			// TODO: this should probably go in institution field
			if ($resultDOM->getElementsByTagName("CollectiveName")->length > 0 && $authorNode->getElementsByTagName("CollectiveName")->item(0)->textContent != '') {
					$author_name .= $authorNode->getElementsByTagName("CollectiveName")->item(0)->textContent;
			}
			$author_name .= ', ';

			$pubmed_metadata['authors'] .= $author_name;
		}
		$pubmed_metadata['authors'] = trim($pubmed_metadata['authors'], ', ');

		// extract pagination
		if (preg_match("/^[:p\.\s]*(?P<p1>[Ee]?\d+)(-(?P<p2>\d+))?/", $resultDOM->getElementsByTagName("MedlinePgn")->item(0)->textContent, $pages)) {
			$pubmed_metadata['spage'] = $pages[1];
			if (isset($pages[3])) $pubmed_metadata['epage'] = $pages[3];
		}

		// get publication date
		// TODO: this could be in multiple places
		if ($resultDOM->getElementsByTagName("ArticleDate")->length > 0) {
			$pubmed_metadata['year'] = $resultDOM->getElementsByTagName("ArticleDate")->item(0)->getElementsByTagName("Year")->item(0)->textContent;
			$pubmed_metadata['month'] = $resultDOM->getElementsByTagName("ArticleDate")->item(0)->getElementsByTagName("Month")->item(0)->textContent;
			$pubmed_metadata['day'] = $resultDOM->getElementsByTagName("ArticleDate")->item(0)->getElementsByTagName("Day")->item(0)->textContent;
		}

		// get DOI if it exists
		foreach ($resultDOM->getElementsByTagName("ArticleId") as $idNode) {
			if ($idNode->getAttribute('IdType') == 'doi') $pubmed_metadata['doi'] = $idNode->textContent;
		}

		// ==== END XSL MAPPING

		// use eLink utility to find fulltext links
		$url = "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/elink.fcgi?dbfrom=pubmed&cmd=llinks";
		$url .= "&tool=Lemon8-XML&email=".urlencode($email);
		$url .= "&id=".$pmid;

		// call the eFetch URL and get an XML result
		$tempDOM->loadXML(utf8_normalize(getCachedURL($url)));
		$resultDOM =& $tempDOM;

		// get a list of possible links
		foreach ($resultDOM->getElementsByTagName("ObjUrl") as $linkOut) {
			$attributes = '';
			foreach ($linkOut->getElementsByTagName("Attribute") as $attribute) $attributes .= strtolower($attribute->textContent).' / ';

			// NB: only add links to open access resources
			if (strpos($attributes, "subscription") === false && strpos($attributes, "membership") === false &&
				 strpos($attributes, "fee") === false && $attributes != "") {
				$links[] = $linkOut->getElementsByTagName("Url")->item(0)->textContent;
			 }
		}

		// take the first link if we have any left (presumably pubmed returns them in preferential order)
		if (isset($links[0])) $pubmed_metadata['targetURL'] = $links[0];

		return $pubmed_metadata;
	}

}
?>

<?xml version="1.0"?>

<!--============================================

	isbndb.xsl - Crosswalk from ISBNDB API XML to L8X citation elements (OpenURL 1.0)
	Part of Lemon8-XML; developed by the Public Knowledge Project   http://pkp.sfu.ca

	Copyright (c) 2008-2009 MJ Suhonos
	Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.

==============================================-->

<xsl:transform version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:php="http://php.net/xsl"
	exclude-result-prefixes="xsl php">

<xsl:output omit-xml-declaration='yes'/>

<xsl:strip-space elements="*"/>

<!--============================================
	START TRANSFORMATION AT THE ROOT NODE
==============================================-->
<xsl:template match="/">
	<citation>
		<xsl:apply-templates select="ISBNdb/BookList/BookData/*"/>
	</citation>
</xsl:template>

<!-- Book title -->
<xsl:template match="TitleLong">
	<title>
		<xsl:choose>
			<xsl:when test=". != ''">
				<xsl:value-of select="."/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="../Title"/>
			</xsl:otherwise>
		</xsl:choose>
	</title>
</xsl:template>

<!-- Authors -->
<xsl:template match="Authors">
	<xsl:for-each select="Person">
		<author><xsl:value-of select="php:functionString('preg_replace', '/\. /', '', .)"/></author>
	</xsl:for-each>
</xsl:template>

<!-- Publisher & Location -->
<xsl:template match="PublisherText">
	<!-- FIXME: too greedy  -->
	<publoc><xsl:value-of select="php:functionString('preg_replace', '/^(.+):.*/', '\1', .)"/></publoc>

	<publisher><xsl:value-of select="php:functionString('preg_replace', '/.*:([^,]+),?.*/', '\1', .)"/></publisher>

	<!-- also possible year in Details/@edition_info -->	
	<year><xsl:value-of select="php:functionString('preg_replace', '/^[^\d{4}]+(\d{4}).*/', '\1', .)"/></year>
</xsl:template>

<!-- possible edition in Details/@edition_info -->

<!-- Ignore everything else -->
<xsl:template match="*"/>

</xsl:transform>
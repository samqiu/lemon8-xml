<?php

/**
 * isbndb.php
 *
 * Copyright (c) 2008-2009 MJ Suhonos
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Uses the ISBNdb web service to search for book citation metadata
 *
 */

class IsbndbComponent extends Object
{
	// array of settings required for use
	var $settings = array('apikey' => 'ISBNdb API key');

	// types of citation that can be processed
	var $types = array('book');

	function lookup($metadata, $apikey = '') {

		// ISBNdb search API URL
		$baseUrl = 'http://isbndb.com/api/books.xml?';
		$baseUrl .= '&access_key='.urlencode($apikey);
		$baseUrl .= '&index1=combined&value1=';

		// FIXME: parsed differently when author handling is changed
		$surname = preg_replace('/^([^\s]+).*/', '\1', $metadata['authors']);

//		$searchStrings[] = $metadata['isbn'];				// TODO: requires searching index1=isbn
		$searchStrings[] = $metadata['authors']." ".$metadata['title']." ".$metadata['year'];
		$searchStrings[] = $surname." ".$metadata['title']." ".$metadata['year'];
		$searchStrings[] = $metadata['authors']." ".$metadata['title']." c".$metadata['year'];
		$searchStrings[] = $surname." ".$metadata['title']." c".$metadata['year'];
		$searchStrings[] = $metadata['authors']." ".$metadata['title'];
		$searchStrings[] = $surname." ".$metadata['title'];
		$searchStrings[] = $metadata['title']." ".$metadata['year'];
		$searchStrings[] = $metadata['title']." c".$metadata['year'];
		$searchStrings[] = $metadata['authors']." ".$metadata['year'];
		$searchStrings[] = $surname." ".$metadata['year'];
		$searchStrings[] = $metadata['authors']." c".$metadata['year'];
		$searchStrings[] = $surname." c".$metadata['year'];

		// clean and remove any empty or duplicate searches
		$searchStrings = array_map('strip_punc', $searchStrings);
		$searchStrings = array_unique($searchStrings);
		$searchStrings = array_clean($searchStrings);

		// create a temporary DOM document
		$tempDOM = new DOMDocument();
		$tempDOM->recover = true;			// try to handle non-well-formed responses

		// run the searches, in order, until we have a result
		do {
			$query = current($searchStrings);
			$tempDOM->loadXML(utf8_normalize(getCachedURL($baseUrl . urlencode($query))));

			$results = $tempDOM->getElementsByTagName('BookList')->item(0)->getAttribute('total_results');
			next($searchStrings);

		} while (key($searchStrings) !== null && empty($results));

		// if all attempts returned nothing, abort
		$item =& $tempDOM->getElementsByTagName('BookData')->item(0);
		if (empty($item)) {
			return array();
		}

		$isbn = $item->getAttribute('isbn13');
		// if we have an ISBN, get a metadata array for it
		if (!empty($isbn)) {
			return $this->extractISBNdb($isbn);
		} else {
			return array();
		}
	}

	// take an ISBN, return an array mapped to internal elements
	function extractISBNdb($isbn, $apikey = '') {

		// use ISBNdb to get XML metadata for the given ISBN ("results = texts" for extra information)
		$url = 'http://isbndb.com/api/books.xml?index1=isbn&results=details,authors';
		$url .= '&access_key='.urlencode($apikey);
		$url .= '&value1='.$isbn;

		// create a temporary DOM document
		$tempDOM = new DOMDocument();
		$tempDOM->recover = true;			// try to handle non-well-formed responses

		$tempDOM->loadXML(utf8_normalize(getCachedURL($url)));

		// create a new XSLT processor with PHP functions enabled
		$proc = new XsltProcessor();
		$proc->registerPHPFunctions();

		// transform the section using the XSL for this schema
		$xsl = dirname(__FILE__) . DS . 'isbndb.xsl';
		$xslDOM = new DOMDocument();
		$xslDOM->load($xsl);
		$proc->importStylesheet($xslDOM);
		$outDOM = $proc->transformToDoc($tempDOM);

		// reformat author list to match convention
		// TODO: fix when refactoring author lists
		$authors = '';
		foreach ($outDOM->getElementsByTagName('author') as $authorNode) {

			// parse author elements and rewrite
			$array = parse_author(trim_punc($authorNode->nodeValue));

			$authors .= strip_punc($array['surname'].' '.$array['forename'].' '.$array['initials']). ', ';
		}

		$metadata = xml_to_array($outDOM->documentElement, false);

		// clean up metadata array
		$metadata['authors'] = trim_punc($authors);
		$metadata['author'] = '';
		$metadata = array_map('trim_punc', $metadata);

		// remove errant publoc
		if ($metadata['publoc'] == $metadata['publisher']) unset($metadata['publoc']);

		// clean non-numerics from ISBN
		$metadata['isbn'] = $isbn;
		$metadata['isbn'] = preg_replace('/[^\dX]*/', '', $metadata['isbn']);

		// clean non-numerics from year
		$metadata['year'] = preg_replace('/[^\d{4}]/', '', $metadata['year']);

		// correct capitalization on title
		$metadata['title'] = title_case($metadata['title']);

		$metadata = array_clean($metadata);

		return $metadata;
	}

}
?>
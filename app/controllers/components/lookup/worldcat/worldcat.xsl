<?xml version="1.0"?>

<!--============================================

	worldcat.xsl - Crosswalk from MARC21XML to L8X citation elements (OpenURL 1.0)
	Part of Lemon8-XML; developed by the Public Knowledge Project   http://pkp.sfu.ca

	Based on mappings by Raymond Yee:
	http://www.raymondyee.net/wiki/MarcXmlToOpenUrlCrosswalk

	Copyright (c) 2008-2009 MJ Suhonos
	Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.

==============================================-->

<xsl:transform version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:marc="http://www.loc.gov/MARC21/slim"
	xmlns:php="http://php.net/xsl"
	exclude-result-prefixes="xsl marc php">

<xsl:output omit-xml-declaration='yes'/>

<xsl:strip-space elements="*"/>

<!--============================================
	START TRANSFORMATION AT THE ROOT NODE
==============================================-->
<xsl:template match="/marc:record">
	<citation>
		<xsl:apply-templates/>
	</citation>
</xsl:template>

<!-- Authors/Contributors -->
<xsl:template match="marc:datafield[@tag='100' or @tag='700']">
	<author>
		<xsl:choose>
			<xsl:when test="marc:subfield[@code='q']">
				<!-- strip everything after the comma and replace with full name -->
				<xsl:value-of select="php:functionString('preg_replace', '/,.*/', ', ', marc:subfield[@code='a'])"/>
				<xsl:value-of select="php:functionString('trim_punc', marc:subfield[@code='q'])"/>
			</xsl:when>
			<xsl:otherwise>
				<!--  just clean up what we have -->
				<xsl:value-of select="php:functionString('preg_replace', '/\./', '', marc:subfield[@code='a'])"/>
			</xsl:otherwise>
		</xsl:choose>
	</author>
</xsl:template>

<!-- Book title -->
<xsl:template match="marc:datafield[@tag='245'][1]">
	<title><xsl:value-of select="marc:subfield[@code='a']"/><xsl:text> </xsl:text><xsl:value-of select="marc:subfield[@code='b']"/></title>
</xsl:template>

<!-- Edition -->
<xsl:template match="marc:datafield[@tag='250'][1]">
	<edition><xsl:value-of select="marc:subfield[@code='a']"/></edition>
</xsl:template>

<!-- Publisher & Location -->
<xsl:template match="marc:datafield[@tag='260'][1]">
	<publoc><xsl:value-of select="marc:subfield[@code='a']"/></publoc>
	<publisher><xsl:value-of select="marc:subfield[@code='b']"/></publisher>
	<year><xsl:value-of select="php:functionString('preg_replace', '/,.*/', ', ', marc:subfield[@code='c'])"/></year>
</xsl:template>

<!-- ISBN not reliable, use xISBN service-->
<xsl:template match="marc:datafield[@tag='020'][1]">
	<isbn><xsl:value-of select="marc:subfield[@code='a']"/></isbn>
</xsl:template>

<!-- Journal information is in datafield 773 -->

<!-- Ignore everything else -->
<xsl:template match="*"/>

</xsl:transform>
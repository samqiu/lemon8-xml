<?php

/**
 * worldcat.php
 *
 * Copyright (c) 2008-2009 MJ Suhonos
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Uses the OCLC Worldcat Search API and xISBN services to search for book citation metadata
 *
 */

class WorldcatComponent extends Object
{
	// array of settings required for use
	var $settings = array('apikey' => 'WorldCat API key');

	// types of citation that can be processed
	var $types = array('book', 'proceedings', 'other');

	function lookup($metadata, $apikey) {

		// Worldcat Web search; results are (mal-formed) XHTML
		// TODO: might wish to change this if genre is book, etc. for advanced search
		// http://www.worldcat.org/search?qt=worldcat_org_bks&fq=dt%3Abks&qt=facet_dt%3A&q=';
		$baseUrl = 'http://www.worldcat.org/search?qt=worldcat_org_all&q=';

		// FIXME: parsed differently when author handling is changed
		$surname = preg_replace('/^([^\s]+).*/', '\1', $metadata['authors']);

		// NB: this might work better with book search above (or not)
		if (!empty($metadata['isbn'])) $searchStrings[] = $metadata['isbn'];
		$searchStrings[] = $surname." ".$metadata['title']." ".$metadata['year'];
		$searchStrings[] = $metadata['title']." ".$metadata['year'];
		$searchStrings[] = $surname." ".$metadata['year'];
		$searchStrings[] = $surname." ".$metadata['title'];

		// clean and remove any empty or duplicate searches
		$searchStrings = array_map('strip_punc', $searchStrings);
		$searchStrings = array_unique($searchStrings);
		$searchStrings = array_clean($searchStrings);

		// run the searches, in order, until we have a result
		do {
			$query = current($searchStrings);
			$result = getCachedURL($baseUrl . urlencode($query));

			// parse the OCLC numbers from search results
			preg_match_all('/id="itemid_(\d+)"/', $result, $matches);
			next($searchStrings);

		} while (key($searchStrings) !== null && empty($matches[1]));

		// if we have an OCLC number, get a metadata array for it
		if (!empty($matches[1])) {
			// use xISBN because it's free
			$isbns = $this->oclc_to_isbns($matches[1][0]);

			if (!empty($apikey)) {
				// you have an API key, so you get the cadillac
				$metadata = $this->extractWorldcat($matches[1][0], $apikey);

				// prefer ISBN from xISBN if possible
				if (!empty($isbns[0])) $metadata['isbn'] = $isbns[0];
				return $metadata;

			} else {
				// use the first ISBN if we have multiple
				return $this->extractxISBN($isbns[0]);
			}
		} else {
			return array();
		}
	}

	// take an OCLC ID and return the associated ISBNs as an array
	function oclc_to_isbns($oclcID) {
		$url = 'http://xisbn.worldcat.org/webservices/xid/oclcnum/';
		$url .= $oclcID;
		$url .= '?method=getMetadata&format=xml&fl=*';

		// create a temporary DOM document
		$tempDOM = new DOMDocument();
		$tempDOM->recover = true;			// try to handle non-well-formed responses

		$tempDOM->loadXML(utf8_normalize(getCachedURL($url)));

		// extract ISBN from response
		$oclcNode = $tempDOM->getElementsByTagName('oclcnum')->item(0);

		if (isset($oclcNode)) {
			return explode(' ', $oclcNode->getAttribute('isbn'));
		} else {
			return array();
		}
	}

	// take an ISBN, return an array mapped to internal elements
	function extractxISBN($isbn) {
		$url = 'http://xisbn.worldcat.org/webservices/xid/isbn/';
		$url .= $isbn;
		$url .= '?method=getMetadata&format=xml&fl=*';

		// create a temporary DOM document
		$tempDOM = new DOMDocument();
		$tempDOM->recover = true;			// try to handle non-well-formed responses

		$tempDOM->loadXML(utf8_normalize(getCachedURL($url)));

		// extract metadata from response
		$recordNode = $tempDOM->getElementsByTagName('isbn')->item(0);

		$metadata['isbn'] = $isbn;
		$metadata['year'] = $recordNode->getAttribute('year');
		$metadata['edition'] = $recordNode->getAttribute('ed');
		$metadata['title'] = $recordNode->getAttribute('title');
		$metadata['publisher'] = $recordNode->getAttribute('publisher');
		$metadata['publoc'] = $recordNode->getAttribute('city');

		// authors are kind of crappy from xISBN; at least compared to the MARC records
		$metadata['authors'] = $recordNode->getAttribute('author');

		$metadata = array_map('trim_punc', $metadata);
		$metadata = array_clean($metadata);

		// correct capitalization on title
		$metadata['title'] = title_case($metadata['title']);

		return $metadata;
	}

	// take an OCLC ID, return an array mapped to internal elements
	function extractWorldcat($oclcID, $apikey) {

		// result in MARCXML; this has better granularity than Dublin Core
		$url = 'http://www.worldcat.org/webservices/catalog/content/';
		$url .= $oclcID;
		$url .= '?wskey='.urlencode($apikey);

		// create a temporary DOM document
		$tempDOM = new DOMDocument();
		$tempDOM->recover = true;			// try to handle non-well-formed responses

		$tempDOM->loadXML(utf8_normalize(getCachedURL($url)));

		// create a new XSLT processor with PHP functions enabled
		$proc = new XsltProcessor();
		$proc->registerPHPFunctions();

		// transform the section using the XSL for this schema
		$xsl = dirname(__FILE__) . DS . 'worldcat.xsl';
		$xslDOM = new DOMDocument();
		$xslDOM->load($xsl);
		$proc->importStylesheet($xslDOM);
		$outDOM = $proc->transformToDoc($tempDOM);

		// reformat author list to match convention
		// TODO: fix when refactoring author lists
		$authors = '';
		foreach ($outDOM->getElementsByTagName('author') as $authorNode) {

			// parse author elements and rewrite
			$array = parse_author(trim_punc($authorNode->nodeValue));

			$authors .= strip_punc($array['surname'].' '.$array['forename'].' '.$array['initials']). ', ';
		}

		$metadata = xml_to_array($outDOM->documentElement, false);

		// clean up metadata array
		$metadata['authors'] = trim_punc($authors);
		$metadata['author'] = '';
		$metadata = array_map('trim_punc', $metadata);

		// clean non-numerics from ISBN
		if (!empty($metadata['isbn'])) $metadata['isbn'] = preg_replace('/[^\dX]*/', '', $metadata['isbn']);

		// clean non-numerics from year
		if (!empty($metadata['year'])) $metadata['year'] = preg_replace('/[^\d{4}]/', '', $metadata['year']);

		// correct capitalization on title
		if (!empty($metadata['title'])) $metadata['title'] = title_case($metadata['title']);

		$metadata = array_clean($metadata);

		return $metadata;
	}

}
?>
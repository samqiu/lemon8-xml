<?php

/**
 * crossref.php
 *
 * Copyright (c) 2008-2009 MJ Suhonos
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Uses the CrossRef OpenURL interface to search for journal article citation metadata
 *
 */

class CrossrefComponent extends Object
{
	// types of citation that can be processed
	// NB: books and proceedings haven't been well verified
	var $types = array('journal', 'proceedings', 'book');

	function lookup($metadata) {

		// if we have a DOI, use that directly
		// http://www.crossref.org/openurl/?noredirect=true&format=unixref&id=doi:10.2196/jmir.5.1.e2
		if (!empty($metadata['doi'])) {
			$baseUrl = 'http://www.crossref.org/openurl/?redirect=false&format=unixref&id=doi:';
			$query = urlencode(strip_punc($metadata['doi']));

		} else {
			// TODO: switch for books, using different elements

			// CrossRef OpenURL resolver
			$baseUrl = 'http://www.crossref.org/openurl/?url_ver=Z39.88-2004';
			$baseUrl .= '&redirect=false&format=unixref&rft_val_fmt=info:ofi/fmt:kev:mtx:journal';

			// good thing we use the OpenURL 1.0 elements internally...
			// not used: jtitle, stitle, pages, issn, eissn, artnum
			$elements = array('atitle', 'title', 'date', 'volume', 'issue', 'spage', 'epage',
											'aulast', 'aufirst', 'au');

			// build an array of openurl elements
			foreach ($elements as $element) {
				if (!empty($metadata[$element])) $openUrl[$element] = strip_punc($metadata[$element]);
			}

			// map title to short journal title (which is what we actually store)
			if (!empty($openUrl['title'])) {
				$openUrl['jtitle'] = $openUrl['title'];
				unset($openUrl['title']);
			}

			// map year to date if it's empty
			if (empty($openUrl['date']) && !empty($metadata['year'])) $openUrl['date'] = $metadata['year'];

			// TODO: ensure we have an author first and last name
			// NB: this is affected by manual edits and the author model
//			$surname = preg_replace('/^([^\s]+).*/', '\1', $metadata['authors']);

			// remove any empty elements and build the OpenURL
			$openUrl = array_clean($openUrl);
			$query = '';
			foreach ($openUrl as $element => $value) $query .= '&rft.'.$element.'='.urlencode($value);
		}

		// TODO: possibly a looping search like the others, but one usually gets it

		// create a temporary DOM document
		$tempDOM = new DOMDocument();
		$tempDOM->recover = true;			// try to handle non-well-formed responses

		$tempDOM->loadXML(utf8_normalize(getCachedURL($baseUrl . $query)));
//echo nl2br(htmlentities(getCachedURL($baseUrl . $query)));
		$result = $tempDOM->getElementsByTagName('error');

		// if we have a result, get a metadata array for it
		if ($result->length == 0) {
			return $this->extractCrossref($tempDOM);
		} else {
			return array();
		}
	}

	// take a CrossRef tagged result, return an array mapped to internal elements
	function extractCrossref($tempDOM) {
//echo nl2br(strtr(htmlentities($tempDOM->saveXML()), array(' ' => '&nbsp;&nbsp;&nbsp;')));

		// create a new XSLT processor with PHP functions enabled
		$proc = new XsltProcessor();
		$proc->registerPHPFunctions();

		// transform the section using the XSL for this schema
		$xsl = dirname(__FILE__) . DS . 'crossref.xsl';
		$xslDOM = new DOMDocument();
		$xslDOM->load($xsl);
		$proc->importStylesheet($xslDOM);
		$outDOM = $proc->transformToDoc($tempDOM);
//echo nl2br(strtr(htmlentities($outDOM->saveXML()), array(' ' => ' ')));

		// reformat author list to match convention
		// TODO: fix when refactoring author lists
		$authors = '';
		foreach ($outDOM->getElementsByTagName('author') as $authorNode) {

			// parse author elements and rewrite
			$array = parse_author(trim_punc($authorNode->nodeValue));

			// FIXME: this will throw warnings
			$authors .= strip_punc($array['surname'].' '.$array['forename'].' '.$array['initials']). ', ';
//echo '<br/>'.nl2br(print_r($authors, true));
		}

		$metadata = xml_to_array($outDOM->documentElement, false);

		// clean up metadata array
		$metadata['authors'] = trim_punc($authors);
		$metadata['author'] = '';
		$metadata = array_map('trim_punc', $metadata);

		// fix authors in all-caps
		if (strtoupper($metadata['authors']) == $metadata['authors']) $metadata['authors'] = ucwords(strtolower($metadata['authors']));

		// clean non-numerics from year
		if (!empty($metadata['year'])) $metadata['year'] = preg_replace('/[^\d{4}]/', '', $metadata['year']);

		// correct capitalization on title
		if (!empty($metadata['title'])) $metadata['title'] = title_case($metadata['title']);

		$metadata = array_clean($metadata);

		return $metadata;
	}

}
?>
<?xml version="1.0"?>

<!--============================================

	crossref.xsl - Crosswalk from CrossRef XML to L8X citation elements (OpenURL 1.0)
	Part of Lemon8-XML; developed by the Public Knowledge Project   http://pkp.sfu.ca

	Copyright (c) 2008-2009 MJ Suhonos
	Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.

==============================================-->

<xsl:transform version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:php="http://php.net/xsl"
	exclude-result-prefixes="xsl php">

<xsl:output omit-xml-declaration='yes'/>

<xsl:strip-space elements="*"/>

<!--============================================
	START TRANSFORMATION AT THE ROOT NODE
==============================================-->
<xsl:template match="/">
	<citation>
		<xsl:apply-templates select="doi_records/doi_record/crossref/*//*"/>
	</citation>
</xsl:template>

<!--============================================
	JOURNAL METADATA
==============================================-->

<!-- Journal short title -->
<xsl:template match="journal_metadata/full_title">
	<title>
		<xsl:choose>
			<xsl:when test="../abbrev_title != ''">
				<xsl:value-of select="../abbrev_title"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="."/>
			</xsl:otherwise>
		</xsl:choose>
	</title>
</xsl:template>

<!-- Issue -->
<xsl:template match="journal_issue/issue">
	<issue><xsl:value-of select="."/></issue>
</xsl:template>

<!-- Volume -->
<xsl:template match="journal_issue/journal_volume/volume">
	<volume><xsl:value-of select="."/></volume>
</xsl:template>

<!-- Article Title -->
<xsl:template match="journal_article/titles/title">
	<atitle><xsl:value-of select="."/></atitle>
</xsl:template>

<!-- Authors -->
<xsl:template match="journal_article/contributors/person_name">
	<author><xsl:value-of select="surname"/><xsl:text>, </xsl:text><xsl:value-of select="php:functionString('preg_replace', '/\. /', '', given_name)"/></author>
</xsl:template>

<!-- Year -->
<!--  more date info: month, day, season? perhaps store in <date/>-->
<xsl:template match="journal_article/publication_date/year">
	<year><xsl:value-of select="."/></year>
</xsl:template>

<!-- First Page -->
<xsl:template match="journal_article/pages/first_page">
	<spage><xsl:value-of select="."/></spage>
</xsl:template>

<!-- Last Page -->
<xsl:template match="journal_article/pages/last_page">
	<epage><xsl:value-of select="."/></epage>
</xsl:template>

<!-- DOI -->
<xsl:template match="doi_data/doi">
	<doi><xsl:value-of select="."/></doi>
</xsl:template>

<!-- URL; NB: may not be Open Access / Fulltext -->
<xsl:template match="doi_data/resource">
	<targetURL><xsl:value-of select="."/></targetURL>
</xsl:template>

<!-- 
More identifier info: ISSN, publisher eg SICI
 -->

<!--============================================
	BOOK METADATA
==============================================-->

<!-- Book Title -->
<xsl:template match="book_metadata/titles/title">
	<title><xsl:value-of select="."/></title>
</xsl:template>

<!-- Authors -->
<xsl:template match="book_metadata/contributors/person_name">
	<author><xsl:value-of select="given_name"/><xsl:text> </xsl:text><xsl:value-of select="surname"/></author>
</xsl:template>

<!-- Year -->
<!--  more date info: month, day, season? perhaps store in <date/>-->
<xsl:template match="book_metadata/publication_date/year">
	<year><xsl:value-of select="."/></year>
</xsl:template>

<!-- Volume -->
<!-- 
<xsl:template match="book_metadata/volume">
	<volume><xsl:value-of select="."/></volume>
</xsl:template>
 -->
 
<!-- Edition -->
<xsl:template match="book_metadata/edition_number">
	<edition><xsl:value-of select="."/></edition>
</xsl:template>

<!-- ISBN -->
<xsl:template match="book_metadata/isbn">
	<isbn><xsl:value-of select="."/></isbn>
</xsl:template>

<!-- Publisher -->
<xsl:template match="book_metadata/publisher/publisher_name">
	<publisher><xsl:value-of select="."/></publisher>
</xsl:template>

<!-- Publisher Location -->
<xsl:template match="book_metadata/publisher/publisher_place">
	<publoc><xsl:value-of select="."/></publoc>
</xsl:template>


<!-- Ignore everything else -->
<xsl:template match="*"/>

</xsl:transform>
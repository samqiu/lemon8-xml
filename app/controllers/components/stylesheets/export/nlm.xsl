<?xml version='1.0' encoding='utf-8'?>

<!--============================================

	export-nlm.xsl - stylesheet to transform Lemon8-XML data into well-formed NLM XML
	Part of Lemon8-XML; developed by the Public Knowledge Project   http://pkp.sfu.ca

	Copyright (c) 2008-2009 MJ Suhonos
	Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.

==============================================-->


<xsl:stylesheet version="1.0" 
    xmlns:xlink="http://www.w3.org/1999/xlink"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:str="http://exslt.org/strings"
	xmlns:php="http://php.net/xsl"
	extension-element-prefixes="str"
	exclude-result-prefixes="xsl xlink php">

<xsl:output method='xml' 
			version='1.0' 
			encoding='utf-8' 
			indent='yes'
			doctype-public="-//NLM//DTD Journal Publishing DTD v2.3 20070202//EN"
			doctype-system="http://dtd.nlm.nih.gov/publishing/2.3/journalpublishing.dtd"
			omit-xml-declaration='no'/>

<xsl:include href="str.tokenize.template.xsl"/>

<xsl:strip-space elements="*"/>

<!--============================================
	START TRANSFORMATION AT THE ROOT NODE
==============================================-->
<xsl:template match="/document">

<article xmlns:xlink="http://www.w3.org/1999/xlink" dtd-version="2.2" article-type="{metadata/*/Metadata/value[../element='article-type']}">
    <front>
        <journal-meta>
            <journal-id/>
			<journal-title/>
            <issn pub-type="epub"/>
        </journal-meta>
        <article-meta>
			<article-id pub-id-type="publisher-id"/>
			<article-categories>
				<subj-group subj-group-type="article-type">
					<subject/>
				</subj-group>
			</article-categories>

        	<!-- TODO: handle multiple article-id elements (update metadata schema) -->
			<xsl:if test="boolean(metadata/*/Metadata/value[../element='article-id' and . != ''])">
	            <article-id pub-id-type="{metadata/*/Metadata/value[../element='pub-id-type']}">
	            	    <xsl:value-of select="metadata/*/Metadata/value[../element='article-id']"/>
				</article-id>
			</xsl:if>

            <title-group>
                <article-title>
                	<!--  ASSUME we have an article-title (required by NLM) -->
	                <xsl:value-of select="metadata/*/Metadata/value[../element='article-title']"/>
				</article-title>
            </title-group>

			<!--  ASSUME we have an author (required by Lemon8) -->
            <contrib-group>
	            <xsl:apply-templates select="metadata/*[Metadata/element='author']" mode="author"/>
			</contrib-group>

			<xsl:for-each select="metadata/*[Metadata/element='affiliation']">
	            <aff id="aff{position()}">
	            	<label>
	            		<xsl:value-of select="position()"/>
	            	</label>

	                <xsl:for-each select="str:tokenize(children/*/Metadata/value[../element='contents'], '&#x9;&#xA;')">
		                <addr-line>
	    	                <xsl:value-of select="normalize-space(.)"/>	                	
						</addr-line>
        	        </xsl:for-each>

					<!--  ASSUME we have a country (required by Lemon8) -->
    	            <country>
						<xsl:value-of select="children/*/Metadata/value[../element='country']"/>
					</country>
				</aff>
			</xsl:for-each>
			<pub-date pub-type="epub">
				<day/>
				<month/>
				<year/>
			</pub-date>
			<volume/>
			<issue/>
			<history>
        		<date date-type="received">
          			<day/>
          			<month/>
          			<year/>
        		</date>
        		<date date-type="rev-request">
          			<day/>
          			<month/>
          			<year/>
        		</date>
        		<date date-type="rev-recd">
          			<day/>
          			<month/>
          			<year/>
        		</date>
        		<date date-type="accepted">
          			<day/>
          			<month/>
          			<year/>
        		</date>
			</history>
			<copyright-statement/>
			<copyright-year/>
			<self-uri/>

			<xsl:if test="boolean(metadata/*/Metadata/value[../element='abstract' and . != ''])">
				<abstract>
					<!-- FIXME: this is temporary until we define a transformation schema for abstracts -->
					<sec>
						<title/>
						<xsl:apply-templates select="metadata/*/Metadata/value[../element='abstract']"/>
					</sec>
				</abstract>
			</xsl:if>

			<xsl:if test="boolean(metadata/*/Metadata/value[../element='keywords' and . != ''])">
        	    <kwd-group>
	                <xsl:for-each select="str:tokenize(metadata/*/Metadata/value[../element='keywords'], ';,')">
	                	<kwd>
	    	                <xsl:value-of select="normalize-space(.)"/>
						</kwd>
        	        </xsl:for-each>
	            </kwd-group>
			</xsl:if>

        </article-meta>
    </front>
    <body>
    	<xsl:apply-templates select="sections/*" mode="section"/>
    </body>
    <back>
		<xsl:if test="boolean(metadata/*/Metadata/value[../element='conflicts' and . != ''])">
			<fn-group>
				<fn fn-type="conflict">
					<p>
						<xsl:apply-templates select="metadata/*/Metadata/value[../element='conflicts']"/>
					</p>
				</fn>
			</fn-group>
		</xsl:if>
		<ref-list>
			<xsl:apply-templates select="citations/*" mode="citation"/>
		</ref-list>
    </back>

<!--  testing  -->
<!-- 
			<TEST>
			    <xsl:copy-of select="."/>
			</TEST>
-->
<!--  end testing  -->

</article>
</xsl:template>

<xsl:template match="*" mode="author">
	<!-- TODO: add corresp="yes" to data schema -->
	<contrib contrib-type="author" id="contrib{position()}" xlink:type="simple">

		<!--  ASSUME we have a full name/surname (required by Lemon8) -->
		<name name-style="western">
			<surname><xsl:value-of select="children/*/Metadata/value[../element='surname']"/></surname>
			<given-names><xsl:value-of select="children/*/Metadata/value[../element='forename']"/></given-names>
		</name>

		<xsl:for-each select="str:tokenize(children/*/Metadata/value[../element='degrees'], ';,')">
			<degrees>
				<xsl:value-of select="normalize-space(.)"/>
			</degrees>
		</xsl:for-each>

		<!-- TODO: handle (remove/break) semicolon-separated aff strings -->
		<xsl:for-each select="children/*/Metadata/value[../element='aff' and . != '']">
			<xref ref-type="aff" rid="aff{position()}">
				<xsl:value-of select="position()"/>			
			</xref>
		</xsl:for-each>
	</contrib>
</xsl:template>

<xsl:template match="*" mode="section">
	<sec>
		<xsl:choose>
			<xsl:when test="Section/type = 'figure'">
				<title/>
				<!--  FIXME: this is temporary until we refactor figure handling (labels, caption) -->
				<fig position="float">
					<xsl:attribute name="id">
						fig<xsl:value-of select="Section/id"/>
					</xsl:attribute>
					<label>
						Figure <xsl:value-of select="Section/id"/>
					</label>
					<caption>
						<p><xsl:value-of select="Section/title"/></p>
					</caption>
					<graphic xlink:href="{Section/contents}" />
			    </fig>
			</xsl:when>
			<xsl:otherwise>
				<title><xsl:value-of select="Section/title"/></title>
				<xsl:apply-templates select="Section/contents"/>
			</xsl:otherwise>
		</xsl:choose>

		<xsl:apply-templates select="children/*" mode="section"/>

	</sec>
</xsl:template>

<xsl:template match="*" mode="citation">
		<ref id="ref{position()}">
			<label>
				<xsl:value-of select="position()"/>
			</label>

			<!-- TODO: make sure that all citations have been edited and have metadata -->
			<nlm-citation citation-type="{Metadata/*/value[../element='genre']}">

				<xsl:if test="boolean(Metadata/*/value[../element='authors' and . != ''])">
					<!-- TODO: handle institutions separately from individual authors-->
					<person-group person-group-type="author">
						<xsl:for-each select="str:tokenize(Metadata/*/value[../element='authors'], ',')">
							<name name-style="western">
								<xsl:if test="normalize-space(substring-before(normalize-space(.), ' ')) != ''">
									<surname>
										<xsl:value-of select="substring-before(normalize-space(.), ' ')"/>
									</surname>
								</xsl:if>
								<xsl:if test="normalize-space(substring-after(normalize-space(.), ' ')) != ''">
									<given-names>
										<xsl:value-of select="substring-after(normalize-space(.), ' ')"/>
									</given-names>
								</xsl:if>
							</name>
						</xsl:for-each>
					</person-group>
				</xsl:if>

				<xsl:if test="boolean(Metadata/*/value[../element='editor' and . != ''])">
					<person-group person-group-type="editor">
						<xsl:for-each select="str:tokenize(Metadata/*/value[../element='editor'], ',')">
							<name name-style="western">
								<xsl:if test="normalize-space(substring-before(normalize-space(.), ' ')) != ''">
									<surname>
										<xsl:value-of select="substring-before(normalize-space(.), ' ')"/>
									</surname>
								</xsl:if>
								<xsl:if test="normalize-space(substring-after(normalize-space(.), ' ')) != ''">
									<given-names>
										<xsl:value-of select="substring-after(normalize-space(.), ' ')"/>
									</given-names>
								</xsl:if>
							</name>
						</xsl:for-each>
					</person-group>
				</xsl:if>

				<xsl:if test="boolean(Metadata/*/value[../element='atitle' and . != ''])">
					<article-title>
						<xsl:value-of select="Metadata/*/value[../element='atitle']"/>
					</article-title>
				</xsl:if>
				<xsl:if test="boolean(Metadata/*/value[../element='title' and . != ''])">
					<source>
						<xsl:value-of select="Metadata/*/value[../element='title']"/>
					</source>
				</xsl:if>
				<xsl:if test="boolean(Metadata/*/value[../element='year' and . != ''])">
					<year>
						<xsl:value-of select="Metadata/*/value[../element='year']"/>
					</year>
				</xsl:if>
				<xsl:if test="boolean(Metadata/*/value[../element='month' and . != ''])">
					<month>
						<xsl:value-of select="Metadata/*/value[../element='month']"/>
					</month>
				</xsl:if>
				<xsl:if test="boolean(Metadata/*/value[../element='day' and . != ''])">
					<day>
						<xsl:value-of select="Metadata/*/value[../element='day']"/>
					</day>
				</xsl:if>
				<xsl:if test="boolean(Metadata/*/value[../element='volume' and . != ''])">
					<volume>
						<xsl:value-of select="Metadata/*/value[../element='volume']"/>
					</volume>
				</xsl:if>
				<xsl:if test="boolean(Metadata/*/value[../element='issue' and . != ''])">
					<issue>
						<xsl:value-of select="Metadata/*/value[../element='issue']"/>
					</issue>
				</xsl:if>
				<xsl:if test="boolean(Metadata/*/value[../element='publoc' and . != ''])">
					<publisher-loc>
						<xsl:value-of select="Metadata/*/value[../element='publoc']"/>
					</publisher-loc>
				</xsl:if>
				<xsl:if test="boolean(Metadata/*/value[../element='publisher' and . != ''])">
					<publisher-name>
						<xsl:value-of select="Metadata/*/value[../element='publisher']"/>
					</publisher-name>
				</xsl:if>
				<xsl:if test="boolean(Metadata/*/value[../element='spage' and . != ''])">
					<fpage>
						<xsl:value-of select="Metadata/*/value[../element='spage']"/>
					</fpage>
				</xsl:if>
				<xsl:if test="boolean(Metadata/*/value[../element='epage' and . != ''])">
					<lpage>
						<xsl:value-of select="Metadata/*/value[../element='epage']"/>
					</lpage>
				</xsl:if>

				<xsl:if test="boolean(Metadata/*/value[../element='comment' and . != '']) or boolean(Metadata/*/value[../element='targetURL' and . != ''])">
					<comment>
						<xsl:value-of select="Metadata/*/value[../element='comment']"/>
						<xsl:if test="boolean(Metadata/*/value[../element='targetURL'])">
							<ext-link ext-link-type="uri" xlink:href="{Metadata/*/value[../element='targetURL']}" xlink:type="simple">
								<xsl:value-of select="Metadata/*/value[../element='targetURL']"/>
							</ext-link>
						</xsl:if>
					</comment>
				</xsl:if>

				<xsl:if test="boolean(Metadata/*/value[../element='doi' and . != ''])">
					<pub-id pub-id-type="doi">
						<xsl:value-of select="Metadata/*/value[../element='doi']"/>
					</pub-id>
				</xsl:if>
				<xsl:if test="boolean(Metadata/*/value[../element='pmid' and . != ''])">
					<pub-id pub-id-type="pmid">
						<xsl:value-of select="Metadata/*/value[../element='pmid']"/>
					</pub-id>
				</xsl:if>

			</nlm-citation>
		</ref>
</xsl:template>


<!--============================================
	BODY / SECTION LEVEL TRANSFORMATIONS
==============================================-->


<!-- xsl parameters that describe id lists of interesting elements -->
<xsl:param name="bolds"/>
<xsl:param name="italics"/>
<xsl:param name="underlines"/>
<xsl:param name="superscripts"/>
<xsl:param name="subscripts"/>


<!--============================================
	BODY-LEVEL TRANSFORMS TO NLM-DTD
==============================================-->

<xsl:template match="reference">
	<xref ref-type="bibr" rid="ref{.}">
		<xsl:apply-templates/>
	</xref>
</xsl:template>

<xsl:template match="a">
	<ext-link xlink:href="{@href}">
		<xsl:apply-templates/>
	</ext-link>
</xsl:template>

<xsl:template match="h | p | s | span">

	<xsl:variable name="style-name">
		<xsl:choose>
			<xsl:when test="contains($bolds, concat(@style-name, ','))">bold</xsl:when>
			<xsl:when test="contains($italics, concat(@style-name, ','))">italic</xsl:when>
			<xsl:when test="contains($underlines, concat(@style-name, ','))">underline</xsl:when>
			<xsl:when test="contains($superscripts, concat(@style-name, ','))">sup</xsl:when>
			<xsl:when test="contains($subscripts, concat(@style-name, ','))">sub</xsl:when>
		</xsl:choose>
	</xsl:variable>

	<xsl:choose>
		<!-- throw away empty elements -->
		<xsl:when test="normalize-space(.)='' and count(child::*)=0"/>

		<!-- p's that are fully italicized are probably speech -->
		<xsl:when test="$style-name='italic' and (local-name()='p' or local-name()='h')">
			<speech>
				<!-- TODO: we should probably strip out a speaker if possible; in PHP? -->
				<speaker/>
				<p>
					<xsl:apply-templates/>
				</p>
			</speech>
		</xsl:when>

		<!-- if the element applies a style, transform it -->
		<xsl:when test="$style-name!='' and (local-name()='p' or local-name()='h')">
			<p>
			<xsl:element name="{$style-name}">
				<xsl:apply-templates/>
			</xsl:element>
			</p>
		</xsl:when>
		<xsl:when test="$style-name!=''">
			<xsl:element name="{$style-name}">
				<xsl:apply-templates/>
			</xsl:element>
		</xsl:when>

		<xsl:when test="local-name()='p' or local-name()='h'">
			<p>
				<xsl:apply-templates/>
			</p>
		</xsl:when>
		<xsl:otherwise>
			<xsl:apply-templates/>
		</xsl:otherwise>
	</xsl:choose>

</xsl:template>

<!-- marked figures -->

<xsl:template match="figure-wrap">
    <fig position="float">
		<xsl:attribute name="id">
			<xsl:value-of select=".//image/@name"/>
		</xsl:attribute>
		<xsl:apply-templates select="label"/>
		<xsl:apply-templates select="caption"/>
		<xsl:apply-templates select=".//image"/>
    </fig>
</xsl:template>

<xsl:template match="label">
	<label>
		<xsl:apply-templates/>
	</label>
</xsl:template>

<xsl:template match="caption">
    <caption>
		<xsl:choose>
			<xsl:when test=".//p or .//h">
				<xsl:apply-templates/>
			</xsl:when>
			<xsl:otherwise>
				<p>
					<xsl:apply-templates/>
				</p>
			</xsl:otherwise>
		</xsl:choose>
    </caption>
</xsl:template>

<xsl:template match="image">
    <graphic xlink:href="{@href}" />
</xsl:template>

<!-- lists -->
<xsl:template match="list">
	<!-- TODO: need to look at styles to determine @list-type -->
	<list>
		<xsl:apply-templates/>
	</list>
</xsl:template>

<xsl:template match="list-item">
	<list-item>
		<xsl:apply-templates select="p | list"/>
	</list-item>
</xsl:template>

<!-- TODO: note-citations within the text can be changed to xrefs -->
<xsl:template match="note-citation | note-ref | note-body">
	<xsl:apply-templates/>
</xsl:template>

<!--============================================
	TABLE TRANSFORMS
==============================================-->

<xsl:template match="table-wrap">
	<table-wrap>
		<xsl:apply-templates/>
	</table-wrap>
</xsl:template>

<xsl:template match="footer">
	<table-wrap-foot>
		<xsl:apply-templates/>
	</table-wrap-foot>
</xsl:template>

<xsl:template match="table">
	<xsl:if test="@is-sub-table = 'true'">
		<xsl:comment>TODO: this is a sub-table, please flatten me!</xsl:comment>
	</xsl:if>

	<table>
		<xsl:if test="table-column">
			<colgroup>
				<xsl:apply-templates select="table-column"/>
			</colgroup>
		</xsl:if>

		<xsl:if test="table-header-rows/table-row">
			<thead>
			<xsl:apply-templates
				select="table-header-rows/table-row"/>
			</thead>
		</xsl:if>

		<tbody>
			<xsl:apply-templates select="table-row"/>
		</tbody>
	</table>
</xsl:template>

<xsl:template match="table-column">
<col>
	<xsl:if test="@number-columns-repeated">
		<xsl:attribute name="span">
			<xsl:value-of select="@number-columns-repeated"/>
		</xsl:attribute>
	</xsl:if>

	<xsl:if test="@column-width">
		<xsl:attribute name="width">
			<xsl:value-of select="round(number(substring-before(@column-width,'in')) div number(substring-before(ancestor::table/@width,'in')) * 100)"/><xsl:text>%</xsl:text>
		</xsl:attribute>
	</xsl:if>

</col>
</xsl:template>

<xsl:template match="table-row">
<tr>
	<xsl:apply-templates select="table-cell"/>
</tr>
</xsl:template>

<xsl:template match="table-cell">
	<xsl:variable name="n">
		<xsl:choose>
			<xsl:when test="@number-columns-repeated != 0">
				<xsl:value-of select="@number-columns-repeated"/>
			</xsl:when>
			<xsl:otherwise>1</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:call-template name="process-table-cell">
		<xsl:with-param name="n" select="$n"/>
	</xsl:call-template>
</xsl:template>

<xsl:template name="process-table-cell">
	<xsl:param name="n"/>
	<xsl:if test="$n != 0">
		<td>
		<xsl:if test="@number-columns-spanned">
			<xsl:attribute name="colspan">
				<xsl:value-of select="@number-columns-spanned"/>
			</xsl:attribute>
		</xsl:if>
		<xsl:if test="@number-rows-spanned">
			<xsl:attribute name="rowspan">
				<xsl:value-of select="@number-rows-spanned"/>
			</xsl:attribute>
		</xsl:if>
		<xsl:apply-templates/>
		</td>
		<xsl:call-template name="process-table-cell">
			<xsl:with-param name="n" select="$n - 1"/>
		</xsl:call-template>
	</xsl:if>
</xsl:template>

</xsl:stylesheet>
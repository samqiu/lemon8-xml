<?xml version='1.0' encoding='utf-8'?>

<!--============================================

	odt-styles.xsl - Analyzes content.xml from a valid ODT XML document to determine
	which styles are applied and relevant to which content nodes.
	Part of Lemon8-XML; developed by the Public Knowledge Project   http://pkp.sfu.ca

	Copyright (c) 2008-2009 MJ Suhonos
	Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.

==============================================-->

<xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'
xmlns:style="urn:oasis:names:tc:opendocument:xmlns:style:1.0" 
xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0" 
xmlns:table="urn:oasis:names:tc:opendocument:xmlns:table:1.0" 
xmlns:draw="urn:oasis:names:tc:opendocument:xmlns:drawing:1.0" 
xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" 
xmlns:xlink="http://www.w3.org/1999/xlink" 
xmlns:dc="http://purl.org/dc/elements/1.1/" 
xmlns:meta="urn:oasis:names:tc:opendocument:xmlns:meta:1.0" 
xmlns:number="urn:oasis:names:tc:opendocument:xmlns:datastyle:1.0" 
xmlns:svg="urn:oasis:names:tc:opendocument:xmlns:svg-compatible:1.0" 
xmlns:chart="urn:oasis:names:tc:opendocument:xmlns:chart:1.0" 
xmlns:dr3d="urn:oasis:names:tc:opendocument:xmlns:dr3d:1.0" 
xmlns:math="http://www.w3.org/1998/Math/MathML" 
xmlns:form="urn:oasis:names:tc:opendocument:xmlns:form:1.0" 
xmlns:script="urn:oasis:names:tc:opendocument:xmlns:script:1.0" 
xmlns:ooo="http://openoffice.org/2004/office" 
xmlns:ooow="http://openoffice.org/2004/writer" 
xmlns:oooc="http://openoffice.org/2004/calc" 
xmlns:dom="http://www.w3.org/2001/xml-events" 
xmlns:xforms="http://www.w3.org/2002/xforms" 
xmlns:xsd="http://www.w3.org/2001/XMLSchema" 
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
exclude-result-prefixes="style text table draw fo xlink dc meta number svg chart dr3d math form script ooo ooow oooc dom xforms xsd xsi">

<xsl:output method='xml' version='1.0' encoding='utf-8' indent='no' omit-xml-declaration="yes"/>

<!-- match all style element descriptors-->
<xsl:template match="style:style">
	  <xsl:element name="style">
	    <!-- go process attributes and children -->
	    <xsl:apply-templates select="@*|node()"/>

		<!-- get a count of all non-empty content elements that use this style ID -->
		<count><xsl:value-of select="count(//*[(@text:style-name=current()/@style:name or @table:style-name=current()/@style:name) and . != ''])"/></count>

	  </xsl:element>
</xsl:template>

<xsl:template match="style:*[ancestor::style:style]">
	<!-- go process attributes and children -->
	<xsl:apply-templates select="@*|node()"/>
</xsl:template>

<xsl:template match="@*">
	<xsl:choose>

		<!-- we want to throw away these things, they're not useful for us -->
		<xsl:when test="contains(name(), 'asian')"/>
		<xsl:when test="contains(name(), 'complex')"/>
		<xsl:when test="contains(name(), 'country')"/>
		<xsl:when test="contains(name(), 'language')"/>
		<xsl:when test="contains(name(), 'color')"/>
		<xsl:when test="contains(name(), 'hyphenat')"/>
		<xsl:when test="contains(name(), 'font-family')"/>
		<xsl:when test="contains(name(), 'font-name')"/>
		<xsl:when test="contains(name(), 'line-height')"/>
		<xsl:when test="contains(name(), 'margin-top')"/>
		<xsl:when test="contains(name(), 'margin-bottom')"/>
		<xsl:when test="contains(name(), 'margin-left')"/>
		<xsl:when test="contains(name(), 'margin-right')"/>
		<xsl:when test="contains(name(), 'auto-text-indent')"/>
		<xsl:when test="contains(name(), 'text-indent')"/>
		<xsl:when test="contains(name(), 'start-indent')"/>
		<xsl:when test="contains(name(), 'end-indent')"/>

		<!-- we want to trap and keep these things, they are most useful -->
		<xsl:when test="name() = 'style:name'"><name><xsl:value-of select="."/></name></xsl:when>
		<xsl:when test="contains(name(), 'size')"><size><xsl:value-of select="."/></size></xsl:when>
		<xsl:when test="contains(name(), 'position')"><position><xsl:value-of select="."/></position></xsl:when>
		<xsl:when test="contains(name(), 'family')"><family><xsl:value-of select="."/></family></xsl:when>
		<xsl:when test="contains(name(), 'parent')"><parent><xsl:value-of select="."/></parent></xsl:when>
		<xsl:when test="contains(name(), 'underline')"><underline><xsl:value-of select="."/></underline></xsl:when>
		<xsl:when test="contains(name(), 'level')"><level><xsl:value-of select="."/></level></xsl:when>
		<xsl:when test="contains(name(), 'weight') and contains(., 'bold')"><weight><xsl:value-of select="."/></weight></xsl:when>
		<xsl:when test="contains(name(), 'style') and contains(., 'italic')"><italic><xsl:value-of select="."/></italic></xsl:when>

		<!-- these may or may not be useful, so we'll output them as extras -->
		<xsl:otherwise><extra><xsl:value-of select="name()"/><xsl:text>: </xsl:text><xsl:value-of select="."/></extra></xsl:otherwise>

	</xsl:choose>

	<!-- mark the number of text children in this element;
		 elements with no text children (likely?) have styles 
		 applied by their child elements				-->
	<xsl:if test="name() = 'style:name'">
		<xsl:variable name="current_name" select="."/>
		<text-children><xsl:value-of select="count(//*[@text:style-name = $current_name]/child::text())"/></text-children>
	</xsl:if>

</xsl:template>

<!-- suppress content elements -->
<xsl:template match="text:*"/>

</xsl:stylesheet>
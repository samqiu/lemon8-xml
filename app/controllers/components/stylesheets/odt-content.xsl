<?xml version='1.0' encoding='utf-8'?>

<!--============================================

	odt-content.xsl - Normalizes content.xml from a valid ODT XML document
	Part of Lemon8-XML; developed by the Public Knowledge Project   http://pkp.sfu.ca

	Copyright (c) 2008-2009 MJ Suhonos
	Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.

==============================================-->

<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
    xmlns:meta="urn:oasis:names:tc:opendocument:xmlns:meta:1.0"
    xmlns:config="urn:oasis:names:tc:opendocument:xmlns:config:1.0"
    xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0"
    xmlns:table="urn:oasis:names:tc:opendocument:xmlns:table:1.0"
    xmlns:draw="urn:oasis:names:tc:opendocument:xmlns:drawing:1.0"
    xmlns:presentation="urn:oasis:names:tc:opendocument:xmlns:presentation:1.0"
    xmlns:dr3d="urn:oasis:names:tc:opendocument:xmlns:dr3d:1.0"
    xmlns:chart="urn:oasis:names:tc:opendocument:xmlns:chart:1.0"
    xmlns:form="urn:oasis:names:tc:opendocument:xmlns:form:1.0"
    xmlns:script="urn:oasis:names:tc:opendocument:xmlns:script:1.0"
    xmlns:style="urn:oasis:names:tc:opendocument:xmlns:style:1.0"
    xmlns:number="urn:oasis:names:tc:opendocument:xmlns:datastyle:1.0"
    xmlns:anim="urn:oasis:names:tc:opendocument:xmlns:animation:1.0"

    xmlns:dc="http://purl.org/dc/elements/1.1/"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns:math="http://www.w3.org/1998/Math/MathML"
    xmlns:xforms="http://www.w3.org/2002/xforms"

	xmlns:dom="http://www.w3.org/2001/xml-events" 
	xmlns:xsd="http://www.w3.org/2001/XMLSchema" 
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"

    xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0"
    xmlns:svg="urn:oasis:names:tc:opendocument:xmlns:svg-compatible:1.0"
    xmlns:smil="urn:oasis:names:tc:opendocument:xmlns:smil-compatible:1.0"
	
	xmlns:ooo="http://openoffice.org/2004/office"
	xmlns:ooow="http://openoffice.org/2004/writer"
	xmlns:oooc="http://openoffice.org/2004/calc">

<xsl:output method='xml' version='1.0' encoding='utf-8' indent='yes' omit-xml-declaration="yes"/>

<!-- xsl parameters that describe id lists of interesting elements -->
<xsl:param name="headings_1"/>
<xsl:param name="headings_2"/>
<xsl:param name="headings_3"/>
<xsl:param name="headings_4"/>
<xsl:param name="headings_5"/>
<xsl:param name="bolds"/>
<xsl:param name="italics"/>
<xsl:param name="underlines"/>
<xsl:param name="superscripts"/>
<xsl:param name="subscripts"/>

<xsl:key name="listTypes" match="text:list-style" use="@style:name"/>

<xsl:variable name="all-headings" select="concat($headings_1, $headings_2, $headings_3, $headings_4, $headings_5)"/>
<xsl:variable name="all-important" select="concat($all-headings, $bolds, $italics, $underlines, $superscripts, $subscripts)"/>

<!--============================================
	START TRANSFORMATION AT THE DOCUMENT LEVEL
==============================================-->

<xsl:template match="/office:document-content">
	<content>
		<xsl:apply-templates select="office:body/office:text/*"/>
		<xsl:apply-templates select="//text:note" mode="endnotes"/>
	</content>
</xsl:template>


<!-- suppress elements that contain line breaks, handle them below -->
<xsl:template match="*[descendant::text:line-break]">
	<!-- if the first element is text, then copy that -->
	<!--
		<text:p text:style-name="Standard">
		1. INSA, Instituto Ricardo Jorge, Inquérito Nacional de Saúde, 1998/99. <text:span text:style-name="T2">
		Lisboa <text:line-break/>
		</text:span>
		</text:p>

		<span style-name="T2">Lisboa </span>
	-->

	<xsl:if test="text() and local-name() = 'p'">
		<xsl:element name="p">
			<xsl:if test="contains($headings_1, concat(@text:style-name, ','))"><xsl:attribute name="heading">1</xsl:attribute></xsl:if>
			<xsl:if test="contains($headings_2, concat(@text:style-name, ','))"><xsl:attribute name="heading">2</xsl:attribute></xsl:if>
			<xsl:if test="contains($headings_3, concat(@text:style-name, ','))"><xsl:attribute name="heading">3</xsl:attribute></xsl:if>
			<xsl:if test="contains($headings_4, concat(@text:style-name, ','))"><xsl:attribute name="heading">4</xsl:attribute></xsl:if>
			<xsl:if test="contains($headings_5, concat(@text:style-name, ','))"><xsl:attribute name="heading">5</xsl:attribute></xsl:if>

			<xsl:apply-templates select="@*"/>
			<xsl:value-of select="text()"/>
		</xsl:element>
	</xsl:if>

	<!-- if the line break is the last element, process normally (ignore) -->
	<xsl:apply-templates select="*"/>

</xsl:template>

<!-- catch line breaks and transform into parent elements -->
<xsl:template match="*/text:line-break">
    <p>
		<xsl:attribute name="style-name">
			<xsl:value-of select="parent::*/@text:style-name"/>
		</xsl:attribute>

      <xsl:apply-templates 
      select="preceding-sibling::node()[not(self::text:line-break)]
                       [generate-id(following-sibling::text:line-break)
                       = generate-id(current())]"/>
	</p>
	<!--
    <xsl:if test="position() = last()">
      <xsl:element name="group{position() + 1}">
        <xsl:apply-templates select="following-sibling::node()"/>
      </xsl:element>
    </xsl:if>
	-->
</xsl:template>

<!-- catch spans -->
<xsl:template match="text:span">
	<!-- if the span applies a style, copy it -->
	<xsl:choose>
		<xsl:when test="contains($all-important, concat(@text:style-name, ',')) and @text:style-name != ''">
			  <xsl:element name="{local-name()}">
				<!-- mark styles on this element -->
				<xsl:if test="contains($bolds, concat(@text:style-name, ','))"><xsl:attribute name="bold"/></xsl:if>
				<xsl:if test="contains($italics, concat(@text:style-name, ','))"><xsl:attribute name="italic"/></xsl:if>
				<xsl:if test="contains($underlines, concat(@text:style-name, ','))"><xsl:attribute name="underline"/></xsl:if>
				<xsl:if test="contains($superscripts, concat(@text:style-name, ','))"><xsl:attribute name="super"/></xsl:if>
				<xsl:if test="contains($subscripts, concat(@text:style-name, ','))"><xsl:attribute name="sub"/></xsl:if>

				<xsl:if test="contains($headings_1, concat(@text:style-name, ','))"><xsl:attribute name="heading">1</xsl:attribute></xsl:if>
				<xsl:if test="contains($headings_2, concat(@text:style-name, ','))"><xsl:attribute name="heading">2</xsl:attribute></xsl:if>
				<xsl:if test="contains($headings_3, concat(@text:style-name, ','))"><xsl:attribute name="heading">3</xsl:attribute></xsl:if>
				<xsl:if test="contains($headings_4, concat(@text:style-name, ','))"><xsl:attribute name="heading">4</xsl:attribute></xsl:if>
				<xsl:if test="contains($headings_5, concat(@text:style-name, ','))"><xsl:attribute name="heading">5</xsl:attribute></xsl:if>

			    <!-- go process attributes and children -->
			    <xsl:apply-templates select="@*|node()"/>
			  </xsl:element>
		</xsl:when>
		<!-- if the span is empty, make a space -->
		<xsl:when test="normalize-space(.)=''">
			<xsl:text> </xsl:text>
		</xsl:when>
		<!-- else pass through the span -->
		<xsl:otherwise>
			<xsl:apply-templates/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<!-- catch p and h; strip empty ones -->
<xsl:template match="text:p | text:h">
	<xsl:choose>
		<xsl:when test="normalize-space(.)=''"/>
		<xsl:otherwise>
			  <xsl:element name="{local-name()}">
				<!-- mark styles on this element -->
				<xsl:if test="contains($bolds, concat(@text:style-name, ','))"><xsl:attribute name="bold"/></xsl:if>
				<xsl:if test="contains($italics, concat(@text:style-name, ','))"><xsl:attribute name="italic"/></xsl:if>
				<xsl:if test="contains($underlines, concat(@text:style-name, ','))"><xsl:attribute name="underline"/></xsl:if>
				<xsl:if test="contains($superscripts, concat(@text:style-name, ','))"><xsl:attribute name="super"/></xsl:if>
				<xsl:if test="contains($subscripts, concat(@text:style-name, ','))"><xsl:attribute name="sub"/></xsl:if>

				<xsl:if test="contains($headings_1, concat(@text:style-name, ','))"><xsl:attribute name="heading">1</xsl:attribute></xsl:if>
				<xsl:if test="contains($headings_2, concat(@text:style-name, ','))"><xsl:attribute name="heading">2</xsl:attribute></xsl:if>
				<xsl:if test="contains($headings_3, concat(@text:style-name, ','))"><xsl:attribute name="heading">3</xsl:attribute></xsl:if>
				<xsl:if test="contains($headings_4, concat(@text:style-name, ','))"><xsl:attribute name="heading">4</xsl:attribute></xsl:if>
				<xsl:if test="contains($headings_5, concat(@text:style-name, ','))"><xsl:attribute name="heading">5</xsl:attribute></xsl:if>

			    <!-- go process attributes and children -->
			    <xsl:apply-templates select="@*|node()"/>
			  </xsl:element>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<!-- flatten sections -->
<xsl:template match="text:section">
	<xsl:choose>
		<xsl:when test="normalize-space(.)=''"/>
		<xsl:otherwise>
			<xsl:apply-templates/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<!-- mark p's that contain an image -->
<xsl:template match="text:p[descendant::draw:*]">
	<p>
		<!-- mark styles on this element -->
		<xsl:if test="contains($bolds, concat(@text:style-name, ','))"><xsl:attribute name="bold"/></xsl:if>
		<xsl:if test="contains($italics, concat(@text:style-name, ','))"><xsl:attribute name="italic"/></xsl:if>
		<xsl:if test="contains($underlines, concat(@text:style-name, ','))"><xsl:attribute name="underline"/></xsl:if>
		<xsl:if test="contains($superscripts, concat(@text:style-name, ','))"><xsl:attribute name="super"/></xsl:if>
		<xsl:if test="contains($subscripts, concat(@text:style-name, ','))"><xsl:attribute name="sub"/></xsl:if>

		<xsl:if test="contains($headings_1, concat(@text:style-name, ','))"><xsl:attribute name="heading">1</xsl:attribute></xsl:if>
		<xsl:if test="contains($headings_2, concat(@text:style-name, ','))"><xsl:attribute name="heading">2</xsl:attribute></xsl:if>
		<xsl:if test="contains($headings_3, concat(@text:style-name, ','))"><xsl:attribute name="heading">3</xsl:attribute></xsl:if>
		<xsl:if test="contains($headings_4, concat(@text:style-name, ','))"><xsl:attribute name="heading">4</xsl:attribute></xsl:if>
		<xsl:if test="contains($headings_5, concat(@text:style-name, ','))"><xsl:attribute name="heading">5</xsl:attribute></xsl:if>

		<xsl:for-each select="@*">
			<xsl:attribute name="{local-name()}">
				<xsl:value-of select="."/>
			</xsl:attribute>
		</xsl:for-each>
		<xsl:attribute name="image">true</xsl:attribute>

		<xsl:apply-templates/>
	</p>

</xsl:template>

<!-- flatten images -->
<xsl:template match="draw:*">
	<image>
		<xsl:apply-templates select="descendant-or-self::*/@*"/>
	</image>
</xsl:template>

<!-- catch endnotes and only pass through content, not ref data -->
<xsl:template match="text:note">
	<xsl:apply-templates select="text:note-citation"/>
</xsl:template>

<xsl:template match="text:note" mode="endnotes">
	<note id="{@text:id}">
		<xsl:apply-templates/>
	</note>
</xsl:template>

<!-- capture styles for tables, and copy attributes to the element -->
<xsl:template match="table:table | table:table-column">
	<xsl:variable name="style-name" select="@table:style-name"/>
	<xsl:variable name="style" select="//style:style[@style:name = $style-name]"/>

	<xsl:element name="{local-name()}">
		<xsl:apply-templates select="@*"/>
		<xsl:apply-templates select="$style//@*"/>
		<xsl:apply-templates/>
	</xsl:element>
</xsl:template>


<!-- When processing a list, you have to look at the parent style *and* level of nesting -->
<xsl:template match="text:list">
	<xsl:variable name="level" select="count(ancestor::text:list)+1"/>
	
	<!-- the list class is the @text:style-name of the outermost <text:list> element -->
	<xsl:variable name="listClass">
		<xsl:choose>
			<xsl:when test="$level=1">
				<xsl:value-of select="@text:style-name"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="
					ancestor::text:list[last()]/@text:style-name"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	
	<!-- Now select the <text:list-level-style-foo> element at this level of nesting for this list -->
	<xsl:variable name="node" select="key('listTypes', $listClass)/*[@text:level='$level']"/>

	<!-- emit appropriate list type -->
	<xsl:variable name="style-name" select="@text:style-name"/>
	<xsl:variable name="style" select="//style:style[@style:list-style-name = $style-name]"/>

	<xsl:element name="{local-name()}">
		<xsl:apply-templates select="@*"/>
		<xsl:apply-templates select="$style//@*"/>
		<xsl:apply-templates/>
	</xsl:element>

<!--
	<xsl:choose>
		<xsl:when test="local-name($node)='list-level-style-number'">
			<ol class="{concat($listClass,'_',$level)}">
				<xsl:apply-templates/>
			</ol>
		</xsl:when>
		<xsl:otherwise>
			<ul class="{concat($listClass,'_',$level)}">
				<xsl:apply-templates/>
			</ul>
		</xsl:otherwise>
	</xsl:choose>
-->
</xsl:template>

<!--
	<xsl:template match="text:list-level-style-bullet">
		<xsl:text>.</xsl:text>
		<xsl:value-of select="../@style:name"/>
		<xsl:text>_</xsl:text>
		<xsl:value-of select="@text:level"/>
		<xsl:text>{ list-style-type: </xsl:text>
		<xsl:text>bullet</xsl:text>
		<xsl:text>;}</xsl:text>
		<xsl:value-of select="$lineBreak"/>
	</xsl:template>


	<xsl:template match="text:list-level-style-number">
		<xsl:text>.</xsl:text>
		<xsl:value-of select="../@style:name"/>
		<xsl:text>_</xsl:text>
		<xsl:value-of select="@text:level"/>
		<xsl:text>{ list-style-type: </xsl:text>
		<xsl:choose>
			<xsl:when test="@style:num-format='1'">order</xsl:when>
			<xsl:when test="@style:num-format='I'">roman-upper</xsl:when>
			<xsl:when test="@style:num-format='i'">roman-lower</xsl:when>
			<xsl:when test="@style:num-format='A'">alpha-upper</xsl:when>
			<xsl:when test="@style:num-format='a'">alpha-lower</xsl:when>
			<xsl:otherwise>decimal</xsl:otherwise>
		</xsl:choose>
		<xsl:text>;}</xsl:text>
		<xsl:value-of select="$lineBreak"/>
	</xsl:template>
-->

<!-- throw away empty elements -->
<xsl:template match="*[normalize-space(.)='' and count(child::*)=0 and name()!='text:line-break' and name()!='table:table-column']"/>

<!--============================================
	identity transform everything else, but strip namespaces
==============================================-->
<xsl:template match="/|comment()|processing-instruction()">
  <xsl:copy>
    <!-- go process children (applies to root node only) -->
    <xsl:apply-templates/>
  </xsl:copy>
</xsl:template>

<xsl:template match="*">
  <xsl:element name="{local-name()}">
		<!-- mark styles on this element -->
		<xsl:if test="contains($bolds, concat(@text:style-name, ','))"><xsl:attribute name="bold"/></xsl:if>
		<xsl:if test="contains($italics, concat(@text:style-name, ','))"><xsl:attribute name="italic"/></xsl:if>
		<xsl:if test="contains($underlines, concat(@text:style-name, ','))"><xsl:attribute name="underline"/></xsl:if>
		<xsl:if test="contains($superscripts, concat(@text:style-name, ','))"><xsl:attribute name="super"/></xsl:if>
		<xsl:if test="contains($subscripts, concat(@text:style-name, ','))"><xsl:attribute name="sub"/></xsl:if>

		<xsl:if test="contains($headings_1, concat(@text:style-name, ','))"><xsl:attribute name="heading">1</xsl:attribute></xsl:if>
		<xsl:if test="contains($headings_2, concat(@text:style-name, ','))"><xsl:attribute name="heading">2</xsl:attribute></xsl:if>
		<xsl:if test="contains($headings_3, concat(@text:style-name, ','))"><xsl:attribute name="heading">3</xsl:attribute></xsl:if>
		<xsl:if test="contains($headings_4, concat(@text:style-name, ','))"><xsl:attribute name="heading">4</xsl:attribute></xsl:if>
		<xsl:if test="contains($headings_5, concat(@text:style-name, ','))"><xsl:attribute name="heading">5</xsl:attribute></xsl:if>

		<!-- go process attributes and children -->
		<xsl:apply-templates select="@*|node()"/>
  </xsl:element>

	<!-- output line breaks for readability -->
	<xsl:text>
	</xsl:text>

</xsl:template>

<xsl:template match="@*">
  <xsl:attribute name="{local-name()}">
    <xsl:value-of select="."/>
  </xsl:attribute>
</xsl:template>
<!--============================================
 	end of identity transform
==============================================-->

</xsl:stylesheet>

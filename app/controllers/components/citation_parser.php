<?php

/**
 * citation_parser.php
 *
 * Copyright (c) 2008-2009 MJ Suhonos
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Methods to apply parsing and lookup components on citation data
 *
 */

class CitationParserComponent extends Object
{
 	var $components = array('Session');

	// take in a citation string, pass through parser components, and return a populated array
	function parse($citation) {

		// clean citation to UTF-8
		$citation = utf8_normalize($citation);
		$citation = trim(stripslashes($citation));

		// get list of parser components available
		foreach (AppController::getComponents('parse') as $component) {
			// if the component is enabled, add it to the list
			if($this->Session->read('Settings.parse.component.'.Inflector::underscore($component))) {
				// get parameters if they exist
				$params = $this->Session->read('Settings.parse.'.Inflector::underscore($component));
				$parsers[] = array('parse' => $component, 'params' => $params);
			}
		}

		// go through each parser and generate metadata
		foreach ($parsers as $component) {
			// instantiate parser object
			$parser = AppController::enableComponent($component['parse']);

			// if parameters are supplied, pass them along
			if (isset($component['params']) && is_array($component['params'])) {
				$params = array_merge(array($citation), $component['params']);
				$result = array_clean(call_user_func_array(array(&$parser, 'parse'), $params));
			} else {
				$component['params'] = array();
				$result = array_clean($parser->parse($citation));
			}
			$id = $component['parse'];

			// if the array is numeric, we have multiple results
			// NB: this is a hack for the paracite parser with multiple results
			if (is_numeric(key($result))) {
				foreach ($result as $key => $multiresult) {
					$id = $component['parse'].$key;

					// if the genre is not set, take a guess for scoring purposes
					if (!isset($multiresult['genre'])) {
						$genre = $this->guessGenre($multiresult);
						if ($genre != 'other' ) $multiresult['genre'] = $genre;
					}

					// calculate the score for this parser
					$multiresult['parse_score'] = $this->parseScore($multiresult);
					$scoring_array[$id] = $this->parseScore($multiresult);

					$newArray[$id] = $multiresult;

					// add each element from the parser to the array of values
					foreach ($multiresult as $element => $value) {
						$parsed[$element][$id] = $value;
					}
				}
				continue;
			}
			// END CRAZY HACK

			// if the genre is not set, take a guess for scoring purposes
			if (!isset($result['genre'])) {
				$genre = $this->guessGenre($result);
				if ($genre != 'other' ) $result['genre'] = $genre;
			}

			// calculate the score for this parser
			$result['parse_score'] = $this->parseScore($result);
			$scoring_array[$id] = $this->parseScore($result);

			$newArray[$id] = $result;

			// add each element from the parser to the array of values
			foreach ($result as $element => $value) {
				$parsed[$element][$id] = $value;
			}
		}

		$average_score = array_sum($scoring_array) / count($scoring_array);
		$max_score = max($scoring_array);
		$parse_score = ($max_score + $average_score) / 2;

		// get a single set of "best" values for the citation
		$best = $this->guessValues($parsed, $scoring_array);

		// assemble into a single array to return
		return array('best' => $best, 'elements' => $parsed, 'parse_score' => $parse_score);
	}

	// derive a confidence score based on elements for each genre; based on the OpenURL 0.1 schema
	function parseScore($metadata) {

		// suppress warnings
		if (empty($metadata['genre'])) $metadata['genre'] = '';

		switch($metadata['genre']) {
			case 'book':
				$elements = array('authors', 'year', 'publisher', 'publoc', 'title', 'spage', 'epage', 'genre');
				break;
			case 'journal':
				$elements = array('atitle', 'authors', 'year', 'issue', 'volume', 'title', 'spage', 'epage', 'genre');
				break;
			default:
				$elements = array('atitle', 'authors', 'year', 'issue', 'volume', 'publisher', 'publoc', 'title', 'spage', 'epage', 'genre');
		}

		// TODO:  need to include a measure of % elements marked (ie. book citation w/o pages)
		$parsed_elements = count(array_intersect($elements, array_keys($metadata)));
		$genre_elements = count($elements);
		$parse_score = min((($parsed_elements / $genre_elements)*100), 100);

		return $parse_score;
	}

	// try to guess a citation's genre based on detected elements
	function guessGenre($metadata) {
		// take a guess at the genre if it's not detected
		if (!isset($metadata['genre'])
			&& isset($metadata['volume']) && isset($metadata['issue'])
			&& isset($metadata['title'])
			&& isset($metadata['authors']) && isset($metadata['year']) ) {
			return 'journal';

		} elseif (!isset($metadata['genre'])
			&& isset($metadata['publisher']) && isset($metadata['publoc'])
			&& isset($metadata['title']) && isset($metadata['year'])
			&& (isset($metadata['authors']) || isset($metadata['editor']) )) {
			return 'book';

		} elseif (!isset($metadata['genre'])) {
			return 'other';
		} else {
			return $metadata['genre'];
		}
	}

	// take a multiple array of parse/lookup results and derive one set of values
	function guessValues($values, $scores) {
/*
echo '<table border="1">';
	echo '<tr>';
	echo '<th></th>';
	foreach ($scores as $component => $score) {
		echo '<th>';
		if (!empty($component)) echo $component;
		echo '</th>';
	}
	echo '</tr>';

knatsort($values);
foreach ($values as $element => $dummy) {
	echo '<tr>';
	echo '<td><b>'.$element.'</b></td>';
	foreach ($scores as $component => $dummy2) {
		echo '<td>';
		if (!isset($values[$element][$component])) $values[$element][$component] = '';
		echo '"'.$values[$element][$component].'"';
		echo '</td>';
	}
	echo '</tr>';
}
echo '</table>';
*/
		// NB: if there is a tie for max score, this is an ambiguous choice
		$max_score_index = array_search(max($scores), $scores);

		// choose values based on frequency
		foreach (array_keys($values) as $element) {
			// suppress warnings, don't count scores
			if (strpos($element, '_score')) continue;

			$freq = array_count_values(array_clean(array_values($values[$element])));

			// intelligently re-order frequency values
			natsort($freq);
			$freq = array_reverse($freq, true);

			// NB: careful here; reset() has to be called before key()
			$val_freq[$element] = array('freq' => reset($freq), 'value' => key($freq));
		}

		foreach ($val_freq as $element => $data) {
			// only include values from lookup results above the threshold
			if (array_key_exists('lookup_score', $values) && max($scores) < $this->Session->read('Settings.lookup_margin')) {
				continue;
			}

			// if values differ, choose the "best" one; prefer multiple results over score
			if ($data['freq'] > 1 || ($data['freq'] != '' && $values[$element][$max_score_index] == '') ) {
				// NB: this can potentially include result elements that are below lookup threshold
				// this is mitigated by more lookup services
				$result[$element] = $data['value'];
			} else {
				// default to highest score
				$result[$element] = $values[$element][$max_score_index];
			}
		}

		return $result;
	}

	// take in a parsed citation, pass through lookup components, and return a populated array
	function lookup($cite_metadata) {

		// TODO: perhaps normalize to ASCII for lookups?

		// reformat metdata to element-value pairs
		foreach ($cite_metadata as $element => $data) {
			$metadata[$element] = $data['value'];
		}

		// get list of lookup components available
		foreach (AppController::getComponents('lookup') as $component) {
			// if the component is enabled, add it to the list
			if($this->Session->read('Settings.lookup.component.'.Inflector::underscore($component))) {
				// get parameters if they exist
				$params = $this->Session->read('Settings.lookup.'.Inflector::underscore($component));
				$lookupComponents[] = array('lookup' => $component, 'params' => $params);
			}
		}

		// go through each lookup component and retrieve metadata
		$mergedMetadata = $metadata;
		$retrieved = array();
		foreach ($lookupComponents as &$component) {
			// instantiate lookup object
			$lookup =& AppController::enableComponent($component['lookup']);

			// only perform a lookup if this citation type is supported
			if (in_array($metadata['genre'], $lookup->types)) {

				// if parameters are supplied, pass them along
				if (isset($component['params']) && is_array($component['params'])) {
					$params = array_merge(array($metadata), $component['params']);
					$result = array_clean(call_user_func_array(array(&$lookup, 'lookup'), $params));
				} else {
					$component['params'] = array();
					$result = array_clean($lookup->lookup($metadata));
				}

				if (!empty($result)) {
					$mergedMetadata = array_merge($mergedMetadata, $result);
					$id = $component['lookup'];

					// calculate the score for this lookup
					// NB: this averages two comparisons to even results
					$result['lookup_score'] = ($this->lookupScore($metadata, $result) + $this->lookupScore($metadata, $mergedMetadata)) / 2;
					$scoring_array[$id] = $result['lookup_score'];

					$newArray[$id] = $result;

					// add each element from the lookup to the array of values
					foreach ($result as $element => $value) {
						$retrieved[$element][$id] = $value;
					}
				}
			}
		}

		// calculate an overall lookup score across all components
		$average_score = array_sum($scoring_array) / count($scoring_array);
		$max_score = max($scoring_array);
		$lookup_score = ($max_score + $average_score) / 2;

		// get a single set of "best" values for the citation
		$best = $this->guessValues($retrieved, $scoring_array);

		// assemble into a single array to return
		return array('best' => $best, 'elements' => $retrieved, 'lookup_score' => $lookup_score);
	}

	// derive a % difference based on initial and lookup metadata
	function lookupScore($metadata, $lookupData) {

		// don't include these elements in the calculation
		unset($lookupData['targetURL']);
		unset($lookupData['marked']);
		unset($lookupData['comment']);
		unset($lookupData['access']);
		unset($lookupData['genre']);

		// identifiers
		// unsetting these will increase the lookup score
		unset($lookupData['doi']);
		unset($lookupData['isbn']);
		unset($lookupData['pmid']);

		$tmpdata = array_clean($lookupData);

		$similar = 0;
		foreach ($tmpdata as $element => $value) {
			$percent = '';
			similar_text($value, $metadata[$element], $percent);
			$similar += $percent;
		}

		$diff = $similar / count($tmpdata);
/*
echo '<table border="1">';
	foreach ($lookupData as $element => $value) {
		echo '<tr>';
		echo '<td><b>'.$element.'</b></td>';
		echo '<td>'.$metadata[$element].'</td>';
		echo '<td>'.$value.'</td>';
		echo '</tr>';
	}
	echo '<tr><td><b>DIFF</b></td><th colspan="2" align="center">'.$diff.'</th></tr>';
echo '</table>';
*/
		return $diff;
	}

}

?>
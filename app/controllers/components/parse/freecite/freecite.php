<?php

/**
 * freecite.php
 *
 * Copyright (c) 2008-2009 MJ Suhonos
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Uses the FreeCite web service to break a citation into metadata elements
 *
 */

class FreeciteComponent extends Object
{

	function parse($citationText) {

		// strip newline and tab characters
		$citationText = preg_replace('/[\r\n\s]/', ' ', $citationText);

		// Paracit web form; results are (mal-formed) HTML
		$baseUrl = 'http://freecite.library.brown.edu/citations/create';
		$postData = array('citation' => $citationText);

		$result = getCachedURL($baseUrl, $postData);

		// create a temporary DOM document to hold the XML response
		$tempDOM = new DOMDocument();
		$tempDOM->recover = true;			// try to handle non-well-formed responses

		$tempDOM->loadXML(utf8_normalize($result));

		// create a new XSLT processor
		$xslDOM = new DOMDocument();
		$proc = new XsltProcessor();
		$proc->registerPHPFunctions();

		// transform the Freecite XML into a flat array
		$xsl = dirname(__FILE__) . DS . 'freecite.xsl';
		$xslDOM->load($xsl);
		$proc->importStylesheet($xslDOM);
		$outDOM = $proc->transformToDoc($tempDOM);

		$metadata = xml_to_array($outDOM->documentElement, false);

		// format authors into comma-separated for UI
		// TODO: this will change when author model is refactored
		$formattedAuthors = '';
		foreach ($outDOM->getElementsByTagName('author') as $authorNode) {
			$author = $authorNode->nodeValue;

			// parse author elements and rewrite
			$array = parse_author(trim_punc($author));
			$formattedAuthors .= strip_punc($array['surname'].' '.$array['forename'].' '.$array['initials']). ', ';
		}
		$metadata['authors'] = trim_punc($formattedAuthors);

		// clean up metadata array
		$metadata['author'] = '';
		$metadata = array_map('trim_punc', $metadata);

		// map date to year
		if (isset($metadata['date']) && empty($metadata['year'])) $metadata['year'] = $metadata['date'];
		if (!empty($metadata['date']) && $metadata['date'] == $metadata['year']) unset($metadata['date']);

		// check publisher and location; NB: not well-tested
		if (!empty($metadata['publisher']) && $metadata['publisher'] == $metadata['publoc']) unset($metadata['publisher']);

		// TODO: remove elements we don't care about
		unset($metadata['raw_string']);

		$metadata = array_clean($metadata);

		return $metadata;
	}

}
?>
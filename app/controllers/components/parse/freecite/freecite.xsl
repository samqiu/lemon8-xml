<?xml version='1.0' encoding='utf-8'?>

<!--============================================

	freecite.xsl -  simple mapping from Freecite contextobject
					into a flat XML for converting into a PHP array
	Part of Lemon8-XML; developed by the Public Knowledge Project   http://pkp.sfu.ca

	Copyright (c) 2008-2009 MJ Suhonos
	Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.

==============================================-->

<xsl:transform version="1.0"
        xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:php="http://php.net/xsl"
        xmlns:ctx="info:ofi/fmt:xml:xsd:ctx"
        exclude-result-prefixes="xsl php ctx">

<xsl:strip-space elements="*"/>

<xsl:template match="/citations">
	<citation>

		<xsl:choose>
			<!-- get elements from contextobject if it exists -->
			<xsl:when test="ctx:context-objects/ctx:context-object/ctx:referent/ctx:metadata-by-val/ctx:metadata">
				<xsl:apply-templates select="ctx:context-objects/ctx:context-object/ctx:referent/ctx:metadata-by-val/ctx:metadata/*/*"/>
			</xsl:when>

			<xsl:otherwise>
				<!-- get any additional elements from citation -->
				<xsl:apply-templates select="citation/authors/*"/>
				<xsl:apply-templates select="citation/*[local-name(.) != 'authors']"/>
			</xsl:otherwise>
		</xsl:choose>

	</citation>
</xsl:template>

<!-- Genre -->
<xsl:template match="*[local-name() = 'genre']">
	<genre>
		<xsl:choose>
			<!-- map to interal options -->
			<xsl:when test=". = 'article'">journal</xsl:when>
			<xsl:when test=". = 'proceeding'">proceedings</xsl:when>
			<xsl:otherwise>other</xsl:otherwise>
		</xsl:choose>
	</genre>		
</xsl:template>

<!-- Authors -->
<xsl:template match="*[local-name() = 'au']">
	<author><xsl:value-of select="."/></author>		
</xsl:template>

<!-- Book title -->
<xsl:template match="*[local-name() = 'btitle']">
	<title><xsl:value-of select="."/></title>		
</xsl:template>

<!-- Journal/conference title -->
<xsl:template match="*[local-name() = 'stitle']">
	<title><xsl:value-of select="."/></title>		
</xsl:template>

<!-- Location -->
<xsl:template match="*[local-name() = 'place'] | *[local-name() = 'location']">
	<publoc><xsl:value-of select="php:functionString('preg_replace', '/^(.+):.*/', '\1', .)"/></publoc>

	<!--  try to extract publisher if we don't have one -->
	<xsl:if test="not(../*[local-name() = 'pub'])">
		<publisher><xsl:value-of select="php:functionString('preg_replace', '/.*:([^,]+),?.*/', '\1', .)"/></publisher>
	</xsl:if>
</xsl:template>

<!-- Publisher -->
<xsl:template match="*[local-name() = 'pub']">
	<publisher><xsl:value-of select="."/></publisher>
</xsl:template>

<!-- Pages -->
<xsl:template match="*[local-name() = 'pages']">
	<spage><xsl:value-of select="substring-before(., '--')"/></spage>
	<epage><xsl:value-of select="substring-after(., '--')"/></epage>
</xsl:template>

<!-- copy element and value -->
<xsl:template match="*">
	<xsl:element name="{local-name()}"><xsl:value-of select="."/></xsl:element>
</xsl:template>

</xsl:transform>
<?php

/**
 * regexparse.php
 *
 * Copyright (c) 2008-2009 MJ Suhonos
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Use magical, time-tested regexes to break a citation into metadata elements
 * Works best on ICMJE/Vancouver-type journal citations
 *
 */

class RegexParseComponent extends Object
{

	function parse($citationText) {

		// strip newline and tab characters
		$citationText = preg_replace('/[\r\n\s]/', ' ', $citationText);
		$metadata = array();

		// parse out any embedded URLs
		if (preg_match('(https?://([-\w\.]+)+(:\d+)?(/([\w/_\.,]*(\?\S+)?)?)?)', $citationText, $matches)) {

			// if the URL is a link to pubmed, replace it by the PMID
			if (preg_match("/list_uids=(?P<pID>\d+)/", $matches[0], $uid) ) {
				$metadata['pmid'] = $uid[1];
			} elseif (preg_match("/pubmed.*details_term=(\d+)/", $matches[0], $uid) ) {
				$metadata['pmid'] = $uid[1];
			} elseif (preg_match("/pubmedid=(?P<pID>\d+)/", $matches[0], $uid)) {
				$metadata['pmid'] = $uid[1];
			} else {
				$metadata['targetURL'] = $matches[0];
			}
			$citationText = preg_replace('(https?://([-\w\.]+)+(:\d+)?(/([\w/_\.,]*(\?\S+)?)?)?)', '', $citationText);
		}

		// parse out any embedded PMIDs
		if (preg_match('/PMID:?\s*(\d+)/i', $citationText, $matches) ) {
			$metadata['pmid'] = $matches[1];
			$citationText = preg_replace('/PMID:\S*(\d+)/i', '', $citationText);
		}

		// parse out any embedded DOIs
		if (preg_match('/DOI:?\s*(\S+)/i', $citationText, $matches) ) {
			$metadata['doi'] = $matches[1];
			$citationText = preg_replace('/DOI:\s*(\S+)/i', '', $citationText);
		}

		// parse out access date if we have one
		if (preg_match('/accessed:?\s*([\s\w]+)/i', $citationText, $matches)) {
			$metadata['access_date'] = $matches[1];
			$citationText = preg_replace('/accessed:?\S*([\s\w]+)/i', '', $citationText );
		}

		// clean out all square bracket business
		$citationText = preg_replace('/\[\s*(pubmed|medline|full text)\s*]/i', '', $citationText);
		$citationText = preg_replace('/[\[\]]/', '', $citationText);

		// book citation
		if (preg_match("/\s*(?P<a1>[^\.]+)\.\s*(?P<m1>.*?)\s*(?P<l>[^\.]*):\s*(?P<p>[^:]*?);\s*(?P<d>\d\d\d\d.*?)(?P<tail>.*)/", $citationText, $matches)) {

			$metadata['genre'] = 'book';
			$metadata['authors'] = $matches[1];
			$metadata['title'] = $matches[2];
			$metadata['publoc'] = $matches[3];
			$metadata['publisher'] = $matches[4];
			$metadata['date'] = $matches[5];
			$metadata['comment'] = $matches[6];

		// journal citation
		} elseif (preg_match("/\s*(?P<a1>[^\.]+)\.\s*(?P<m>.*)\s*(?P<d>\d\d\d\d.*?);(?P<v>[^:]+):(?P<tail>.*)/", $citationText, $matches)) {

			$metadata['genre'] = 'journal';
			$metadata['authors'] = $matches[1];

			preg_match("/(.*[\.!\?])(.*)/", trim($matches[2], " ."), $title_source);
			$metadata['atitle'] = $title_source[1];
			$metadata['title'] = $title_source[2];

			$metadata['date'] = $matches[3];

			preg_match("/([^\(]+)(\(([^\)]+)\))?/", $matches[4], $volume_issue);
			$metadata['volume'] = $volume_issue[1];
			if (isset($volume_issue[3])) $metadata['issue'] = $volume_issue[3];

			$metadata['comment'] = $matches[5];

		// web citation w/ authors
		} elseif (preg_match("/\s*(?P<a1>[^\.]+)\.\s*(?P<t1>.*?)\s*URL:\s*(?P<tail>.*)/", $citationText, $matches)) {

			$metadata['genre'] = 'web';
			$metadata['authors'] = $matches[1];
			$metadata['title'] = $matches[2];
			$metadata['comment'] = $matches[3];

		// web citation w or w/o authors
		} elseif (preg_match("/\s*(?P<t1>.*?)\s*URL:\s*(?P<tail>.*)/", $citationText, $matches)) {

			$metadata['genre'] = 'web';
			$metadata['atitle'] = $matches[1];
			$metadata['comment'] = $matches[2];

			$temp = explode(".", $metadata['atitle']);
			if ( count($temp) > 2 ) {
				$metadata['authors'] = $temp[0]." ".$temp[1];
				$metadata['title'] = implode(". ", array_slice($temp, 2));
			} elseif ( count($temp = 2) ) {
				$metadata['authors'] = $temp[0];
				$metadata['title'] = $temp[1];
			}
		}

		// TODO: handle in-ref titles, eg. with editor lists

		// normalize the date if we have a month/day
		if (isset($metadata['date']) && preg_match("/(?P<year>\d{4})\s*(?P<month>[a-z]\w+)?\s*(?P<day>\d+)?/i", $metadata['date'], $newdate) ){
			$metadata['date'] = $newdate['year'];
			$metadata['year'] = $newdate['year'];
			if (isset($newdate['month'])) $metadata['month'] = $newdate['month'];
			if (isset($newdate['day'])) $metadata['day'] = $newdate['day'];
		}

		// extract page numbers if possible
		if (isset($metadata['comment']) && preg_match("/^[:p\.\s]*(?P<p1>[Ee]?\d+)(-(?P<p2>\d+))?/", $metadata['comment'], $pages)) {
			$metadata['spage'] = $pages[1];
			if (isset($pages[3])) $metadata['epage'] = $pages[3];
			$metadata['comment'] = preg_replace("/^[:p\.\s]*(?P<p1>[Ee]?\d+)(-(?P<p2>\d+))?/", '', $matches[5]);
		}

		// trim et al from authors
		if (isset($metadata['authors'])) $metadata['authors'] = preg_replace('/et ?al$/', '', $metadata['authors']);

		// map date to year
		if (isset($metadata['date']) && $metadata['year'] == '') $metadata['year'] = $metadata['date'];
		if (isset($metadata['date']) && isset($metadata['year']) && $metadata['date'] == $metadata['year']) unset($metadata['date']);

		// trim extra stuff from all elements
		$metadata = array_map('trim_punc', $metadata);

		return $metadata;
	}

}
?>

<?xml version='1.0' encoding='utf-8'?>

<!--============================================

	parscit.xsl -  simple mapping from Parscit web service
				   into a flat XML for converting into a PHP array
	Part of Lemon8-XML; developed by the Public Knowledge Project   http://pkp.sfu.ca

	Copyright (c) 2008-2009 MJ Suhonos
	Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.

==============================================-->

<xsl:transform version="1.0"
        xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:php="http://php.net/xsl"
        exclude-result-prefixes="xsl php">

<xsl:strip-space elements="*"/>

<xsl:template match="/citationList">
	<citation>
		<xsl:apply-templates select="citation/*"/>
	</citation>
</xsl:template>

<!-- Genre -->
<xsl:template match="*[local-name() = 'genre']">
	<genre>
		<xsl:choose>
			<!-- map to interal options -->
			<xsl:when test=". = 'article'">journal</xsl:when>
			<xsl:when test=". = 'proceeding'">proceedings</xsl:when>
			<xsl:otherwise>other</xsl:otherwise>
		</xsl:choose>
	</genre>		
</xsl:template>

<!-- Authors -->
<xsl:template match="authors">
	<xsl:copy-of select="*"/>
</xsl:template>

<!-- Article title -->
<xsl:template match="title">
	<xsl:choose>
		<xsl:when test="../booktitle or ../journal">
			<atitle><xsl:value-of select="."/></atitle>
		</xsl:when>
		<xsl:otherwise>
			<title><xsl:value-of select="."/></title>		
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<!-- Book title -->
<xsl:template match="booktitle">
	<title><xsl:value-of select="."/></title>		
</xsl:template>

<!-- Journal title -->
<xsl:template match="journal">
	<title><xsl:value-of select="."/></title>		
</xsl:template>

<!-- Location -->
<xsl:template match="location">
	<publoc><xsl:value-of select="php:functionString('preg_replace', '/^(.+):.*/', '\1', .)"/></publoc>

	<!--  try to extract publisher if we don't have one -->
	<xsl:if test="not(../publisher)">
		<publisher><xsl:value-of select="php:functionString('preg_replace', '/.*:([^,]+),?.*/', '\1', .)"/></publisher>
	</xsl:if>
</xsl:template>

<!-- Pages -->
<xsl:template match="*[local-name() = 'pages']">
	<spage><xsl:value-of select="substring-before(., '--')"/></spage>
	<epage><xsl:value-of select="substring-after(., '--')"/></epage>
</xsl:template>

<!-- Comment -->
<xsl:template match="note">
	<comment><xsl:value-of select="."/></comment>
</xsl:template>

<!-- copy element and value -->
<xsl:template match="*">
	<xsl:element name="{local-name()}"><xsl:value-of select="."/></xsl:element>
</xsl:template>

</xsl:transform>
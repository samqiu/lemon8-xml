<?php

/**
 * freecite.php
 *
 * Copyright (c) 2008-2009 MJ Suhonos
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Uses the Parscit web service to break a citation into metadata elements
 *
 */

class ParscitComponent extends Object
{

	function parse($citationText) {

		// strip newline and tab characters
		$citationText = preg_replace('/[\r\n\s]/', ' ', $citationText);

		// Paracit web form; results are (mal-formed) HTML
		$baseUrl = 'http://aye.comp.nus.edu.sg/parsCit/parsCit.cgi?textlines=';
		$result = getCachedURL($baseUrl . urlencode($citationText));

		// turn the tagged portion into XML
		// NB: screen-scraping can be a bit flaky
		$xmlResult = preg_replace(array('/.*<algorithm[^>]+>(.*)<\/algorithm>.*/s', '/&/'), array('\1', '&amp;'), html_entity_decode($result));

		// create a temporary DOM document to hold the XML response
		$tempDOM = new DOMDocument();
		$tempDOM->recover = true;			// try to handle non-well-formed responses

		$tempDOM->loadXML(utf8_normalize($xmlResult));

		// create a new XSLT processor
		$xslDOM = new DOMDocument();
		$proc = new XsltProcessor();
		$proc->registerPHPFunctions();

		// transform the Freecite XML into a flat array
		$xsl = dirname(__FILE__) . DS . 'parscit.xsl';
		$xslDOM->load($xsl);
		$proc->importStylesheet($xslDOM);
		$outDOM = $proc->transformToDoc($tempDOM);

		$metadata = xml_to_array($outDOM->documentElement, false);

		// format authors into comma-separated for UI
		// TODO: this will change when author model is refactored
		$formattedAuthors = '';
		foreach ($outDOM->getElementsByTagName('author') as $authorNode) {
			$author = $authorNode->nodeValue;

			// parse author elements and rewrite
			$array = parse_author(trim_punc($author));
			$formattedAuthors .= strip_punc($array['surname'].' '.$array['forename'].' '.$array['initials']). ', ';
		}
		$metadata['authors'] = trim_punc($formattedAuthors);

		// clean up metadata array
		$metadata = array_map('trim_punc', $metadata);

		// map date to year
		if (isset($metadata['date']) && empty($metadata['year'])) $metadata['year'] = $metadata['date'];
		if (isset($metadata['date']) && $metadata['date'] == $metadata['year']) unset($metadata['date']);

		// TODO: remove elements we don't care about
		unset($metadata['author']);

		$metadata = array_clean($metadata);

		return $metadata;
	}

}
?>
<?php

/**
 * paracite.php
 *
 * Copyright (c) 2008-2009 MJ Suhonos
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Uses the ParaCite perl parser library to break a citation into metadata elements
 *
 */

class ParaciteComponent extends Object
{
	// array of settings required for use
	var $settings = array('parsers' => 'ParaCite types (Standard, Citebase, Jiao)');

	// set the parser type; case-sensitive options are: Standard (default), Citebase, Jiao
	// this is a bit of a hack for multiple parsers in one component
	function parse($citationText, $types) {
		$parsers = explode(',', $types);
		foreach ($parsers as $parser) {
			$results[] = ParaciteComponent::paraciteParse($citationText, trim($parser));
		}
		return $results;
	}

	function paraciteParse($citationText, $parserType = 'Standard') {

		// convert to ASCII; Paracite doesn't handle UTF-8 especially well
		App::import('Vendor', 'phputf8/utf8_to_ascii');
		$citationText = utf8_to_ascii($citationText);

		// strip newline and tab characters
		$citationText = preg_replace('/[\r\n\s]/', ' ', $citationText);

		// paracite calls are cached to reduce load of invoking perl
		$metadata = Cache::read(sha1($parserType.$citationText));
		if ($metadata === false) {

			// set the directory for the paracite parser
			$paraciteDir = ROOT . DS . APP_DIR . DS . 'vendors' . DS . 'CPAN';
			$paraciteCommand = 'cd '.$paraciteDir.'; perl paracite.pl '.$parserType.' "'.preg_replace('/"/', '\"', $citationText).'"';

			// TODO handle HTML entities returned here
			$result = shell_exec($paraciteCommand);

			// create a temporary DOM document
			$tempDOM = new DOMDocument();
			$tempDOM->recover = true;			// try to handle non-well-formed responses

			$tempDOM->loadXML(utf8_normalize($result));
			$metadata = xml_to_array($tempDOM->documentElement, false);

			Cache::write(sha1($parserType.$citationText), $metadata, array("config" => "default", "duration" => "+1 day"));
		}

		// correct capitalization on title
		if (isset($metadata['title'])) $metadata['title'] = title_case($metadata['title']);

		// set the publication if we have it to correct the title
		if (isset($metadata['publication']) && empty($metadata['title'])) $metadata['title'] = $metadata['publication'];

		// map date to year
		if (isset($metadata['date']) && empty($metadata['year'])) $metadata['year'] = $metadata['date'];
		if (isset($metadata['date']) && $metadata['date'] == $metadata['year']) unset($metadata['date']);

		// format authors into comma-separated for UI
		// TODO: this will change when author model is refactored
		if (isset($metadata['authors'])) {
			$authors = explode(':', trim_punc($metadata['authors']));

			$formattedAuthors = '';
			foreach ($authors as $author) {
				$author = strtr($author, array('_' => ', ', '.' => ' '));

				// parse author elements and rewrite
				$array = parse_author(trim_punc($author));
				$formattedAuthors .= strip_punc($array['surname'].' '.$array['forename'].' '.$array['initials']). ', ';
			}

			$metadata['authors'] = trim_punc($formattedAuthors);
		}

		// rewrite the marked citation if we have one
		if (isset($metadata['marked']) && isset($metadata['atitle']) && isset($metadata['title'])) {
			$metadata['marked'] = preg_replace('/title>/', 'atitle>', $metadata['marked']);
		}

		if (isset($metadata['marked']) && isset($metadata['title']) && isset($metadata['publication'])) {
			$metadata['marked'] = preg_replace('/publication>/', 'title>', $metadata['marked']);
		}

		// TODO: remove elements we don't care about
		// right now they may be used for crossref lookups
//		unset($metadata['aulast']);
//		unset($metadata['aufirst']);
//		unset($metadata['auinit']);
		unset($metadata['aufull']);

		unset($metadata['publication']);
		unset($metadata['rest']);
		unset($metadata['any']);
		unset($metadata['num_of']);
		unset($metadata['text']);
		unset($metadata['_class']);
		unset($metadata['featureID']);
		unset($metadata['jnl']);
		unset($metadata['id']);
		unset($metadata['match']);
		unset($metadata['ref']);
		unset($metadata['jnl_epos']);
		unset($metadata['rest_text']);
		unset($metadata['num_of_fig']);

		// trim extra stuff from all elements
		$metadata = array_map('trim_punc', $metadata);

		return $metadata;
	}

}
?>
<?php

/**
 * google_docs.php
 *
 * Copyright (c) 2008-2009 MJ Suhonos
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Class to send an uploaded file via Zend GDocs API to Google Docs to convert it to ODT
 *
 */

class GoogleDocsComponent extends Object
{
	// array of settings required for use
	var $settings = array('username' => 'Google Docs username',
										'password' => 'Google Docs password');

	function convert($file_array, $settings = array()) {
		// move the file to cake temp directory for uploading to google docs
		$filename = TMP . $file_array['name'];
		move_uploaded_file($file_array['tmp_name'], $filename);

		// load Zend GData classes
		$include_path = set_include_path(get_include_path() . PATH_SEPARATOR . APP . 'vendors');
		App::import('Vendor', 'zend', array('file' => 'Zend/Loader.php'));

		Zend_Loader::loadClass('Zend_Gdata');
		Zend_Loader::loadClass('Zend_Gdata_AuthSub');
		Zend_Loader::loadClass('Zend_Gdata_ClientLogin');
		Zend_Loader::loadClass('Zend_Gdata_Docs');

		// authenticate user to Google Docs
		$httpClient = Zend_Gdata_ClientLogin::getHttpClient($settings['username'], $settings['password'], Zend_Gdata_Docs::AUTH_SERVICE_NAME);
		$gdClient = new Zend_Gdata_Docs($httpClient);

		// upload the temporary file to Google Docs
		$newDocumentEntry = $gdClient->uploadFile($filename, $file_array['name'], null, Zend_Gdata_Docs::DOCUMENTS_LIST_FEED_URI);

		// get the link to the document (with embedded ID)
		$documentLink = $newDocumentEntry->getAlternateLink()->getHref();

		// get the authentication token for this connection
		$httpAuthToken = $httpClient->getClientLoginToken();

		// now use the httpClient to download the file as ODT
		// NB: this is undocumented, so not supported by the Zend GDocs API
		// Available formats: oo: openoffice odt ; html: zipped html ; pdf ; rtf ; txt ; doc
		$gDocsURL = 'http://docs.google.com/MiscCommands?command=saveasdoc&exportformat=oo&doc';
		$httpClient->setHeaders('Authorization', 'GoogleLogin auth='.$httpAuthToken);
		$httpClient->setUri($gDocsURL . parse_url($documentLink, PHP_URL_QUERY));

		$response = $httpClient->request();

		// write the file out to the file system
		file_put_contents($filename . '.odt', $response->getBody());

		// delete the uploaded document from google docs
		$newDocumentEntry->delete();

		set_include_path($include_path);
		return $filename . '.odt';
	}

}
?>
<?php

/**
 *  docvert.php
 *
 * Copyright (c) 2008-2009 MJ Suhonos
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Class to send an uploaded file via HTTP-POST to a Docvert instance to convert it to (zipped) ODT
 *
 */

class DocvertComponent extends Object
{
	// array of settings required for use
	var $settings = array('url' => 'Docvert URL');

	function convert($file_array, $settings = array()) {

		// move the file to cake temp directory for uploading to docvert
		$filename = TMP . $file_array['name'];
		move_uploaded_file($file_array['tmp_name'], $filename);

		// populate the form field data
		$postData = array();
		$postData[ 'random file' ] = '@'.$filename;
		$postData[ 'pipeline' ] = "regularpipeline:none";
		$postData[ 'afterconversion' ] = "downloadZip";

		// initialize the cURL object
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $settings['url']);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postData );

		// make the actual docvert POST call
		$return_file = curl_exec($ch);
		curl_close($ch);

		// create a temporary file for the zipped result
		file_put_contents($filename . '.zip', $return_file);

		// unzip the file and get the ODT filename
		$zipfile = new Archive_Zip($filename . '.zip');

		$data = $zipfile->extract(array('by_index' => array('1'), 'add_path' => TMP, 'remove_all_path' => true));

		// delete the temporary upload and zip files
		unlink($filename);
		unlink($filename . '.zip');

		// return the filename of the new ODT file
		return $data[0]['filename'];
	}

}
<?php

/**
 * metadata_parser.php
 *
 * Copyright (c) 2008-2009 MJ Suhonos
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Parser to extract metadata from the front/back matter of an ODT XML document
 *
 */

class MetadataParserComponent extends Object
{
    var $controller = true;

	// analyze the metadata for semantic markers that define elements
	function analyzeMetadata($metadata) {

		// create a new DOM document for the metadata (a mini-document if you will)
		$metaDOM = new DOMDocument();
		$metaDOM->recover = true;			// try to handle non-well-formed responses

		$metaDOM->loadXML("<metadata/>");		// required root node
		$this->metaDOM =& $metaDOM;
		$xpath =& new DOMXPath($this->metaDOM);

		// loop through the metadata array
		foreach ($metadata as $index => $value) {

			// only process numeric (ie. unparsed) nodes
			if (is_int($index)) {

				$metadataDF = $this->metaDOM->createDocumentFragment();
				$metadataDF->appendXML($value);

				// if the node has only one child, reverse-inherit any style attributes
				if (1 == $metadataDF->firstChild->childNodes->length) {

					$attributeNodes =& $xpath->query("@*", $metadataDF->firstChild->childNodes->item(0));

					foreach ($attributeNodes as $attribute) {
						if (!$metadataDF->childNodes->item(0)->hasAttribute($attribute->nodeName)) {
							$metadataDF->childNodes->item(0)->setAttribute($attribute->nodeName, $attribute->nodeValue);
						}
					}
				}

				// move the node into the DOM and clear it from the metadata array
				$this->metaDOM->documentElement->appendChild($metadataDF);
				unset($metadata[$index]);
			}
		}

		// this is essentially the same approach as DocumentParser::extractSections() 
		$heading_nodes =& $xpath->query("/metadata/*[@heading != '' or boolean(@bold)]", $this->metaDOM);

		foreach ($heading_nodes as $heading_node) {

			// see if we can determine what kind of metadata this is
			if (same_word($heading_node->textContent, 'correspond')) {
				list($metadata['author'], $metadata['affiliation']) = $this->extractCorrespondence($heading_node);

			} elseif (same_word($heading_node->textContent, 'keywords')) {
				$metadata['keywords'] = $this->extractKeywords($heading_node);

			} elseif (same_word($heading_node->textContent, 'paper type')) {
				$metadata['paper-type'] = $this->extractPaperType($heading_node);

			} elseif (same_word($heading_node->textContent, 'conflicts') && !isset($metadata['conflicts'])) {
				$metadata['conflicts'] = $this->extractConflicts($heading_node);

			} elseif (same_word($heading_node->textContent, 'abstract' && !isset($metadata['abstract'])) ) {
				$metadata['abstract'] = $this->extractAbstract($heading_node);

			} elseif (!isset($metadata['article-title'])) {
				$metadata['article-title'] = $heading_node->textContent;

			}

		} // end foreach heading

		// TODO: some data cleaning here: convert abstract, conflicts to XHTML for editing

		return $metadata;
	}

	// parse out corresponding author
	function extractCorrespondence($heading_node) {
		$affiliation = array('contents' => '');

		$content_nodes = $this->getContentNodes($heading_node);
		if (!$content_nodes) return null;

		foreach ($content_nodes as $content_node) {

			// strip newline and tab characters
			$contents = trim(preg_replace('/[\r\n\s]/', ' ', $content_node->textContent), ',:; ');

			// define some common regexp here
			// TODO: perhaps make site-wide constants?
			$regexEmail = "[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]";
			$regexPhone = "((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,5})|(\(?\d{2,6}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}";

			// stuff to detect/extract from author names
			// expressions ported from CiteULike author plugin (in TCL): http://svn.citeulike.org/svn/plugins/author.tcl
			$regexTitle = "(?:His (?:Excellency|Honou?r)\s+|Her (?:Excellency|Honou?r)\s+|The Right Honou?rable\s+|The Honou?rable\s+|Right Honou?rable\s+|The Rt\.? Hon\.?\s+|The Hon\.?\s+|Rt\.? Hon\.?\s+|Mr\.?\s+|Ms\.?\s+|M\/s\.?\s+|Mrs\.?\s+|Miss\.?\s+|Dr\.?\s+|Sir\s+|Dame\s+|Prof\.?\s+|Professor\s+|Doctor\s+|Mister\s+|Mme\.?\s+|Mast(?:\.|er)?\s+|Lord\s+|Lady\s+|Madam(?:e)?\s+|Priv\.-Doz\.\s+)+";
			$regexDegrees = "(,\s+(?:[A-Z\.]+))+";
			$regexForename = "(?:[^ \t\n\r\f\v,.]{2,}|[^ \t\n\r\f\v,.;]{2,}\-[^ \t\n\r\f\v,.;]{2,})";
			$regexInitials = "(?:(?:[A-Z]\.\s){1,4})|(?:[A-Z]{1,4}\s)|(?:(?:[A-Z]\.-?){1,4}\s)|(?:(?:[A-Z]-){1,3}[A-Z]\s)|(?:(?:[A-Z]\s){1,4})|(?:(?:[A-Z] ){1,3}[A-Z]\.\s)|(?:[A-Z]-(?:[A-Z]\.){1,3}\s)";
			$regexPrefix = "Dell(?:[a|e])?\s|Dalle\s|D[a|e]ll\'\s|Dela\s|Del\s|[Dd]e (?:La |Los )?\s|[Dd]e\s|[Dd][a|i|u]\s|L[a|e|o]\s|[D|L|O]\'|St\.?\s|San\s|[Dd]en\s|[Vv]on\s(?:[Dd]er\s)?|(?:[Ll][ea] )?[Vv]an\s(?:[Dd]e(?:n|r)?\s)?";
			$regexSurname = "(?:".$regexPrefix.")?(?:".$regexForename.")";

			if (!isset($email) && preg_match('/('.$regexEmail.')/i', $contents, $results)) {
				$email = $results[1];

			} elseif (!isset($phone) && preg_match('/('.$regexPhone.')/', $contents, $results)) {
				$phone = $results[1];

			} elseif (!isset($fax) && preg_match('/fax[^\d|^+|^-|\(|\)]*('.$regexPhone.')/i', $contents, $results)) {
				$fax = $results[1];

			} elseif (in_iarray(strtolower(trim($contents, ',:; ')), array_keys(country_list()))) {
				$affiliation['country'] =  trim($contents, ',:; ');

			} elseif (!isset($author) && (preg_match('/^('.$regexTitle.')/iu', $contents, $results) ||
															 preg_match('/('.$regexDegrees.')$/iu', $contents, $results) || 
															 preg_match('/^('.$regexSurname.'),? ?('.$regexInitials.')$/iu', $contents, $results) ||
															 preg_match('/^('.$regexSurname.'), ?('.$regexForename.')( \[A-Z\]+)?/iu', $contents, $results) ||
															 preg_match('/^('.$regexInitials.')?('.$regexSurname.')$/iu', $contents, $results) ||
															 preg_match('/^('.$regexForename.') ('.$regexInitials.')?('.$regexSurname.')$/iu', $contents, $results) ||
															 preg_match('/^((?:[A-Z]\\\.){1,3})('.$regexSurname.')$/iu', $contents, $results) ||
															 preg_match('/^('.$regexSurname.')$/iu', $contents, $results) ||
															 preg_match('/^('.$regexInitials.')('.$regexForename.') ('.$regexSurname.')$/iu', $contents, $results) ||
															 preg_match('/^('.$regexForename.') ('.$regexForename.') ('.$regexSurname.')$/iu', $contents, $results) ||
															 preg_match('/^('.$regexSurname.'), ('.$regexInitials.')('.$regexForename.')$/iu', $contents, $results))) {

				// parse the author into: forename, initials, surname, degrees
				$author = $contents;
				if (preg_match('/^('.$regexTitle.')/iu', $author, $results)) $author = preg_replace('/^('.$regexTitle.')/iu', '', $author);

				if (preg_match('/('.$regexDegrees.')$/iu', $author, $results)) {
					$degreesArray = preg_split('/[:;,]/', trim($results[1], ',:; '), -1, PREG_SPLIT_NO_EMPTY );
					$degrees = implode('; ', $degreesArray);
					$degrees = strtr($degrees, array('.' => ''));

					$author = preg_replace('/('.$regexDegrees.')$/iu', '', $author);
				}

				if (
					preg_match('/^(?P<forename>'.$regexForename.') (?P<initials>'.$regexInitials.')?(?P<surname>'.$regexSurname.')$/iu', $author, $results) ||
					preg_match('/^(?P<surname>'.$regexSurname.'), (?P<initials>'.$regexInitials.')(?P<forename>'.$regexForename.')$/iu', $author, $results) ||
					preg_match('/^(?P<initials>'.$regexInitials.')(?P<forename>'.$regexForename.') (?P<surname>'.$regexSurname.')$/iu', $author, $results) ||
					preg_match('/^(?P<forename>'.$regexForename.') (?P<forename>'.$regexForename.') (?P<surname>'.$regexSurname.')$/iu', $author, $results) ||
					preg_match('/^(?P<surname>'.$regexSurname.'), ?(?P<forename>'.$regexForename.')( \[A-Z\]+)?/iu', $author, $results) ||
					preg_match('/^(?P<surname>'.$regexSurname.'),? ?(?P<initials>'.$regexInitials.')$/iu', $author, $results) ||
					preg_match('/^(?P<initials>'.$regexInitials.')?(?P<surname>'.$regexSurname.')$/iu', $author, $results) ||
					preg_match('/^(?P<initials>([A-Z]\\\.){1,3})(?P<surname>'.$regexSurname.')$/iu', $author, $results) ||
					preg_match('/^(?P<surname>'.$regexSurname.')$/iu', $author, $results) ||
					preg_match('/(?P<surname>'.$regexSurname.')/iu', $author, $results)) {

					$forename = $results['forename'];
					$initials = $results['initials'];
					$surname = $results['surname'];
				}

//			} elseif (preg_match('/('.implode('|', similar_words('institution')).')/iu', $contents)) {
//				TODO: not sure if we want to retain this granularity

			} else {
				$affiliation['contents'] .=  "\n".trim($contents, ',:; ');

			}
		} // end foreach

		// assign author variables into array
		$author = array();
		if (isset($forename)) $author['forename'] = $forename;
		if (isset($initials)) $author['initials'] = $initials;
		if (isset($surname)) $author['surname'] = $surname;
		if (isset($degrees)) $author['degrees'] = $degrees;
		if (isset($email)) $author['email'] = $email;
		if (isset($phone)) $author['phone'] = $phone;
		if (isset($fax)) $author['fax'] = $fax;

		return array($author, $affiliation);
	}

	// parse out keywords
	function extractKeywords($heading_node) {

		$content_nodes = $this->getContentNodes($heading_node);
		if (!$content_nodes) return null;

		// join lines and normalize separators
		$keywordString = '';
		foreach ($content_nodes as $content_node) $keywordString .= '; '.$content_node->textContent;

		$keywordArray = preg_split('/[:;,]/', trim($keywordString, ',:; '), -1, PREG_SPLIT_NO_EMPTY );
		$keywords = implode('; ', $keywordArray);

		return $keywords;
	}

	// parse out conflicts of interest
	function extractConflicts($heading_node) {

		$content_nodes = $this->getContentNodes($heading_node);
		if (!$content_nodes) return null;

		// just copy the content wholesale as XML
		$content = '';
		foreach ($content_nodes as $content_node) $content .= $heading_node->ownerDocument->saveXML($content_node);

		return $content;
	}

	// parse out abstract
	function extractAbstract($heading_node) {

		$content_nodes = $this->getContentNodes($heading_node);
		if (!$content_nodes) return null;

		// just copy the content wholesale as XML
		$content = '';
		foreach ($content_nodes as $content_node) $content .= $heading_node->ownerDocument->saveXML($content_node);

		return $content;
	}

	// parse out paper type
	function extractPaperType($heading_node) {

		// TODO: see if we can determine which paper type this is
		// there's some refactoring required in similar_words for this

		return 'other';
	}

	// helper function to look ahead until the next "heading" node
	// and return an array (pseudo node-set) of everything in between
	function getContentNodes($heading_node) {

		// get all following siblings that are not headings (the content of a section)
		// we can't do this with XPath because PHP doesn't support generate-id()
		$content_nodes = array();
		$current_node = $heading_node->nextSibling;
		while (isset($current_node) && $current_node->getAttribute('heading') == '' && !$current_node->hasAttribute('bold')) {
			$content_nodes[] = $current_node;
			$current_node = $current_node->nextSibling;
		}

		// if there is no content after this heading, return false
		if (count($content_nodes) == 0) return false;

		return $content_nodes;
	}

}
?>
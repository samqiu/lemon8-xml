<?php

/**
 * document_parser.php
 *
 * Copyright (c) 2008-2009 MJ Suhonos
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Analyze the semantic markup of an ODT XML document,
 * and extract  the structural elements
 */

class DocumentParserComponent extends Object
{
	var $components = array('MetadataParser', 'Session');

	// variables for statistics gathering
	var $table_count = 0;
	var $figure_count = 0;

	// temporary metadata holder for metadata found while parsing sections
	var $metadata_holder;

	// analyze the style XML for layout cues
	function analyzeStructure($metaXML, $contentXML) {

		// load the document metadata into DOM
		$metaDOM = new DOMDocument();
		$metaDOM->recover = true;			// try to handle non-well-formed responses
		$metaDOM->loadXML($metaXML);

		$this->metaDOM =& $metaDOM;

		// load the marked content XML into DOM
		$contentDOM = new DOMDocument();
		$contentDOM->recover = true;			// try to handle non-well-formed responses

		$contentDOM->loadXML($contentXML);
		$this->contentDOM =& $contentDOM;

		// get some document-level statistics
		$metaNode = $this->metaDOM->getElementsByTagName('document-statistic')->item(0);

		// NB: this is because libxml changed between PHP 5.1 and 5.2
		$this->meta_table_count = $metaNode->getAttribute('meta:table-count') . $metaNode->getAttribute('table-count');
		$this->meta_image_count = $metaNode->getAttribute('meta:image-count') . $metaNode->getAttribute('image-count');
		$this->meta_object_count = $metaNode->getAttribute('meta:object-count') . $metaNode->getAttribute('object-count');
		$this->meta_para_count = $metaNode->getAttribute('meta:paragraph-count') . $metaNode->getAttribute('paragraph-count');
		$this->meta_word_count = $metaNode->getAttribute('meta:word-count') . $metaNode->getAttribute('word-count');
		$this->meta_char_count = $metaNode->getAttribute('meta:character-count') . $metaNode->getAttribute('character-count');

		// loop through each content element to gather statistics from the content itself
		$xpath =& new DOMXPath($this->contentDOM);
		$body_nodes =& $xpath->query("/content/*", $this->contentDOM);

		// more accurate calculation of average word/char counts
		$this->avg_words_pgph = $this->meta_word_count / $body_nodes->length;
		$this->avg_chars_pgph = $this->meta_char_count / $body_nodes->length;

		// handle documents that have no headings (pretty badly laid out in that case)
		if (0 == $xpath->query("/content/*[@heading != '' and normalize-space(.) != '']", $this->contentDOM)->length) {
			$no_headings = true;
		}

		$x = 1;
		foreach ($body_nodes as $body_node) {

			// don't bother with empty elements
			if (strlen($body_node->textContent) > 0) {

				// calculate some stats on the fly
				// NB: some limits added here for unusually short articles
				$relative_words = str_word_count($body_node->textContent) / max($this->avg_words_pgph, $this->Session->read('Settings.min_words'));
				$relative_chars = strlen($body_node->textContent) / max($this->avg_chars_pgph, $this->Session->read('Settings.min_chars'));
				$relative_avg = ($relative_words + $relative_chars) /2;

				// if we have an actual heading element, use its outline-level as the heading level
				if ($body_node->nodeName == 'h' && ( $body_node->getAttribute('heading') == '' || $body_node->getAttribute('outline-level') != '') ) {
					$body_node->setAttribute('heading', $body_node->getAttribute('outline-level'));
				}

				// if the element is under the heading threshold, it's probably a heading or metadata
				// TODO: match elements that are styled by a (single?) sub-element, eg. <p><span heading>
				if ($relative_avg < ($this->Session->read('Settings.heading_margin')/100)) {

					// if this is marked as a heading
					if ($body_node->getAttribute('heading')  &&
						($body_node->nodeName != 'a' &&
						 $body_node->nodeName != 'forms' &&
						 $body_node->nodeName != 'list-item' &&
						 $body_node->nodeName != 'tracked-changes')) {

					} else {
						// if no headings are set, then take a shot in the dark at setting some
						if (isset($no_headings) && (
							$relative_avg < ($this->Session->read('Settings.heading_margin')/1000) ||
							$body_node->previousSibling == false ||
							$body_node->previousSibling->nodeType == XML_TEXT_NODE)) {

							$body_node->setAttribute('heading', '1');
						} else {
							// otherwise it's probably metadata
							// clear any heading attribute incorrectly left by the style parser
							$body_node->removeAttribute('heading');

							if ($body_node->nodeName != 'list' &&
								$body_node->nodeName != 'table') {
								$body_node->setAttribute('metadata', 'true');
							}
						}
					}

				} else {
					// clear any heading attribute incorrectly left by the style parser
					$body_node->removeAttribute('heading');

				}
				$x++;
			} // end empty if
		} // end foreach

		// NB: the parsers have to run in this order because we parse
		// top-to-bottom and add flags throughout

		// detect and extract sections (including figures and tables)
		$sections = $this->extractSections();

		// detect and extract citations
		$citations = $this->extractCitations();

		// detect and extract metadata
		$metadata = $this->extractMetadata();

		// derive a confidence score
		$table_score = $this->meta_table_count ? min($this->table_count / $this->meta_table_count *100, 100) : null;
		$figure_score = $this->meta_image_count ? min($this->figure_count / $this->meta_image_count *100, 100) : null;

		// recursively walk the metadata array to get all the contents
		$this->metadata_string = '';
		array_walk_recursive($metadata, create_function('$elem, $key, $obj', '$obj->metadata_string .= strip_tags($elem) . " ";'), $this);

		$section_string = '';
		$citation_string = '';
		foreach ($sections as $element) $section_string .= $element['contents'];
		foreach ($citations as $element) $citation_string .= strip_tags($element);

		$para_count = count($metadata, COUNT_RECURSIVE) + count($sections) + preg_match_all('/<p/', $section_string, $matches) + count($citations);
		$word_count = (str_word_count($this->metadata_string) + str_word_count(strip_tags($section_string)) + str_word_count($citation_string));
		$char_count = strlen($this->metadata_string) + strlen(strip_tags($section_string)) + strlen($citation_string);

		$para_score = min($para_count / $this->meta_para_count *100, 100);
		$word_score = min($word_count / $this->meta_word_count *100, 100);
		$char_score = min($char_count / $this->meta_char_count *100, 100);
		$parse_score = ($char_score + $word_score + $para_score + $table_score + $figure_score) /
									(3 + ($this->meta_table_count ? 1 : 0) + ($this->meta_image_count ? 1 : 0)
									+ (count($metadata, COUNT_RECURSIVE) ? 0 : 1) + (count($sections) ? 0 : 1));

		// pass relevant score information back to the controller
		$statistics = array('table_count' => $this->table_count,
										'table_score' => $table_score,
										'figure_count' => $this->figure_count,
										'figure_score' => $figure_score,
										'para_count' => $para_count,
										'para_score' => $para_score,
										'word_count' => $word_count,
										'word_score' => $word_score,
										'char_count' => $char_count,
										'char_score' => $char_score,
										'parse_score' => $parse_score);

		// merge the extracted arrays back to the document controller
		return array('statistics' => $statistics, 'metadata'  => $metadata, 'sections'  => $sections, 'citations' => $citations);
	}

	//
	// function to identify headings and break them into sections
	//
	function extractSections() {

		$xpath =& new DOMXPath($this->contentDOM);
		$heading_nodes =& $xpath->query("/content/*[@heading != '' and normalize-space(.) != '']", $this->contentDOM);

		// start at heading level 1 by default
		$heading_level = 1;
		foreach ($heading_nodes as $heading_node) {

			// get all following siblings that are not headings (the content of a section)
			// NB: we can't do this with XPath because PHP doesn't support generate-id()
			$content_nodes = array();
			$current_node = $heading_node->nextSibling;

			// NB: stop at non-element nodes because they can't be a heading
			while (isset($current_node) &&
								  $current_node->nodeType == XML_ELEMENT_NODE &&
								  $current_node->getAttribute('heading') == '') {
				$content_nodes[] = $current_node;
				$current_node = $current_node->nextSibling;
			}

			// if there is no content after this heading, then it's most likely: metadata or a container for a sub-section or figure
			if (count($content_nodes) == 0) {

				// if the next heading is a higher level, then consider this a section wrapper
				if ($heading_node->nextSibling && $current_node->nodeType == XML_ELEMENT_NODE &&
					$heading_node->nextSibling->getAttribute('heading') > $heading_node->getAttribute('heading') && $heading_level > 0) {

					$heading_node->removeAttribute('metadata');

					// add the section to the array
					$section = array('type' => 'section', 'title' => $heading_node->textContent, 'level' => $heading_level, 'contents' => '');
					$sections[] = $section;
					continue;
				} else {
					// mark it as metadata
					$heading_node->setAttribute('metadata', 'true');
				}
			}

			// have a look at the content of this section
			$total = 0;
			foreach ($content_nodes as $content_node) {

				// calculate some stats on the fly
				// NB: some limits added here for unusually short articles
				$relative_words = str_word_count($content_node->textContent) / max($this->avg_words_pgph, $this->Session->read('Settings.min_words'));
				$relative_chars = strlen($content_node->textContent) / max($this->avg_chars_pgph, $this->Session->read('Settings.min_chars'));
				$relative_avg = ($relative_words + $relative_chars) /2;

				$total = $total + $relative_avg;
			}

			// if most of the content in this section is over the margin, then extract it
			if (!empty($content_nodes) && ($total / count($content_nodes)) >= ($this->Session->read('Settings.heading_margin')/100)) {

				// if there are no following sections, this is probably references
				if (0 == $xpath->query("following-sibling::*[@heading != '' and normalize-space(.) != '']", $heading_node)->length) {
					// TODO: handle a mix of lists and paragraphs

					// if the last section is a list, it's likely citations
					if ($content_nodes[0]->nodeName == 'list') {
						$node_list = $xpath->query("descendant::p[normalize-space(.) != '']", $content_nodes[0]);
					} else {
						$node_list = $xpath->query("following-sibling::*[normalize-space(.) != '']", $content_nodes[0]);
					}

					foreach ($node_list as $citation_node) {
						// mark the reference nodes
						$citation_node->setAttribute('citation', 'true');
					}

					continue;
				}

				// set the appropriate level of nesting
				// FIXME:  Fatal error: Call to a member function getAttribute() on a non-object
//				$previous_level = $xpath->evaluate("preceding-sibling::*[@heading != '' and normalize-space(.) != ''][2]", $content_node)->item(0)->getAttribute('heading');

				$previous_node =& $xpath->evaluate("preceding-sibling::*[@heading != '' and normalize-space(.) != ''][2]", $content_node);

				if ($previous_node->length > 0) {
					$previous_level = $previous_node->item(0)->getAttribute('heading');
//				} else {
//					$previous_level = $heading_level;
				}

				if ($heading_node->getAttribute('heading') > $heading_level && $heading_node->getAttribute('heading') > $previous_level) {
						$heading_level++;
				} elseif ($heading_node->getAttribute('heading') < $heading_level) {
					$heading_level = $heading_node->getAttribute('heading');
				}

				$title = $heading_node->textContent;
				$content = '';
				$level = $heading_level;

				// NB: use while instead of foreach so we can do look-ahead
				reset($content_nodes);
				while ($content_node = current($content_nodes)) {
					// if we find an image or table in this section, extract it and append it at the next heading level
					if ($content_node->getAttribute('image') || $content_node->nodeName == 'table') {
						// append the current section to the result array
						if ($content) {
							$section = array('type' => 'section', 'title' => $title, 'level' => $level, 'contents' => $content);
							$sections[] = $section;
							$level = $heading_level + 1;
						}

						// append the table/figure section to the result array
						if ($content_node->getAttribute('image')) {
							$type = 'figure';
							$this->figure_count++;
							$content = $xpath->evaluate(".//image/@href", $content_node)->item(0)->textContent;
						} else {
							// table
							$type = 'table';
							$this->table_count++;
							$content = $this->contentDOM->saveXML($content_node);
						}

						// look-ahead: see if there is a caption for this figure/table
						$next_node = next($content_nodes);

						if (!empty($next_node) && $next_node->getAttribute('metadata')) {
							$next_node->removeAttribute('metadata');
							$title = $next_node->textContent;
							next($content_nodes);
						} else {
							$title = '';
						}

						$section = array('type' => $type, 'title' => $title, 'level' => $level, 'contents' => $content);
						$sections[] = $section;

						$content = '';
						$title = '';

					} elseif ($content_node->getAttribute('metadata')) {
						// look-ahead: see if this is a caption for a figure or table
						$next_node = next($content_nodes);

						if (!empty($next_node) && ($next_node->getAttribute('image') || $next_node->nodeName == 'table')) {
							// append the current section to the result array
							if ($content) {
								$content_node->removeAttribute('metadata');
								$section = array('type' => 'section', 'title' => $title, 'level' => $level, 'contents' => $content);
								$sections[] = $section;
								$level = $heading_level + 1;
							}

							// append the table/figure section to the result array
							if ($next_node->getAttribute('image')) {
								$type = 'figure';
								$this->figure_count++;
								$content = $xpath->evaluate(".//image/@href", $next_node)->item(0)->textContent;
							} else {
								// table
								$type = 'table';
								$this->table_count++;
								$content = $this->contentDOM->saveXML($next_node);
							}

							$section = array('type' => $type, 'title' => $content_node->textContent, 'level' => $level, 'contents' => $content);
							$sections[] = $section;

							$content = '';
							$title = '';
							next($content_nodes);

						} // else: this is just metadata, do not include it in the section content

					} else {
						$content .= $this->contentDOM->saveXML($content_node);

						// if we've reached the end of the content, add the section to the array
						if (!next($content_nodes)) {

							if (same_word($heading_node->textContent, 'abstract')) {
								$this->metadata_holder['abstract'] = $content;

							} elseif (same_word($heading_node->textContent, 'conflicts')) {
								$this->metadata_holder['conflicts'] = $content;

							} else {
								// add the section to the array
								$section = array('type' => 'section', 'title' => $title, 'level' => $level, 'contents' => $content);
								$sections[] = $section;
							}
						}
					}

				} // end while content

			} else {
				// the entire section is metadata (or the references section)

				// assume the last section is probably references
				if ( (0 == $xpath->query("following-sibling::*[@heading != '' and normalize-space(.) != '']", $heading_node)->length &&
					   0 == $xpath->query("//*[@citation != '' and normalize-space(.) != '']", $this->contentDOM)->length ) ||
					same_word($heading_node->textContent, 'references') ) {

					foreach ($content_nodes as $content_node) {
						// mark the reference nodes
						$content_node->setAttribute('citation', 'true');
					}

				// if we've found the abstract, get all the content from this section
				} elseif (same_word($heading_node->textContent, 'abstract')) {

					$this->metadata_holder['abstract'] = '';
					foreach ($content_nodes as $content_node) {
						$content_node->removeAttribute('metadata');
						$this->metadata_holder['abstract'] .= $this->contentDOM->saveXML($content_node);
					}

				// just add it to the metadata array
				} else {
					$heading_node->setAttribute('metadata', 'true');

					foreach ($content_nodes as $content_node) {
						$content_node->setAttribute('metadata', 'true');
					}
				}
			}

		} // end foreach heading

		return $sections;
	}

	//
	// function to identify citations and extract them into an ordered array
	//
	function extractCitations() {
		$xpath =& new DOMXPath($this->contentDOM);
		$citation_nodes =& $xpath->query("//*[@citation != '' and normalize-space(.) != '']", $this->contentDOM);

		foreach ($citation_nodes as $citation_node) {
			$citation_node->removeAttribute('metadata');

			// remove leading numbering and trim excess whitespace
			$citations[] = trim(preg_replace(array('/^\s*\[?\s*\d+\s*\]?\s*\.?(\s*)/', '/[\n\r\s]+/'), ' ', $citation_node->textContent));
		}

		return $citations;
	}

	//
	// function to identify metadata and extract it into a key-value array
	//
	function extractMetadata() {
		$xpath =& new DOMXPath($this->contentDOM);
		$metadata_nodes =& $xpath->query("//*[@metadata != '' and normalize-space(.) != '']", $this->contentDOM);

		foreach ($metadata_nodes as $metadata_node) {
			$metadata_node->removeAttribute('metadata');

			// get the full XML structure of this node
			$metadata[] = $this->contentDOM->saveXML($metadata_node);
		}

		// pass the array of nodes into the metadata parser and get back a structured key-value array
		if (!empty($this->metadata_holder)) {
			$metadata = $this->MetadataParser->analyzeMetadata(array_merge($this->metadata_holder, $metadata));
		} else {
			$metadata = $this->MetadataParser->analyzeMetadata($metadata);
		}

		return $metadata;
	}
}

?>
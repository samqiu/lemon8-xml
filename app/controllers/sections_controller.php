<?php

/**
 * sections_controller.php
 *
 * Copyright (c) 2008-2009 MJ Suhonos
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Controller for document sections
 */

class SectionsController extends AppController {

    var $name = 'Sections';
	var $othAuthRestrictions = "*";				// list of restrictions that require authentication

	// redirect to login
    function index()
    {
     	$this->redirect('/users');
    }

	// move a section up in the list
	function moveup($sectionId = null, $delta = 1){
        $section = $this->Section->findById($sectionId);
        $this->Section->id = $section['Section']['id'];

		$this->Section->moveup($this->Section->id, abs($delta));

        $this->redirect('/documents/sections_edit/'.$section['Document']['id']);
    }

	// move a section down in the list
	function movedown($sectionId = null, $delta = 1){
        $section = $this->Section->findById($sectionId);
        $this->Section->id = $section['Section']['id'];

		$this->Section->movedown($this->Section->id, abs($delta));

        $this->redirect('/documents/sections_edit/'.$section['Document']['id']);
    }

	// remove a section from the list
	function remove($sectionId = null){
        $section = $this->Section->findById($sectionId);
        $this->Section->id = $section['Section']['id'];

		$this->Section->removefromtree($this->Section->id, true);

        $this->redirect('/documents/sections_edit/'.$section['Document']['id']);
    }

}

?>
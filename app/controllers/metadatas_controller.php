<?php

/**
 * metadatas_controller.php
 *
 * Copyright (c) 2008-2009 MJ Suhonos
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Controller for metadata
 */

class MetadatasController extends AppController {

    var $name = 'Metadatas';
	var $othAuthRestrictions = "*";				// list of restrictions that require authentication

	// redirect to login
    function index()
    {
     	$this->redirect('/users');
    }

	// delete a metadata element from a document
	function delete($metadataId = null)
	{
		// validate metadata ID belongs to the current document
		$this->_checkID($metadataId);

		// TODO: rationalize/check this against the current session document ID
		$documentId = $this->metadata['Document']['id'];

		$this->Metadata->del($metadataId, true);

		// redirect to the edit view
		$this->redirect('/documents/metadata_edit/'.$documentId);
	}

	//
	// ========== Internal methods below here ==========
	//

	// ensure we have a valid document, given a document ID
	function _checkID($metadataId) {

		// check valid document ID is provided
		if (is_numeric($metadataId)) {

			if ($metadata =& $this->Metadata->findById($metadataId))
				$this->metadata =& $metadata;
			else
				// otherwise: couldn't find a document in the DB with that ID
				$this->flash('Unable to find a metadata element with that ID', '/');

		// otherwise: non-numeric or empty ID provided, return with error
		} else $this->flash('Please provide a valid metadata ID', '/');

	}
}

?>
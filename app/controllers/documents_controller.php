<?php

/**
 * documents_controller.php
 *
 * Copyright (c) 2008-2009 MJ Suhonos
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Controller for documents
 */

class DocumentsController extends AppController {

    var $name = 'Documents';
	var $othAuthRestrictions = "*";				// list of restrictions that require authentication

	var $components = array('RequestHandler', 'StyleAnalyzer', 'DocumentParser');
	var $helpers = array('Ajax');

	// redirect to login
    function index()
    {
     	$this->redirect('/users');
    }

	// delete a document for this user
	function delete($documentId = null)
	{
		// validate document ID belongs to the current user
		$this->_checkID($documentId);

		$this->Document->Metadata->deleteAll("Metadata.document_id = ".$documentId);
		$this->Document->Section->deleteAll("Section.document_id = ".$documentId);
		$this->Document->Citation->deleteAll("Citation.document_id = ".$documentId);
		$this->Document->delete();

		// redirect to the user view
		$this->redirect('/users');
	}

	// upload a  file for processing
	function upload()
	{
		// check HTTP-POST file upload variables
        if (!empty($this->data) && is_uploaded_file($this->data['Documents']['File']['tmp_name'])) {

			// import the PEAR Archive-Zip class to extract ODT contents
			// TODO: I believe this has been replaced by a newer PEAR library
			App::import('Vendor', 'zip', array('file' => 'PEAR/Archive/Zip.php'));

			// check file extension and MIME-type and invoke Docvert if necessary/possible
			if ($this->data['Documents']['File']['type'] != 'application/octet-stream'
				&& strpos($this->data['Documents']['File']['name'], '.odt') === false
				&& $this->Session->read('Settings.convert.component')) {

				$component = $this->Session->read('Settings.convert.component');
				$converter = AppController::enableComponent(Inflector::camelize($component));

				// pass the entire file array/object to send it intelligently
				$filename = $converter->convert($this->data['Documents']['File'], $this->Session->read('Settings.convert.'.$component));

			} else $filename = $this->data['Documents']['File']['tmp_name'];

				// unzip ODT contents
				$zipfile = new Archive_Zip($filename);

				// extract the pertinent files: content.xml, styles.xml, meta.xml
				$file_content = $zipfile->extract(array('by_name' => array('content.xml'), 'extract_as_string' => true));
				$file_style = $zipfile->extract(array('by_name' => array('styles.xml'), 'extract_as_string' => true));
				$file_meta = $zipfile->extract(array('by_name' => array('meta.xml'), 'extract_as_string' => true));

				// normalize all data into UTF-8 before committing to database
			    // this is to ensure we can properly parse it as XML later on
				$data['doc_content'] = utf8_normalize($file_content[0]['content']);
				$data['doc_styles'] = utf8_normalize($file_style[0]['content']);
				$data['doc_meta'] = utf8_normalize($file_meta[0]['content']);

				$data['filename'] = $this->data['Documents']['File']['name'];
				$data['filesize'] = $this->data['Documents']['File']['size'];

				$data['user_id'] = $this->othAuth->user('id');

				// extract image files and store in files folder
				// TODO: save this information somewhere
				$extracted_figures = $zipfile->extract(array('add_path' => WWW_ROOT . 'files/', 'by_preg' => '/Pictures.*/'));

				// save the current document object into the DB
			    if (!$this->Document->save($data)) {
			    	$this->Session->setFlash('Error saving document to database.');
			    	$this->redirect('/users');
			    }

				// set the document ID for the session
				$this->Session->write('documentId', $this->Document->id);

				// redirect to the edit view
				$this->redirect('/documents/parse/'.$this->Document->id);

		// otherwise: we have no file uploaded, return or error
        } else {
			$this->Session->setFlash('Please select a file to upload.');
			$this->redirect('/users');
        }
	}

	// parse a document and generate a data array from it
	// at some point this will be more interactive
	function parse($documentId = null) {

		// validate document ID and get document
		$this->_checkID($documentId);

		// get data for the document
		$this->_getDocumentStatus($documentId);

		// get the pertinent XML files and send them into the style parser for marking
		$cleanedContentXML = $this->StyleAnalyzer->analyzeStyles($this->document['Document']['doc_styles'],
																										$this->document['Document']['doc_content'],
																										$this->Session->read('Settings.default_size'));

		// send the document metadata XML and content into the document parser for analysis
		$result_array = $this->DocumentParser->analyzeStructure($this->document['Document']['doc_meta'], $cleanedContentXML);

		// generate section hierarchy
		array_nest($result_array['sections']);

		// delete any old document data
		$this->Document->Metadata->deleteAll("Metadata.document_id = ".$documentId);
		$this->Document->Section->deleteAll("Section.document_id = ".$documentId);
		$this->Document->Citation->deleteAll("Citation.document_id = ".$documentId);

		// create new objects for each metadata element and save to DB
		foreach (array_merge($result_array['statistics'], $result_array['metadata']) as $element => $value) {
			$this->Document->Metadata->create();

			$data['document_id'] = $this->Document->id;
			$data['element'] = $element;
			$data['value'] = $value ? $value : '';

			$this->Document->Metadata->save($data);
		}

		// create new objects for each section and save them to the DB
		// TODO: rationalize figures that are uploaded from zip, but not detected by parser
		foreach ($result_array['sections'] as &$section) {
			$this->Document->Section->create();

			$section['document_id'] = $this->Document->id;
			if (!is_null($section['parent'])) $section['parent_id'] = $result_array['sections'][$section['parent']]['id'];

			$this->Document->Section->save($section);
			$section['id'] = $this->Document->Section->getLastInsertID();
		}

		// create new objects for each citation and save them to the DB
		foreach ($result_array['citations'] as $contents) {
			$this->Document->Citation->create();

			$data['document_id'] = $this->Document->id;
			$data['contents'] = $contents;

			$this->Document->Citation->save($data);

			// add new citation id to array for parsing
			$citations[] = $this->Document->Citation->id;
		}

		// display results of decomposition; show some feedback
		$this->pageTitle = 'Parse Document';
		$this->set('document', $this->document);
		$this->set('results', $result_array);
		$this->set('citations', $citations);
	}

	// edit document metadata
	function metadata_edit($documentId = null) {

		// validate document ID and get document
		$this->_checkID($documentId);

		// get data for the document
		$this->_getDocumentStatus($documentId);

		// pass metadata through to view
		$this->pageTitle = 'Edit Metadata';
		$this->set('document', $this->document);
		$this->set('metadata', $this->metadata);
	}

	// save updated document metadata
	function metadata_save($documentId = null) {

		// validate document ID and get document
		$this->_checkID($documentId);

		// see if we're adding an author / affiliation
		if (isset($this->params['form']['add_author'])) {
			$this->Document->Metadata->create(array('document_id' => $this->Document->id, 'element' => 'author'));
			$this->Document->Metadata->save();

		} elseif (isset($this->params['form']['add_affiliation'])) {
			$this->Document->Metadata->create(array('document_id' => $this->Document->id, 'element' => 'affiliation'));
			$this->Document->Metadata->save();

		} else {
			// save each of the POST variables to the DB as metadata elements
			$this->Document->saveMetadata($this->data['Metadata']);
		}

		// update document modification date
		$this->Document->save();

		// redirect to the edit view
		$this->redirect('/documents/metadata_edit/'.$documentId);

	}

	// display sections and figures to edit
	function sections_edit($documentId = null) {

		// validate document ID and get document
		$this->_checkID($documentId);

		// get status for the document
		$this->_getDocumentStatus($documentId);

		// pass variables through to view
		$this->pageTitle = 'Sections';
		$this->set('document', $this->document);
		$this->set('sections', $this->sections);
	}

	// save updated section information
	function sections_save($documentId = null) {

		// validate document ID and get document
		$this->_checkID($documentId);

		// if the image variable is set, we're uploading a new figure
		$images = array_clean(Set::extract($this->data['Image'], '{n}.name'));
		if (!empty($images)) {

			// point to the uploaded data
			foreach ($this->data['Image'] as $id => $file_array) {
				if (!empty($file_array['name'])) {
					break;
				}
			}

			// move the uploaded file into the web folder
			$contents = 'Pictures/'.$file_array['name'];
			$filename = WWW_ROOT . 'files/'.$contents;
			move_uploaded_file($file_array['tmp_name'], $filename);

			// set new data for the section
			$this->Document->Section->save(array('id' => $id, 'contents' => $contents));

		} else {
			// update any modified titles
			foreach ($this->data['Section'] as $id => $title) {
				$this->Document->Section->save(array('id' => $id, 'title' => $title));
			}
		}

		// update document modification date
		$this->Document->save();

		// redirect to the edit view
		$this->redirect('/documents/sections_edit/'.$documentId);
	}

	// display the list of citations to edit
	function citations_edit($documentId = null) {

		// validate document ID and get document
		$this->_checkID($documentId);

		// get status for the document
		$this->_getDocumentStatus($documentId);

		// pass citation list through to view
		$this->pageTitle = 'Citations';

		$this->set('document', $this->document);
		$this->set('citations', $this->citations);
	}

	// add a new citation to the document
	function citation_add($documentId = null) {
		// validate document ID and get document
		$this->_checkID($documentId);

		$this->Document->Citation->create(array('document_id' => $this->Document->id, 'contents' => '(paste citation here)'));
		$this->Document->Citation->save();

		// redirect to the edit view
		$this->redirect('/documents/citations_edit/'.$documentId.'#citation-content-'.$this->Document->Citation->id);
	}
	// display document preview and options to save/export
	function preview_save($documentId = null) {
		// validate document ID and get document
		$this->_checkID($documentId);

		// get status for the document
		$this->_getDocumentStatus($documentId);

		// pass variables through to view
		$this->pageTitle = 'Preview/Save';
		$this->set('previewHTML', $this->_preview('nlm-xhtml'));
		$this->set('document', $this->document);
	}


	// convert document into XML and export
	function export($documentId = null) {

		// validate document ID and get document
		$this->_checkID($documentId);

		// get status for the document
		$this->_getDocumentStatus($documentId);

		// generate XML DOM object from XSL transformer
		$schema = $this->data['Documents']['schema'];
		$exportDOM =& $this->_generateOutputXML($schema);
		$exportDOM->formatOutput = true;

		// TODO: create a zip file to send to client

		// turn debugging off and send output to the browser
		Configure::write('debug', 0);
		header('Content-type: text/xml');
//		header('Content-Disposition: attachment; filename="lemon8-'.$schema.'-'.$documentId.'.xml"');

		echo $exportDOM->saveXML();
		exit;
	}

	// convert document into PDF preview and display
	function previewPDF($documentId = null) {

		// validate document ID and get document
		$this->_checkID($documentId);

		// get status for the document
		$this->_getDocumentStatus($documentId);

		// generate preview FO using XSL
		$previewFO = $this->_preview('nlm-fo');

		// generate preview PDF using FOP

		// output FO XML into temporary file
		$tempFO = new File(TMP.$documentId.'-fo.xml', true);
        $tempFO->append($previewFO);

		// set shell command and arguments for FO transform
		$fopDir = ROOT . DS . APP_DIR . DS . 'vendors' . DS . 'FOP';

		$fopCommand = $fopDir . DS . 'fop -fo '.$tempFO->pwd().' -pdf '. TMP.$documentId.'.pdf';

		// check for safe mode and escape the shell command
		if( !ini_get('safe_mode') ) $fopCommand = escapeshellcmd($fopCommand);

		// run the shell command and get the results
		$contents = $status = '';
		exec($fopCommand . ' 2>&1', $contents, $status);

		// if there is an error, spit out the shell results to aid debugging
		if ($status != false) {
			// TODO: handle error condition gracefully
			if ($contents != '') echo nl2br(implode("\n", $contents));
		}

		// delete the temporary FO file
		$tempFO->delete();

		// turn debugging off and send preview PDF to the browser
		Configure::write('debug', 0);
		header('Content-type: application/pdf');
		readfile(TMP.$documentId.'.pdf');

		// delete the temporary PDF file
		unlink(TMP.$documentId.'.pdf');

		exit;
	}

	//
	// ========== Internal methods below here ==========
	//

	// ensure we have a valid document, given a document ID
	function _checkID($documentId) {

		// if no ID is provided, check the session
		if (!$documentId) {
			if ($this->Session->check('documentId')) $documentId = $this->Session->read('documentId');
		}

		// check valid document ID is provided
		if (is_numeric($documentId)) {

			// retrieve unassociated document data
			if ($document = $this->Document->findById($documentId, null, null, 0)) {

				// make sure this document belongs to this user
				if ($this->othAuth->user('id') == $document['Document']['user_id']) {
					// set the global document array
					$this->document =& $document;
				} else {
					// document and user don't match
					$this->Session->setFlash('Document belongs to another user.');
					$this->redirect('/users');
				}
			} else {
				// couldn't find a document in the DB with that ID
				$this->Session->setFlash('No document with that ID.');
				$this->redirect('/users');
			}
		} else {
			// non-numeric or empty ID provided
			$this->Session->setFlash('Not a valid document ID.');
			$this->redirect('/users');
		}

		// set the document ID for the session
		$this->Session->write('documentId', $documentId);
	}

	// display detailed status info for the document
	// TODO: this is probably totally unnecessary now
	function _getDocumentStatus($documentId = null) {

		// retrieve associated model data
		$this->Document->Metadata->unbindAll();
		$this->metadata =& $this->Document->Metadata->findAllThreaded("Metadata.document_id = ".$documentId);

		$this->Document->Section->unbindAll();
		$this->sections =& $this->Document->Section->findAllThreaded("Section.document_id = ".$documentId, null, 'lft');

		$this->Document->Citation->unbindAll(array('hasMany'=>array('Metadata')));
		$this->citations =& $this->Document->Citation->findAllByDocumentId($documentId, null, 'lft');

		// get metadata, determine number of each kind of 'important' element (for document status page)
		$authors = array_keys(Set::extract($this->metadata, '{n}.Metadata.element'), 'author');
		$affiliations = array_keys(Set::extract($this->metadata, '{n}.Metadata.element'), 'affiliation');

		// 1) metadata validity
		if (count($authors) == 0) $valid_metadata = 0;													// need at least one author
		elseif (count($affiliations) == 0) $valid_metadata = 0;										// need at least one affiliation
		elseif (count($affiliations) > count($authors) * 2) $valid_metadata = 1;				// at most we should have 2 affiliations per author (warn)
		else $valid_metadata = 3;

		// 2) section validity
		if (count($this->sections) == 0) $valid_sections = 0;
			// need at least one section
		else $valid_sections = 3;

		// 3) citation validity
		$cite_modified = $this->Document->Citation->findCount('Citation.modified > Citation.created AND document_id = '.$documentId);
		$cite_validated = $this->Document->Citation->findCount('Citation.validated > Citation.created AND document_id = '.$documentId);

		if ($cite_modified == 0 && count($this->citations) > 0) $valid_citations = 0;
			// citations should be edited at least once
		elseif ($cite_modified < count($this->citations) * 0.5) $valid_citations = 1;
			// at least 50% of citations should be edited (warn)
		elseif ($cite_validated < count($this->citations) * 0.75) $valid_citations = 1;
			// at least 75% of citations should be validated (warn)
		elseif (count($this->citations) == 0) $valid_citations = 1;
			// editorials may not have citations (warn)
		else $valid_citations = 3;

		// 4) document validity (based on aggregate validity)
		if (!$valid_metadata || !$valid_sections || !$valid_citations) $valid_document = 0;
			// one or more parts contain errors
		elseif (($valid_metadata + $valid_sections + $valid_citations) < 9) $valid_document = 1;
			// one or more parts contain warnings
		else $valid_document = 3;

		// make validity variables available to view (for document nav)
		$this->set('valid_metadata', $valid_metadata);
		$this->set('valid_sections', $valid_sections);
		$this->set('valid_citations', $valid_citations);
		$this->set('valid_document', $valid_document);
	}

	// create the transformed XML for a specific schema from the document data in the DB
	function _generateOutputXML($schema = '') {
		// instantiate the input DOM object
		$inputDOM = new DomDocument('1.0', 'UTF-8');
		$inputDOM->loadXML('<document/>');

		// instantiate an xpath object for this DOM
		$xpath = new DOMXPath($inputDOM);

		// create a temporary DOM document to unserialize array hierarchy
		$tempDOM = new DOMDocument();

		// turn metadata array into a DOM document
		$tempDOM->loadXML(utf8_normalize(array_to_xml(array('metadata' => $this->metadata))));
		$inputDOM->documentElement->appendChild($inputDOM->importNode($tempDOM->documentElement, true));

		// metadata contents need to be turned into valid XML
		$nodes =& $xpath->query("/document/metadata/*/Metadata/value[. != '']", $inputDOM);
		foreach ($nodes as $node) {
			$abstractDF = $inputDOM->createDocumentFragment();
			$abstractDF->appendXML($node->nodeValue);
			$node->replaceChild($abstractDF, $node->firstChild);
		}

		// turn sections array into a DOM document
		$tempDOM->loadXML(utf8_normalize(array_to_xml(array('sections' => $this->sections))));
		$inputDOM->documentElement->appendChild($inputDOM->importNode($tempDOM->documentElement, true));

		// section contents need to be turned into valid XML
		$nodes =& $xpath->query('/document/sections//Section/contents', $inputDOM);
		foreach ($nodes as $node) {
			$contentDF = $inputDOM->createDocumentFragment();
			$contentDF->appendXML($node->nodeValue);

			if ($node->hasChildNodes()) $node->replaceChild($contentDF, $node->firstChild);

			// look for in-text references in the content
			foreach ($xpath->query("descendant-or-self::*[text()]", $node) as $element) {

				// only process text nodes to extract references
				foreach ($element->childNodes as $textNode) {
					if ($textNode->nodeType == XML_TEXT_NODE) {
						$element->replaceChild($this->_extractRefNodes($textNode), $textNode);
					}
				}
			}
		}

		// turn citations array into a DOM document
		// NB: NOT UTF-8 normalized for performance reasons
		$tempDOM->loadXML(array_to_xml(array('citations' => $this->citations)));
		$inputDOM->documentElement->appendChild($inputDOM->importNode($tempDOM->documentElement, true));

		// if a schema is specified, transform using XSLT
		if ($schema != '') {
			// create a new XSLT processor with PHP functions enabled
			$proc = new XsltProcessor();
			$proc->registerPHPFunctions();

			// transform the section using the XSL for this schema
			$xsl = dirname(__FILE__) . DS . 'components/stylesheets/export/'.$schema.'.xsl';
			$tempDOM->load($xsl);
			$proc->importStylesheet($tempDOM);
			$outputDOM =& $proc->transformToDoc($inputDOM);

			// send back the transformed DOM XML object
			return $outputDOM;
		} else {
			return $inputDOM;
		}
	}

	// generate HTML/PDF preview of document
	function _preview($stylesheet) {

		// generate XML DOM object from XSL transformer; use NLM for preview
		$previewDOM =& $this->_generateOutputXML("nlm");

		// generate XHTML/FO from DOM using OJS XSL sheets
		$proc = new XsltProcessor();

		// create a temporary DOM document for loading XSL
		$tempDOM = new DOMDocument();

		// transform the section using the XSL for this schema
		$xsl = dirname(__FILE__) . DS . 'components/stylesheets/preview/'.$stylesheet.'.xsl';
		$tempDOM->load($xsl);
		$proc->importStylesheet($tempDOM);

		// set the parameter for image locations for PDF
		$proc->setParameter('', 'image_dir', WWW_ROOT . 'files/');
		$previewXML = $proc->transformToXML($previewDOM);

		// rewrite image locations for XHTML
		if ($stylesheet == 'nlm-xhtml') {
			$previewXML = preg_replace('/<img src="/', '<img src="'.dirname($_SERVER['PHP_SELF']).'/files/', $previewXML);
		}

		return $previewXML;
	}

	// parse an XML string for text that looks like in-text references, and
	// return a DOM document fragment with them transformed into subnodes
	function _extractRefNodes($textNode) {
		$referenceDF = $textNode->ownerDocument->createDocumentFragment();

		// what we consider to be text-embedded references
		$regexRefs = "/([\[\(][\s\d,-]+[\]\)])|(^[\s\d,-]+$)/";

		if (preg_match($regexRefs, $textNode->nodeValue)) {
			// break the text up into pieces and loop through them
			$text_parts = preg_split($regexRefs, $textNode->nodeValue, -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);

			foreach ($text_parts as $part) {
				// transform reference text into DOM nodes

				if (preg_match($regexRefs, $part)) {
					$ref_parts = preg_split("/(\d+)/", trim($part), -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);

					foreach ($ref_parts as $ref) {
						if (preg_match("/(\d+)/", $ref) && $ref > 0 && $ref <= count($this->citations)) {

							// add a child ref node to result node
							$refNode = $referenceDF->ownerDocument->createElement("reference");
							$refNode->appendChild($referenceDF->ownerDocument->createTextNode($ref));

							$referenceDF->appendChild($refNode);
						} else {
							// add separator (,-) text node child to result node
							$referenceDF->appendChild($referenceDF->ownerDocument->createTextNode($ref));
						}
					}
				} else {
					// add text node child to result node
					$referenceDF->appendChild($referenceDF->ownerDocument->createTextNode($part));
				}
			}

			return $referenceDF;

		} else {
			return $textNode;
		}
	}

}

?>
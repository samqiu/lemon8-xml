<?php

/**
 * users_controller.php
 *
 * Copyright (c) 2008-2009 MJ Suhonos
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 */

class UsersController extends AppController {

    var $name = 'Users';
	var $othAuthRestrictions = null;				// list of restrictions that require authentication

	// "home" page; this behaves differently depending on whether the user is logged in or not
    function index()
    {
		// if we have a logged-in user, display the control panel; otherwise, display a login screen
		$id = $this->othAuth->user('id');
		if ($id) {
			$this->User->Document->unbindAll(array('hasMany'=>array('Metadata')));
			$documents = $this->User->Document->findAllByUserId($id);

			// get the user information
			$this->User->unbindAll(array('hasMany'=>array('Settings')));
			$this->user = $this->User->findById($id);

			$this->set('user', $this->user);
			$this->set('documents', $documents);
		} else {
			$this->set('url', isset($this->params['url']['from']) ? $this->params['url']['from'] : null);
		}

		// get a list of available components and their settings
		$this->set('parseComponents', AppController::getComponents('parse'));
		$this->set('lookupComponents', AppController::getComponents('lookup'));
		$this->set('convertComponents', AppController::getComponents('convert'));
    }

	// attempt to login and redirect to index
	function login()
	{
    	if(isset($this->params['data']))
    	{
        	$auth_num = $this->othAuth->login($this->params['data']['User']);
	        $this->set('auth_msg', $this->othAuth->getMsg($auth_num));
    	}

		$id = $this->othAuth->user('id');
		if ($id) {
	    	// load settings from the database into session
	    	$this->User->Setting->unbindAll();
	    	$this->settings = $this->User->Setting->findAllByUserId($id, array('name', 'value'));

	    	$settings = Set::combine($this->settings, '{n}.Setting.name', '{n}.Setting.value');
	    	foreach ($settings as $name => $value) {
				$this->Session->write('Settings.'.$name, $value);
	    	}
		}

		$this->redirect('/users');
	}

	// logout and redirect to home page
	function logout()
	{
	    $this->othAuth->logout();
	    $this->redirect($this->othAuth->logout_page);
	}

	// save settings/profile from form data
	function save() {
		// get the user information
		$id = $this->othAuth->user('id');

		if (empty($id)) {
	    	$this->redirect('/users');
		}

		// if we have form variables update settings or profile
    	if (isset($this->params['data'])) {
    		if (isset($this->params['data']['User'])) {

				// check that current password is correct
				$authData = $this->othAuth->getData('User');
				$hashedPass = $this->othAuth->_getHashOf($this->params['data']['User']['password']);

				if ($authData['passwd'] != $hashedPass) {
					$this->Session->setFlash('Current password is incorrect.');
				} else {
					// check that new passwords match
					if ($this->params['data']['User']['newpass'] != $this->params['data']['User']['newpass2']) {
						$this->Session->setFlash('New passwords do not match.');

					} else {
						// set data to be saved for the profile
						$data['id'] = $id;
						$data['name'] = $this->params['data']['User']['name'];
						$data['email'] = $this->params['data']['User']['email'];
						if ('' != $this->params['data']['User']['newpass']) {
							$data['passwd'] = $this->othAuth->_getHashOf($this->params['data']['User']['newpass']);
						}
						$this->User->save($data);

						// update session settings
						$this->Session->write('othAuth.' .$this->othAuth->hashkey. '.User.name', $data['name']);
						$this->Session->write('othAuth.' .$this->othAuth->hashkey. '.User.email', $data['email']);
						if (!empty($data['passwd'])) {
							$this->Session->write('othAuth.' .$this->othAuth->hashkey. '.User.passwd', $data['passwd']);
						}

						$this->Session->setFlash('Profile saved.');
					}
				}

				// don't carry settings through
				unset($this->data['User']);

    		} elseif (isset($this->params['data']['Setting'])) {
				// set user variables for settings
				$this->User->Setting->deleteAll("Setting.user_id = ".$id);
				$this->Session->del('Settings');

				// generate a list of settings to update
				$data['user_id'] = $id;
				$flattened = Set::flatten($this->params['data']['Setting'], '.');

				foreach ($flattened as $name => $value) {
					if (!empty($value)) {
						$this->User->Setting->create();
						$data['name'] = $name;
						$data['value'] = $value;
						$this->User->Setting->save($data);

						// update session settings
						$this->Session->write('Settings.'.$data['name'], $data['value']);
					}
				}

				$this->Session->setFlash('Settings saved.');
    		}
    	}

		$this->User->Document->unbindAll(array('hasMany'=>array('Metadata')));
		$documents = $this->User->Document->findAllByUserId($id);

		// get the user information
		$this->User->unbindAll(array('hasMany'=>array('Settings')));
		$this->user = $this->User->findById($id);
		$this->set('user', $this->user);
		$this->set('documents', $documents);

		// get a list of available components and their settings
		$this->set('parseComponents', AppController::getComponents('parse'));
		$this->set('lookupComponents', AppController::getComponents('lookup'));
		$this->set('convertComponents', AppController::getComponents('convert'));

		$this->render('index');
	}

	// notify user of insufficient auth and redirect to login
	function noaccess()
	{
		$this->Session->setFlash('You don\'t have permissions to access this page.');
    	$this->redirect('/users');
	}

}

?>
# CocoaMySQL dump
# Version 0.7b5
# http://cocoamysql.sourceforge.net
#
# Host: localhost (MySQL 5.0.45)
# Database: lemon8
# Generation Time: 2008-09-30 10:41:45 -0400
# ************************************************************

# Dump of table citations
# ------------------------------------------------------------

CREATE TABLE `citations` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `document_id` int(10) NOT NULL default '0',
  `parent_id` int(10) default NULL,
  `lft` int(10) default NULL,
  `rght` int(10) default NULL,
  `contents` text NOT NULL,
  `marked_contents` text default NULL,
  `created` datetime default NULL,
  `modified` datetime default NULL,
  `validated` datetime default NULL,
  `parse_score` int(10) NOT NULL default '0',
  `lookup_score` int(10) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;



# Dump of table documents
# ------------------------------------------------------------

CREATE TABLE `documents` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `user_id` int(10) NOT NULL default '0',
  `doc_content` longtext NOT NULL,
  `doc_styles` mediumtext NOT NULL,
  `doc_meta` text NOT NULL,
  `filename` text NOT NULL,
  `filesize` int(10) NOT NULL,
  `created` datetime default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;



# Dump of table groups
# ------------------------------------------------------------

CREATE TABLE `groups` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `name` varchar(128) NOT NULL,
  `level` int(10) NOT NULL,
  `redirect` varchar(50) NOT NULL,
  `perm_type` enum('allow','deny') NOT NULL default 'allow',
  `created` datetime default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

INSERT INTO `groups` (`id`,`name`,`level`,`redirect`,`perm_type`) VALUES ('1','admins','100','','allow');
INSERT INTO `groups` (`id`,`name`,`level`,`redirect`,`perm_type`) VALUES ('2','editors','200','','allow');
INSERT INTO `groups` (`id`,`name`,`level`,`redirect`,`perm_type`) VALUES ('3','members','300','','allow');



# Dump of table groups_permissions
# ------------------------------------------------------------

CREATE TABLE `groups_permissions` (
  `group_id` int(10) unsigned NOT NULL default '0',
  `permission_id` int(10) unsigned NOT NULL default '0',
  KEY `group_id` (`group_id`,`permission_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `groups_permissions` (`group_id`,`permission_id`) VALUES ('1','1');
INSERT INTO `groups_permissions` (`group_id`,`permission_id`) VALUES ('2','1');



# Dump of table metadatas
# ------------------------------------------------------------

CREATE TABLE `metadatas` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `document_id` int(10) NOT NULL default '0',
  `citation_id` int(10) NOT NULL default '0',
  `parent_id` int(10) default NULL,
  `lft` int(10) default NULL,
  `rght` int(10) default NULL,
  `element` text NOT NULL,
  `value` text NOT NULL,
  `created` datetime default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;



# Dump of table permissions
# ------------------------------------------------------------

CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `name` varchar(128) NOT NULL default '',
  `created` datetime default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

INSERT INTO `permissions` (`id`,`name`) VALUES ('1','*');



# Dump of table sections
# ------------------------------------------------------------

CREATE TABLE `sections` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `document_id` int(10) NOT NULL default '0',
  `parent_id` int(10) default NULL,
  `lft` int(10) default NULL,
  `rght` int(10) default NULL,
  `type` varchar(128) NOT NULL,
  `title` tinytext NOT NULL,
  `contents` mediumtext NOT NULL,
  `created` datetime default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;



# Dump of table users
# ------------------------------------------------------------

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `username` varchar(32) NOT NULL default '',
  `passwd` varchar(32) NOT NULL default '',
  `name` varchar(128) NOT NULL default '',
  `email` varchar(128) NOT NULL default '',
  `group_id` int(10) unsigned NOT NULL default '0',
  `active` tinyint(1) unsigned NOT NULL default '0',
  `last_visit` datetime default NULL,
  `created` datetime default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`,`username`),
  KEY `group_id` (`group_id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

# Dump of table settings
# ------------------------------------------------------------

CREATE TABLE `settings` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `user_id` int(10) unsigned NOT NULL default '0',
  `group_id` int(10) unsigned NOT NULL default '0',
  `name` tinytext NOT NULL,
  `value` tinytext NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY  (`id`),
  KEY `user_id` (`user_id`,`group_id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

INSERT INTO `settings` (`id`,`user_id`,`group_id`,`name`,`value`) VALUES (1,1,0,'doc_parse_margin',60);
INSERT INTO `settings` (`id`,`user_id`,`group_id`,`name`,`value`) VALUES (2,1,0,'heading_margin',55);
INSERT INTO `settings` (`id`,`user_id`,`group_id`,`name`,`value`) VALUES (3,1,0,'min_words',40);
INSERT INTO `settings` (`id`,`user_id`,`group_id`,`name`,`value`) VALUES (4,1,0,'min_chars',300);
INSERT INTO `settings` (`id`,`user_id`,`group_id`,`name`,`value`) VALUES (5,1,0,'default_size',12);
INSERT INTO `settings` (`id`,`user_id`,`group_id`,`name`,`value`) VALUES (6,1,0,'parse_margin',55);
INSERT INTO `settings` (`id`,`user_id`,`group_id`,`name`,`value`) VALUES (7,1,0,'parse.component.freecite','on');
INSERT INTO `settings` (`id`,`user_id`,`group_id`,`name`,`value`) VALUES (8,1,0,'parse.component.paracite','on');
INSERT INTO `settings` (`id`,`user_id`,`group_id`,`name`,`value`) VALUES (9,1,0,'parse.component.parscit','on');
INSERT INTO `settings` (`id`,`user_id`,`group_id`,`name`,`value`) VALUES (10,1,0,'parse.component.regex_parse','on');
INSERT INTO `settings` (`id`,`user_id`,`group_id`,`name`,`value`) VALUES (11,1,0,'parse.paracite.parsers','Standard, Citebase, Jiao');
INSERT INTO `settings` (`id`,`user_id`,`group_id`,`name`,`value`) VALUES (12,1,0,'lookup_margin',75);
INSERT INTO `settings` (`id`,`user_id`,`group_id`,`name`,`value`) VALUES (13,1,0,'lookup.component.crossref','on');
INSERT INTO `settings` (`id`,`user_id`,`group_id`,`name`,`value`) VALUES (14,1,0,'lookup.component.isbndb','on');
INSERT INTO `settings` (`id`,`user_id`,`group_id`,`name`,`value`) VALUES (15,1,0,'lookup.component.pubmed','on');
INSERT INTO `settings` (`id`,`user_id`,`group_id`,`name`,`value`) VALUES (16,1,0,'lookup.component.worldcat','on');

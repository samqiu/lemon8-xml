/*
*	General site-wide Javascript functions
*/

function updateFields(select, id) {
	genre = select.options[select.selectedIndex].value;

	// hide everything
	$$('table#citations tr').each(Element.hide);

	// show as appropriate
	$$('table#citations tr.genre').each(Element.show);
	$$('table#citations tr.buttons').each(Element.show);
	$$('table#citations tr.' + genre).each(Element.show);
}

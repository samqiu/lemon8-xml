<html>
    <head><title>XSL Transformation Test</title></head>
    <style type="text/css">

        #outcome {
                  padding: 5px; 
                  border: 1px solid black; 
                  color: green; 
                  background-color: #ffffcc; 
                  width: 600px; 
                  text-align: center;
                 }
    
    </style>
    <body>
<?php

$doc = new DOMDocument();
$doc->loadXML('<?xml version="1.0"?>
                   <content><text>If you see this, excellent.  
                            <bold>This should be in a heavier font weight.</bold></text>
                   </content>
              ');

$doc->saveXML();

$xsl = new DOMDocument();
$xsl->loadXML(' <xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
                    <xsl:template match="/">
                      <p id="outcome">
                         <xsl:apply-templates />
                      </p>
                    </xsl:template>
                    <xsl:template match="bold">
                        <strong><xsl:apply-templates /></strong>
                    </xsl:template>
                </xsl:stylesheet>');

$xsl->saveXML();

$proc = new XSLTProcessor;
$proc->importStyleSheet($xsl);

print $proc->transformToXML($doc);

?>
<p align="center"><input type="button" onClick="self.close()" value="Close" /></p>
    </body>
</html>
<?php

/**
 * app_controller.php
 *
 * Copyright (c) 2008-2009 Jason Nugent
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Installer script to make life easier for everyone involved.
 *
 */

if (!defined('DS')) {
	define('DS', DIRECTORY_SEPARATOR);
}

$cache_dir = '..' . DS . 'tmp' . DS . 'cache';
$files_dir = '..' . DS . 'webroot' . DS . 'files';

?>

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="/lemon8/css/style.css" />

<title>Welcome to the Lemon8-XML Installer</title>

<style type="text/css">
.success {
	color: green;
	font-weight: bold;
	font-size: larger;
}

.failure {
	color: red;
	font-weight: bold;
	font-size: larger;
}

td,th {
	text-align: center;
}

table {
	border: 1px solid black;
	margin-bottom: 1em;
}

.info {
	border: 1px solid #ccc;
	width: 600px;
	background-color: #ffffcc;
	padding: 5px;
	text-align: center;
	margin-top: 0px;
}

table {
    border: 1px solid #ccc;
}

h2 {
	margin: 0px;
	padding: 0px;

}

</style>
</head>
<body>
<center>

<div id="nav" class="frame" style="width: 700px; text-align: left; margin-bottom: 1em; background-position: bottom right;">
<h1>Welcome to the Lemon8-XML Installer</h1>
<p style="width: 600px;">This file will help you get Lemon8-XML up and
running on your server. It will detect and attempt to set various
settings to make installation run more smoothly. You should delete this file
once you've confirmed that everything is running correctly.</p>
</div>

<!--
<p><a href="#version">PHP Version Information</a><br />
<a href="#modules">PHP Module Information</a><br />
<a href="#settings">PHP INI Settings</a><br />
<a href="#xsl">XSL Transformation Test</a><br />
<a href="#perms">Server Permissions Check</a><br />
<a href="#bootstrap">Verify that boostrap.php exists</a><br />
<a href="#loadsql">Load Lemon8-XML SQL into your Database</a></p>
-->

<?php
$loaded_extensions = get_loaded_extensions();
?>

<h2 id="version">PHP Version Information</h2>
<p class="info">Your server must be running PHP version 5.2.0 or
greater. Your version is: <strong><?php print PHP_VERSION; ?></strong><br />
<?php
$vc_status = version_compare(PHP_VERSION, '5.2.0');
if ($vc_status >= 0) {
	print '<span class="success">Your version of PHP is sufficient!</span>';
} else {
	print '<span class="failure">You will need to upgrade PHP in order to run Lemon8-XML correctly.</span>';
}
?></p>
<?php
$required_modules = array ("curl", "dom", "mysql", "libxml", "pcre", "xsl", "xml");
?>
<h2 id="modules">PHP Module Information</h2>
<p class="info">The following PHP modules are needed by Lemon8-XML in
order to work correctly.<br/>If any are missing, please install them
according to your server's configuration.</p>
<table cellpadding="5" cellspacing="0" width="610">
	<tr>
		<th>PHP Module</th>
		<th>Is it loaded?</th>
		<th>&nbsp;</th>
		<?php
		foreach ($required_modules AS $r) {

			print "<tr>";
			print "    <td>$r</td>";
			print "    <td>";

			if (extension_loaded($r))
			print '<span class="success">Yes</span></td><td>&nbsp;</td>';
			else
			print '<span class="failure">No</span><td><a target="_new" href="http://www.php.net/' . $r . '">Info</a></td>';

			print "</tr>";
		}
		?>
</table>

<h2 id="settings">PHP INI Settings</h2>
<p class="info">Lemon8-XML gives you the ability to upload documents and
have them converted to XML. To do this, your web server must have enough memory
resources allocated.<br/>If any of settings displayed below seem too low, you
should adjust their values in <strong><?php print php_ini_loaded_file(); ?></strong> and restart your web server.
</p>

		<?php
		$settings = array ("memory_limit", "post_max_size", "file_uploads", "upload_max_filesize");
		?>

<table cellpadding="5" cellspacing="0" width="610">
	<tr>
		<th>PHP Setting</th>
		<th>Current Value</th>
		<?php
		foreach ($settings AS $s) {

			print "<tr>";
			print "    <td>$s</td>";
			print "    <td>";
			print ini_get($s) == 1 ? "On" : ini_get($s);
			print "    </td>";

			print "</tr>";
		}
		?>
</table>

<h2 id="xsl">XSL Transformation Test</h2>
<p class="info">To verify that the XML and XSLT PHP modules are
installed on your server and are working correctly, you can click the
button below. It will open a small popup window and  attempt to perform
an XSL transformation. You should see a success message.<br />

<input style="margin-top: 1em;" type="button"
	onClick="window.open('xsltest.php', 'xsltest','height=100,width=650,resizable=yes,toolbar=no') ; return false"
	value="Test XSLT Functionality" /></p>

<h2 id="perms">Server Permissions Check</h2>

<p id="cache" class="info">Lemon8-XML needs to be able to write to <strong>app/tmp/cache</strong> in order to function correctly.
If this directory is not writable by the server, you can attempt to fix it by clicking "fix" below; if this does not
solve the problem, please create this directory manually and make it
writable by the web server. <br />
<br />
		<?php

		if (preg_match('/^cache/', $_SERVER['QUERY_STRING'])) {

			/* attempt to fix the app/tmp/cache directory - the user clicked on the fix button */
			if (file_exists($cache_dir) && is_dir($cache_dir)) {
				system ('chmod -R 0777 $cache_dir', $retval1);
			}
			if (file_exists($files_dir) && is_dir($files_dir)) {
				system ('chmod -R 0777 $files_dir', $retval2);
			}

			if ($retval1 == 1) {
				print "<strong>Unable to fix the permissions on app/tmp/cache automatically -- please do it manually before proceeding.</strong><br />";
			}
			if ($retval2 == 1) {
				print "<strong>Unable to fix the permissions on app/webroot/files automatically -- please do it manually before proceeding.</strong><br />";
			}
		}

		if (file_exists($cache_dir) && is_dir($cache_dir) && is_writable($cache_dir)) {
			$cache_check = true;
			print '<span class="success">app/tmp/cache exists and is writable by the server.</span><br/>';
		}
		if (file_exists($files_dir) && is_dir($files_dir) && is_writable($files_dir)) {
			$files_check = true;
			print '<span class="success">app/webroot/files exists and is writable by the server.</span><br/>';
		}

		if (!$cache_check || !$files_check) {
			print '<span class="failure">app/tmp/cache or app/webroot/files are not writable --
				<input type="button" onClick="document.location = \'install.php?cache#cache\';" name="cache" value="attempt fix"/></span>';
		}
		?></p>

<h2 id="bootstrap">Configure database.php</h2>
<p class="info">You will need to edit database.php in <strong>app<?php print DS; ?>config</strong>
 to change the database settings to reflect your MySQL database.
If this file is readable, it will be indicated below: <br />
</br>
		<?php
		$db = ".." . DS . "config" . DS . "database.php";
		$configs_exist = false;

		if (file_exists($db) && is_readable($db)) {
			print '<span class="success">database.php exists and is readable by the server.</span>';
			$configs_exist = true;
		}
		else
		print '<span class="failure">datbase.php does not exist or is not readable by the server.</span>';

		?></p>
<h2 id="loadsql">Load SQL into your Database</h2>
<p class="info">If you've edited database.php so the values for your database are
correct (and the database exists), you can click the button below to
create the necessary tables for Lemon8-XML. The installer will first test to see
if connectivity to your database is working correctly. You will <strong>only</strong>
be able to run the import if this is the case. <br />
<br />

		<?php
		if ($configs_exist) {
			require_once($db);
			$db_config = new DATABASE_CONFIG();
			$dbconn = @mysql_connect($db_config->default['host'], $db_config->default['login'], $db_config->default['password']);

	   	    if ($dbconn && @mysql_select_db($db_config->default['database'], $dbconn)) {

				if (preg_match('/^loadsql/', $_SERVER['QUERY_STRING'])) {

			    /* load the SQL */
				$sql = ".." . DS . "config" . DS . "database.sql";

					if (file_exists($sql) && is_readable($sql)) {

                                $dbsql = file($sql);
                                $queries = array();
                                $query = "";
                                foreach ($dbsql AS $line) {

                                    if (!preg_match("/^#/", $line))  {
                                        $query .= $line;
                                        if (preg_match("/;\s*$/", $line)) {
                                            $queries[] = $query;
                                            $query = "";
                                        }
                                    }
                                }

                                foreach ($queries AS $q) {
			            			$result = mysql_query($q, $dbconn);
                                }

						if ($result) {
							print '<span class="success">Your database has been populated with the necessary data!</span>';
							print '<br/><br/><strong>Create an admin user or <a href="./">Click here</a></strong> to go to your new Lemon8-XML home page.<br/><strong>Do not forget to delete this install.php file.</strong>';
						 } else {
							print mysql_error($dbconn);
						}
		             }
				} else {
					print '<span class="success">Success!  The installer was able to connect to your database.</span><br />';
					print '<br /><input type="button" onClick="document.location = \'install.php?loadsql#loadsql\';" name="loadsql" value="Load SQL structure and data" />';
				}
			} else {
				print '<span class="failure">The installer was unable to connect to your database.</span>';
	   	    }
		}

		?></p>

<h2 id="createuser">Create Initial User Account</h2>
<p class="info">If you've loaded the database structure in the previous section with no error, please enter a username
   and password for your administrative login for Lemon8-XML.<br/><strong>Please note that all fields
   are required.</strong>
<br />

		<?php
		if ($configs_exist) {
			require_once($db);
			$db_config = new DATABASE_CONFIG();
			$dbconn = @mysql_connect($db_config->default['host'], $db_config->default['login'], $db_config->default['password']);

	   	    if ($dbconn && @mysql_select_db($db_config->default['database'], $dbconn)) {

				if ($_POST['createuser'] == 'true') {

				    unset ($username, $email, $realname, $password1, $password2);

				    $username  = trim($_POST['username']);
				    $email     = trim($_POST['email']);
				    $realname  = trim($_POST['realname']);
				    $password1 = trim($_POST['password1']);
				    $password2 = trim($_POST['password2']);

				    if ($username != '' && $email != '' && $realname != '' && $password1 != '' && $password2 != '') {

				        if ($password1 == $password2) {

				            $result = mysql_query("INSERT INTO `users`
				                     (`username`,`passwd`,`name`,`email`,`group_id`,`active`)
				                     VALUES
				                     ('" . mysql_real_escape_string($username) .
				                      "','" .  md5($password1) . "','" . mysql_real_escape_string($realname) . "','" .
				                      mysql_real_escape_string($email) . "','1','1');", $dbconn);

				             if ($result) {
				                 print '<span class="success">Your user has been created.  You may create another, if you wish.</span>';
								 print '<br/><br/><strong>Or <a href="./">Click here</a></strong> to go to your new Lemon8-XML home page.<br/><strong>Do not forget to delete this install.php file.</strong>';
				             }

				        } else
				            print '<br/><span class="failure">Your two password fields do not match.  Please try again.</span>';

				    } else
				        print '<br/><span class="failure">One or more fields were left empty.</span>';
				}
	   	    }
		}
		?>

<form method="post" action="install.php#createuser">
		<input type="hidden" name="createuser" value="true" />
		  <table cellpadding="5" cellspacing="0">
		    <tr><td>Username:</td><td><input type="text" name="username" value="<?php print htmlspecialchars($username); ?>" /></td></tr>
            <tr><td>Real Name:</td><td><input type="text" name="realname" value="<?php print htmlspecialchars($realname); ?>" /></td></tr>
		    <tr><td>Email Address:</td><td><input type="text" name="email" value="<?php print htmlspecialchars($email); ?>" /></td></tr>
		    <tr><td>Password:</td><td><input type="password" name="password1" value="<?php print htmlspecialchars($password1); ?>" /></td></tr>
		    <tr><td>Password again:</td><td><input type="password" name="password2" value="<?php print htmlspecialchars($password2); ?>" /></td></tr>
		    <tr><td colspan="2" align="center"><input type="submit" value="create user" /></td></tr>
		  </table>
		</form>
		</p>
</center>
</body>
</html>
<?php

/**
 * app_controller.php
 *
 * Copyright (c) 2008-2009 MJ Suhonos
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Application-wide methods in the class below, inherited by controllers
 *
 */

App::import('Vendor', 'functions');
uses('model' . DS . 'connection_manager');

class AppController extends Controller {
	var $components  = array('p28n', 'othAuth', 'Session');
	var $helpers = array('Html', 'Javascript', 'Form', 'OthAuth', 'CountryList', 'Session');

	var $othAuthRestrictions = null;							// list of restrictions that require authentication

    function beforeFilter()
    {
		// check that database is configured; if not, redirect to installer
		$db =& ConnectionManager::getDataSource('default');
		if (empty($db->connection)) {
			$this->redirect(FULL_BASE_URL . $this->here . 'install.php');
		}

    	// use othAuth for authentication handling
        $this->othAuth->controller = &$this;
        $this->othAuth->init();
		$this->othAuth->check();

		// make sure the session settings are set
		// FIXME: this is a hack for wonky cake behaviour
		// BAD cake.  BAD cake.
		$settings = $this->Session->read('Settings');

		if (empty($settings)) {
			$id = $this->othAuth->user('id');
			if ($id && $this->name != 'Pages') {

	    		// load settings from the database into session
				App::import('Model', 'User');
				$user = new User();

	    		$user->Setting->unbindAll();
		    	$this->settings = $user->Setting->findAllByUserId($id, array('name', 'value'));

		    	$settings = Set::combine($this->settings, '{n}.Setting.name', '{n}.Setting.value');
	    		foreach ($settings as $name => $value) {
					$this->Session->write('Settings.'.$name, $value);
		    	}
			}
		}
    }

	function afterFilter()
	{
		// if debugging is enabled, show some memory info
		if (Configure::read('debug')) {
			if (function_exists('memory_get_usage')) echo 'Memory: '. round(memory_get_usage()/1048576, 1) . ' MB';
			if (function_exists('memory_get_peak_usage')) echo ' (Peak: '. round(memory_get_peak_usage()/1048576, 1) . ' MB)';
		}
	}

	// function to load a component by name on demand
    function enableComponent($component, $parent=true, $params=null)
    {
       $name = Inflector::camelize($component);
       $found = App::Import('Component', $name, $parent);
       if($found)
       {
			$cn = $name . 'Component';
			$comp = new $cn($params);
			return $comp;
       }
    }

	// function to list components -- $type is, eg. convert, lookup or parse
    function getComponents($type = '') {
		// set the folder
        $folder = new Folder(COMPONENTS . $type);
        $files = $folder->findRecursive('.*php');

		foreach ($files as $filename) {
			$components[] = Inflector::camelize(basename($filename, '.php'));
		}

        return $components;
    }

}

?>
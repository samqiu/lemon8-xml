<?php

/**
 * app_model.php
 *
 * Copyright (c) 2008-2009 MJ Suhonos
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * This file is application-wide model-related methods here.
 * 
*/

class AppModel extends Model{
	
	// unbind all associations except some
     function unbindAll($params = array())
    {
        foreach($this->__associations as $ass)
        {
            if(!empty($this->{$ass}))
            {
                 $this->__backAssociation[$ass] = $this->{$ass};
                if(isset($params[$ass]))
                {
                    foreach($this->{$ass} as $model => $detail)
                    {
                        if(!in_array($model,$params[$ass]))
                        {
                             $this->__backAssociation = array_merge($this->__backAssociation, $this->{$ass});
                            unset($this->{$ass}[$model]);
                        }
                    }
                }else
                {
                    $this->__backAssociation = array_merge($this->__backAssociation, $this->{$ass});
                    $this->{$ass} = array();
                }
                
            }
        }
        return true;
    }
} 

?>
<?php

/**
 * Helper for outputing a country select list.
 *
 * Allows you to include a select list of all countries using 1 line of code.
 *
 * Author: Tane Piper (digitalspaghetti@gmail.com)
 * URL: http://digitalspaghetti.me.uk
 *
 * PHP versions 4 and 5
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 */

class CountryListHelper extends FormHelper
{

	var $helpers = array('Form');

	function select($fieldname, $label, $default="  ", $attributes=null)
	{
		$countries = array_combine(array_keys(country_list()), array_keys(country_list()));
		natsort($countries);

		$list = $this->Form->label($label);
		$list .= $this->Form->select($fieldname, $countries, $default, $attributes);
		return $this->output($list);
	}

}
?>
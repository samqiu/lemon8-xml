<?php
	/**
	 * home.ctp
	 *
	 * Copyright (c) 2008-2009 MJ Suhonos
	 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
	 *
	**/
?>

<?php switch ($session->read('Config.language')) {
			case 'fr': ?>

	<table width="100%" style="background-color: white;">
		<tr>
			<td colspan="2">
	<p>Lemon8-XML est un application sur le Web conçu pour faciliter l'accès non technique des éditeurs et
	auteurs de convertir articles savants typique de traitement de texte, tels que l'édition des formats
	MS-Word .DOC et OpenOffice <a href="http://en.wikipedia.org/wiki/OpenDocument">.ODT</a>, en publiant
	des formats tels que la mise en page L'ouvert, standard de l'industrie format
	<a href="http://dtd.nlm.nih.gov/publishing/2.3/index.html">NLM Journal Publishing XML</a>.</p>

	<p>Pour utiliser Lemon8-XML, vous n'avez pas besoin de comprendre le XML, il vous suffit d'avoir un
	peu de temps et d'une compréhension générale de la façon dont des articles sont structurés. En général,
	cela signifie un document avec:
	</p>
	<ol style="list-style-type: lower-alpha";>
		<li>quelques informations sur l'article et les auteurs au sommet, et généralement un résumé</li>
		<li>plusieurs sections, souvent intitulée "présentation", "méthodes", "résultats", etc.</li>
		<li>facultatif chiffres ou des tableaux, que ce soit dans le texte ou sous forme d'annexes</li>
		<li>une liste de références ou de citations dans un format standard (par exemple, MLA, APA, etc.)</li>
	</ol>
	<hr style="margin-bottom: 1em;"/>
			</td>
		</tr>
		<tr>
			<td>
	<a href="http://pkp.sfu.ca/"><?php echo $html->image('chameleon_logo.gif', array('style' => 'margin: 1em; border: none; width: 140px;')); ?></a>
	<br/>
	<a href="http://www.synergiescanada.org/index_en.html"><?php echo $html->image('synergies.gif', array('style' => 'margin: 1em; border: none; width: 140px;')); ?></a>
			</td>
			<td valign="top">
	<p>Lemon8-XML est en cours d'élaboration par le <a href="http://pkp.sfu.ca/">Public Knowledge Project</a>,
	dans le cadre de ses efforts pour assurer que les dernières technologies basées sur le Web sont axés sur
	l'amélioration de la qualité non seulement globale mais le public et la portée de la communication savante.
	Lemon8-XML est un système autonome, qui servira à la publication des processus plus généralement, ainsi que
	de compléter les flux de travail de la <a href="http://pkp.sfu.ca/ojs">Open Journal Systems</a>.</p>

	<p>Pour plus d'informations, consultez la <a href="http://pkp.sfu.ca/lemon8_faq">FAQ</a> ou participer à la
	<a href="http://pkp.sfu.ca/support/forum/viewforum.php?f=15">forum de discussion Lemon8-XML</a>.</p>

	<p>Lemon8-XML a été inspiré par les premiers travaux entrepris pour le
	<a href="http://www.jmir.org/">Journal of Medical Internet Research</a>, et est actuellement parrainé par la
	<a href="http://www.innovation.ca/index.cfm">Fondation canadienne pour l'innovation</a>, dans le cadre de
	<a href="http://www.synergiescanada.org/">l'Initiative de Synergies</a>.</p>
			</td>
		</tr>
	</table>

	<?php break; ?>

<?php case 'es': ?>

	<table width="100%" style="background-color: white;">
		<tr>
			<td colspan="2">
	<p>Lemon8-XML es un basado en la web diseñado para que sea más fácil para los no técnicos editores y autores
	para convertir documentos de carácter académico o de procesador de textos típicos de edición de formatos como
	MS Word. DOC y OpenOffice. ODT, en la publicación de la disposición formatos tales como Al aire libre, los
	estándares de la industria formato
	<a href="http://dtd.nlm.nih.gov/publishing/2.3/index.html">NLM Journal Publishing XML</a>.</p>

	<p>Para utilizar Lemon8-XML, no es necesario que entiendan XML, todo lo que necesita es un poco de tiempo
	y un conocimiento general de cómo están estructurados los artículos académicos. En general, este es un documento con:
	</p>
	<ol style="list-style-type: lower-alpha";>
		<li>alguna información sobre el artículo y los autores, en la parte superior, y por lo general un resumen</li>
		<li>varias secciones, a menudo llamado "introducción", "métodos", "resultados", etc.</li>
		<li>opcional figuras o cuadros, ya sea en el texto o como anexos</li>
		<li>una lista de referencias o citas en un formato normalizado (por ejemplo, MLA, APA, etc.)</li>
	</ol>
	<hr style="margin-bottom: 1em;"/>
			</td>
		</tr>
		<tr>
			<td>
	<a href="http://pkp.sfu.ca/"><?php echo $html->image('chameleon_logo.gif', array('style' => 'margin: 1em; border: none; width: 140px;')); ?></a>
	<br/>
	<a href="http://www.synergiescanada.org/index_en.html"><?php echo $html->image('synergies.gif', array('style' => 'margin: 1em; border: none; width: 140px;')); ?></a>
			</td>
			<td valign="top">
	<p>Lemon8-XML está siendo desarrollado por el <a href="http://pkp.sfu.ca/">Public Knowledge Project</a>,
	como parte de sus esfuerzos para asegurar que las últimas tecnologías basadas en la Web están dirigidos no
	sólo hacia la mejora de la calidad, pero el público mundial y alcance de la comunicación académica. Lemon8-XML
	es un sistema autónomo que servirá a la publicación de los procesos en general, así como complementar el flujo
	de trabajo de <a href="http://pkp.sfu.ca/ojs">Open Journal Systems</a>.</p>

	<p>Para obtener más información, eche un vistazo a la <a href="http://pkp.sfu.ca/lemon8_faq">FAQ</a> o participar en el
	<a href="http://pkp.sfu.ca/support/forum/viewforum.php?f=15">foro de discusión Lemon8-XML</a>.</p>

	<p>Lemon8-XML se inspiró en principios de la labor realizada por la revista
	<a href="http://www.jmir.org/">Journal of Medical Internet Research</a>, y actualmente es patrocinado por la
	<a href="http://www.innovation.ca/index.cfm">Fundación para la Innovación Canadá</a>, como parte de la
	<a href="http://www.synergiescanada.org/index_en.html">Iniciativa de Sinergias</a>.</p>
			</td>
		</tr>
	</table>

	<?php break; ?>

<?php default: ?>

	<table width="100%" style="background-color: white;">
		<tr>
			<td colspan="2">
	<p>Lemon8-XML is a web-based application designed to make it easier for non-technical editors and authors
	to convert scholarly papers from typical word-processor editing formats such as MS-Word .DOC and OpenOffice
	<a href="http://en.wikipedia.org/wiki/OpenDocument">.ODT</a>, into structured publishing layout formats
	such as the open, industry-standard
	<a href="http://dtd.nlm.nih.gov/publishing/2.3/index.html">NLM Journal Publishing XML</a> format.</p>

	<p>To use Lemon8-XML, you don't need to understand XML, all you need is a little time and
	a general understanding of how scholarly articles are structured.  In general, this means a document with:
	</p>
	<ol style="list-style-type: lower-alpha";>
		<li>some information about the article and authors at the top, and usually an abstract</li>
		<li>several sections, often titled "introduction", "methods", "results", etc.</li>
		<li>optional figures or tables, either in-text or as appendices</li>
		<li>a list of references or citations in a standardized format (eg. MLA, APA, etc.)</li>
	</ol>
	<hr style="margin-bottom: 1em;"/>
			</td>
		</tr>
		<tr>
			<td>
	<a href="http://pkp.sfu.ca/"><?php echo $html->image('chameleon_logo.gif', array('style' => 'margin: 1em; border: none; width: 140px;')); ?></a>
	<br/>
	<a href="http://www.synergiescanada.org/index_en.html"><?php echo $html->image('synergies.gif', array('style' => 'margin: 1em; border: none; width: 140px;')); ?></a>
			</td>
			<td valign="top">
	<p>Lemon8-XML is being developed by the <a href="http://pkp.sfu.ca/">Public Knowledge Project</a>,
	as part of its efforts to ensure that the latest web-based technologies are directed toward improving
	not only the quality but the global and public reach of scholarly communication. Lemon8-XML is a stand-alone
	system that will serve publishing processes more generally, as well as complement the workflow of
	<a href="http://pkp.sfu.ca/ojs">Open Journal Systems</a>.</p>

	<p>For more information, have a look at the <a href="http://pkp.sfu.ca/lemon8_faq">FAQ</a> or participate in the
	<a href="http://pkp.sfu.ca/support/forum/viewforum.php?f=15">Lemon8-XML discussion forum</a>.</p>

	<p>Lemon8-XML was inspired by early work undertaken for the
	<a href="http://www.jmir.org/">Journal of Medical Internet Research</a>, and is currently sponsored by the
	<a href="http://www.innovation.ca/index.cfm">Canada Foundation for Innovation</a>, as part of the
	<a href="http://www.synergiescanada.org/index_en.html">Synergies Initiative</a>.</p>
			</td>
		</tr>
	</table>
<?php } //switch ?>

<hr style="margin-top: 0px; "/>

<p style="margin-left: 1em;">Lemon8-XML has been developed using 100% free and <a href="http://en.wikipedia.org/wiki/Open-source_software">Open Source software</a> and technology.</p>

<table border="0" width="90%" style="background: #FFF;" align="center">
	<tr>
		<td style="height: 3em;">
			<a href="http://cakephp.org/"><?php echo $html->image('tech/cakephp.gif', array('style' => 'vertical-align: -50%; border: none; height: 50px;')); ?></a>
			&nbsp;
			<a href="http://www.gophp5.org/"><?php echo $html->image('tech/php5-logo.png', array('style' => 'border: none; height: 30px;')); ?></a>
		</td>
		<td>Lemon8-XML is built on the flexible <a href="http://cakephp.org/">CakePHP</a> framework using <a href="http://www.gophp5.org/">PHP5</a>.</td>
	</tr>
	<tr>
		<td style="height: 3em;">
			<a href="http://holloway.co.nz/docvert/"><?php echo $html->image('tech/docvert.gif', array('style' => 'vertical-align: 25%; border: none; height: 20px;')); ?></a>
			&nbsp;
			<a href="http://docs.google.com/"><?php echo $html->image('tech/google_docs.gif', array('style' => 'border: none; height: 35px;')); ?></a>
		</td>
		<td>Document conversion is done using <a href="http://holloway.co.nz/docvert/">Docvert</a> and <a href="http://docs.google.com">Google Docs</a>.</td>
	</tr>
	<tr>
		<td style="height: 3em;"><a href="http://www.openoffice.org/"><?php echo $html->image('tech/openoffice-logo.png', array('style' => 'border: none; height: 40px;')); ?></a></td>
		<td>Actual document format conversion is done using <a href="http://www.openoffice.org/">OpenOffice.org</a>.</td>
	</tr>
	<tr>
		<td style="height: 3em;"><a href="http://paracite.eprints.org/"><?php echo $html->image('tech/paracite.png', array('style' => 'border: none; height: 30px;')); ?></a></td>
		<td>The citation parser incorporates the <a href="http://paracite.eprints.org/">ParaCite</a>, <a href="http://freecite.library.brown.edu/">FreeCite</a>, and <a href="http://wing.comp.nus.edu.sg/parsCit/">ParsCit</a> parsing services.</td>
	</tr>
	<tr>
		<td style="height: 3em;"><a href="http://xmlgraphics.apache.org/fop/"><?php echo $html->image('tech/fop_logo.jpg', array('style' => 'border: none; height: 40px;')); ?></a></td>
		<td>PDF preview rendering is done with XSL-FO via <a href="http://paracite.eprints.org/">Apache FOP</a>.</td>
	</tr>
	<tr>
		<td style="height: 3em;">
			<a href="http://isbndb.com/"><?php echo $html->image('tech/isbndb.gif', array('style' => 'border: none; height: 30px;')); ?></a>
			&nbsp;
			<a href="http://www.worldcat.org/"><?php echo $html->image('tech/worldcat.gif', array('style' => 'border: none; height: 30px;')); ?></a>
		</td>
		<td>Book citation correction uses data from the <a href="http://isbndb.com/">ISBNdb</a> and <a href="http://www.worldcat.org/">WorldCat</a> databases.</td>
	</tr>
	<tr>
		<td style="height: 3em;">
			<a href="http://www.ncbi.nlm.nih.gov/sites/entrez"><?php echo $html->image('tech/pubmed.gif', array('style' => 'border: none; height: 30px;')); ?></a>
			&nbsp;
			<a href="http://www.crossref.org/"><?php echo $html->image('tech/crossref.gif', array('style' => 'border: none; height: 30px;')); ?></a>
		</td>
		<td>Journal citation correction uses data from in the <a href="http://www.ncbi.nlm.nih.gov/sites/entrez">PubMed</a> and <a href="http://www.crossref.org/">CrossRef</a> databases.</td>
	</tr>
</table>

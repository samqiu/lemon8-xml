<?php
	/**
	 * index.ctp
	 *
	 * Copyright (c) 2008-2009 MJ Suhonos
	 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
	 *
	**/

	// sort the components alphabetically
	natsort($convertComponents);
	natsort($lookupComponents);
	natsort($parseComponents);
?>

<script type="text/javascript">
function toggleSection(class) {
	$$('.'+class).each( function(el){
		if ( el.getStyle('display') != 'none' ) {
			el.hide();
		} else {
			el.show();
		}
	});
}
</script>

<?php if ( $othAuth->user('id') ): ?>

			<table width="100%" style="background:#fff;">
			<tr>

			<td valign="top" width="50%" style="padding: 0 2% 0 2%;">

			<table width="100%" style="padding: 2%; border: 1px solid #ddd;">
				<tr>
				<td colspan="2"><h3 style="margin-top: 0;"><?php echo __('Your Documents'); ?></h3></td>
				</tr>

			<?php if (count($documents) == 0): ?>
				<tr>
					<td>
					<span style="font-size: 0.85em;"><?php echo __('(You have not uploaded any documents)'); ?></span>
					<p><?php echo __('Why not try <a href="sample-document.odt">this sample document (308KB)</a>?'); ?></p>
					</td>
				</tr>
			<?php else: ?>
				<?php $i=1; foreach ($documents as $document): ?>
					<tr>
						<td width="20"><?php echo $i++; ?>. </td>
						<td>
			             	<?php echo $html->link($html->image('delete.png', array('style' => 'vertical-align: middle; border: 1px solid #ccc; text-align: right;')), '/documents/delete/'.$document['Document']['id'], array(), 'Are you sure you wish to delete this document?\n\nThis action cannot be undone.', false); ?>
							<?php echo $html->link( $document['Document']['filename'], '/documents/preview_save/'.$document['Document']['id'])?>
							<?php $id = array_search('parse_score', Set::extract($document['Metadata'], '{n}.element'));
								echo '('.round($document['Metadata'][$id]['value']).'%)'; ?>
							<span style="font-size: 0.85em;">
							<?php echo date('Y-m-d', strtotime($document['Document']['created'])); ?>;
							<?php echo round($document['Document']['filesize'] / 1024); ?>KB
							</span>
						</td>
					</tr>
				<?php endforeach; ?>
			<?php endif; ?>

				<tr>
					<td colspan="2">
				<fieldset id="upload" style="width: 90%">
					<legend><?php echo __('Upload a new document'); ?></legend>
					<div style="font-size: 0.75em; margin-bottom: 1em;">

					<b><?php
					if ($session->read('Settings.convert.component')) {
						echo __('Document can be of any word-processor type:');
					} else {
						echo __('Document must be OpenDocument (ODT) format:');
					}
					?></b>

					<?php
					if ($session->read('Settings.convert.component')) {
						echo __('(eg. Word .DOC, Word .XML, Rich Text .RTF, Wordperfect .WPD, DocBook, HTML, etc.)');
					} else {
						echo __('Automatic document conversion is not enabled.');
					}
					?>
					</div>
					<?php
					    echo $form->create('Documents', array('action' => 'upload', 'type' => 'file'));
					    echo $form->file('File', array('style' => 'width: 250px; margin-right: 1em;', 'class' => 'text'));
					    echo $form->submit(__('Upload', true), array('class' => 'button'));
					    echo $form->end();
					?>
				</fieldset>
					</td>
				</tr>

			</table>

			<br/>

			<?php echo $form->create('User', array('action' => 'save', 'name' => 'profile'));?>
			<table width="100%" style="padding: 2%; border: 1px solid #ddd;">
				<tr>
				<td colspan="2"><h3 style="margin-top: 0;"><?php echo __('Your Profile'); ?></h3></td>
				</tr>

				<tr>
				<td class="label"><label><?php echo __('Username:'); ?></label></td>
				<td class="field"><?php echo $user['User']['username']; ?></td>
				</tr>

				<tr>
				<td class="label"><label><?php echo __('Name:'); ?></label></td>
				<td class="field">
				<?php echo $form->input('User.name', array('type' => 'text', 'class' => 'text', 'label' => '', 'value' => $user['User']['name'])); ?>
				</td>
				</tr>

				<tr>
				<td class="label"><label><?php echo __('Email address:'); ?></label></td>
				<td class="field">
				<?php echo $form->input('User.email', array('type' => 'text', 'class' => 'text', 'label' => '', 'value' => $user['User']['email'])); ?>
				</td>
				</tr>

				<tr>
				<td class="label"><label><?php echo __('New password:'); ?></label></td>
				<td class="field">
				<?php echo $form->input('User.newpass', array('type' => 'password', 'class' => 'text', 'label' => '', 'value' => '')); ?>
				</td>
				</tr>

				<tr>
				<td class="label"><label><?php echo __('Repeat password:'); ?></label></td>
				<td class="field">
				<?php echo $form->input('User.newpass2', array('type' => 'password', 'class' => 'text', 'label' => '', 'value' => '')); ?>
				</td>
				</tr>

				<tr>
				<td class="label"><br/><label><?php echo __('Current password:'); ?></label></td>
				<td class="field"><br/>
				<?php echo $form->input('User.password', array('type' => 'password', 'class' => 'text', 'label' => '', 'value' => '')); ?>
				</td>
				</tr>
				<tr><td colspan="2"><span style="font-size: 0.85em; padding-left: 1.5em;"><?php echo __('Enter your current password to change these values.'); ?></span></td></tr>

			</table>

			<br/>
			<input name="submit" value="Save/Update Profile" type="submit" class="button">
			<?php echo $form->end(); ?>

			</td>

			<td valign="top" width="50%" style="padding: 0 2% 0 2%;">

			<?php echo $form->create('User', array('action' => 'save', 'name' => 'settings'));?>
			<table width="100%" style="padding: 2%; border: 1px solid #ddd;">

				<!-- CONVERT COMPONENTS-->
				<tr>
				<td colspan="2">
				<?php echo $html->link($html->image('plus.jpg', array('style' => 'vertical-align: top; border: none; margin-bottom: 0.5em;')), 'javascript://', array('onclick' => 'toggleSection(\'convert\');'), false, false); ?>
				<h3 style="margin-left: 0.5em; display: inline;"><?php echo __('Document Conversion Settings'); ?></h3>
				</td>
				</tr>

				<tr class="convert" style="display: none;">
					<td><h4 style="margin: 0.25em 0em 0.25em 0.5em;">Do Not Convert</h4></td>
					<td><input type="radio" name="data[Setting][convert][component]" value="" <?php if ('' == $session->read('Settings.convert.component')) echo 'checked'; ?> /></td>
				</tr>

				<?php foreach ($convertComponents as $component) {
					$com = AppController::enableComponent($component);
					?>
					<tr class="convert" style="display: none;">
						<td><h4 style="margin: 0.25em 0em 0.25em 0.5em;"><?php echo $component; ?>
						<?php
						if (file_exists(WWW_ROOT . 'img' . DS . 'tech' . DS . Inflector::underscore($component) . '.gif')) {
							echo $html->image('tech/'.Inflector::underscore($component).'.gif', array('style' => 'border: none; height: 20px; vertical-align: top; margin-left: 0.5em;'));
						} ?>
						</h4>
						</td>
						<td valign="top"><input type="radio" name="data[Setting][convert][component]" value="<?php echo Inflector::underscore($component); ?>" <?php if (Inflector::underscore($component) == $session->read('Settings.convert.component')) echo 'checked'; ?> /></td>
					</tr>
					<?php
					// suppress warnings if no settings are required
					if (!isset($com->settings)) continue;
					foreach ($com->settings as $field => $description) {
					?>
					<tr class="convert" style="display: none;">
						<td class="label" style="padding-left: 1.5em; font-size: 0.85em;"><label><?php echo $description; ?></label></td>
						<td class="field" width="50%"><?php $name = 'convert.'.Inflector::underscore($component).'.'.$field; ?>
						<?php echo $form->input('Setting.'.$name, array('type' => 'text', 'class' => 'text', 'value' => $session->read('Settings.'.$name), 'label' => '', 'style' =>'width: 95%;')); ?>
						</td>
					</tr>
				<?php }
					}
				?>

			</table>

			<br/>

			<table width="100%" style="padding: 2%; border: 1px solid #ddd;">

				<tr>
				<td colspan="2">
				<?php echo $html->link($html->image('plus.jpg', array('style' => 'vertical-align: top; border: none; margin-bottom: 0.5em;')), 'javascript://', array('onclick' => 'toggleSection(\'document\');'), false, false); ?>
				<h3 style="margin-left: 0.5em; display: inline;"><?php echo __('Document Parser Settings'); ?></h3>
				</td>
				</tr>

				<tr class="document" style="display: none;">
				<td class="label"><label><?php echo __('Document margin:'); ?></label></td>
				<td class="field">
				<input class="text" name="data[Setting][doc_parse_margin]" value="<?php echo $session->read('Settings.doc_parse_margin'); ?>" type="text" style="width: 50%;">
				</td>
				</tr>
				<tr class="document" style="display: none;"><td colspan="2"><span style="font-size: 0.85em; padding-left: 1.5em;"><?php echo __('Threshold to consider document parses successful.'); ?></span></td></tr>

				<tr class="document" style="display: none;">
				<td class="label"><label><?php echo __('Heading margin:'); ?></label></td>
				<td class="field">
				<input class="text" name="data[Setting][heading_margin]" value="<?php echo $session->read('Settings.heading_margin'); ?>" type="text" style="width: 50%;">
				</td>
				</tr>
				<tr class="document" style="display: none;"><td colspan="2"><span style="font-size: 0.85em; padding-left: 1.5em;"><?php echo __('Maximum length of average heading line, in %.'); ?></span></td></tr>

				<tr class="document" style="display: none;">
				<td class="label"><label><?php echo __('Minimum words:'); ?></label></td>
				<td class="field">
				<input class="text" name="data[Setting][min_words]" value="<?php echo $session->read('Settings.min_words'); ?>" type="text" style="width: 50%;">
				</td>
				</tr>
				<tr class="document" style="display: none;"><td colspan="2"><span style="font-size: 0.85em; padding-left: 1.5em;"><?php echo __('Minimum words per paragraph for heading calculation.'); ?></span></td></tr>

				<tr class="document" style="display: none;">
				<td class="label"><label><?php echo __('Minimum characters:'); ?></label></td>
				<td class="field">
				<input class="text" name="data[Setting][min_chars]" value="<?php echo $session->read('Settings.min_chars'); ?>" type="text" style="width: 50%;">
				</td>
				</tr>
				<tr class="document" style="display: none;"><td colspan="2"><span style="font-size: 0.85em; padding-left: 1.5em;"><?php echo __('Minimum characters per paragraph for heading calculation.'); ?></span></td></tr>

				<tr class="document" style="display: none;">
				<td class="label"><label><?php echo __('Default font size:'); ?></label></td>
				<td class="field">
				<input class="text" name="data[Setting][default_size]" value="<?php echo $session->read('Settings.default_size'); ?>" type="text" style="width: 50%;">
				</td>
				</tr>
				<tr class="document" style="display: none;"><td colspan="2"><span style="font-size: 0.85em; padding-left: 1.5em;"><?php echo __('Default font size to determine headings, in points.'); ?></span></td></tr>

			</table>

			<br/>

			<table width="100%" style="padding: 2%; border: 1px solid #ddd;">

				<!-- PARSE COMPONENTS-->
				<tr>
				<td colspan="2">
				<?php echo $html->link($html->image('plus.jpg', array('style' => 'vertical-align: top; border: none; margin-bottom: 0.5em;')), 'javascript://', array('onclick' => 'toggleSection(\'parser\');'), false, false); ?>
				<h3 style="margin-left: 0.5em; display: inline;"><?php echo __('Citation Parser Settings'); ?></h3>
				</td>
				</tr>

				<tr class="parser" style="display: none;">
				<td class="label"><label><?php echo __('Parse margin:'); ?></label></td>
				<td class="field">
				<input class="text" name="data[Setting][parse_margin]" value="<?php echo $session->read('Settings.parse_margin'); ?>" type="text" style="width: 50%;">
				</td>
				</tr>
				<tr class="parser" style="display: none;"><td colspan="2"><span style="font-size: 0.85em; padding-left: 1.5em;"><?php echo __('Threshold to consider citation parses successful.'); ?></span></td></tr>

				<?php foreach ($parseComponents as $component) {
					$com = AppController::enableComponent($component);
					?>
					<tr class="parser" style="display: none;">
						<td><h4 style="margin: 0.25em 0em 0.25em 0.5em;"><?php echo $component; ?></h4></td>
						<td><input type="checkbox" name="data[Setting][parse][component][<?php echo Inflector::underscore($component); ?>]" <?php if ($session->read('Settings.parse.component.'.Inflector::underscore($component))) echo 'checked'; ?> /></td>
					</tr>

					<?php
					// suppress warnings if no settings are required
					if (!isset($com->settings)) continue;
					foreach ($com->settings as $field => $description) {
					?>
					<tr class="parser" style="display: none;">
						<td class="label" style="padding-left: 1.5em; font-size: 0.85em;"><label><?php echo $description; ?></label></td>
						<td class="field" width="50%"><?php $name = 'parse.'.Inflector::underscore($component).'.'.$field; ?>
						<?php echo $form->input('Setting.'.$name, array('type' => 'text', 'class' => 'text', 'value' => $session->read('Settings.'.$name), 'label' => '', 'style' =>'width: 95%;')); ?>
						</td>
					</tr>
				<?php }
					}
				?>

			</table>

			<br/>

			<table width="100%" style="padding: 2%; border: 1px solid #ddd;">

				<!-- LOOKUP COMPONENTS-->
				<tr>
				<td colspan="2">
				<?php echo $html->link($html->image('plus.jpg', array('style' => 'vertical-align: top; border: none; margin-bottom: 0.5em;')), 'javascript://', array('onclick' => 'toggleSection(\'lookup\');'), false, false); ?>
				<h3 style="margin-left: 0.5em; display: inline;"><?php echo __('Citation Lookup Settings'); ?></h3>
				</td>
				</tr>

				<tr class="lookup" style="display: none;">
				<td class="label"><label><?php echo __('Lookup margin:'); ?></label></td>
				<td class="field">
				<input class="text" name="data[Setting][lookup_margin]" value="<?php echo $session->read('Settings.lookup_margin'); ?>" type="text" style="width: 50%;"></td>
				</tr>
				<tr class="lookup" style="display: none;"><td colspan="2"><span style="font-size: 0.85em; padding-left: 1.5em;"><?php echo __('Threshold to consider citation lookups successful.'); ?></span></td></tr>

				<?php foreach ($lookupComponents as $component) {
					$com = AppController::enableComponent($component);
					?>
					<tr class="lookup" style="display: none;">
						<td><h4 style="margin: 0.25em 0em 0.25em 0.5em;"><?php echo $component; ?>
						<?php
						if (file_exists(WWW_ROOT . 'img' . DS . 'tech' . DS . Inflector::underscore($component) . '.gif')) {
							echo $html->image('tech/'.Inflector::underscore($component).'.gif', array('style' => 'border: none; height: 20px; vertical-align: top; margin-left: 0.5em;'));
						} ?>
						</h4>
						</td>
						<td><input type="checkbox" name="data[Setting][lookup][component][<?php echo Inflector::underscore($component); ?>]" <?php if ($session->read('Settings.lookup.component.'.Inflector::underscore($component))) echo 'checked'; ?> /></td>
					</tr>

					<?php
					// suppress warnings if no settings are required
					if (!isset($com->settings)) continue;
					foreach ($com->settings as $field => $description) {
					?>
					<tr class="lookup" style="display: none;">
						<td class="label" style="padding-left: 1.5em; font-size: 0.85em;"><label><?php echo $description; ?></label></td>
						<td class="field" width="50%"><?php $name = 'lookup.'.Inflector::underscore($component).'.'.$field; ?>
						<?php echo $form->input('Setting.'.$name, array('type' => 'text', 'class' => 'text', 'value' => $session->read('Settings.'.$name), 'label' => '', 'style' =>'width: 95%;')); ?>
						</td>
					</tr>
				<?php }
					}
				?>

			</table>

				<br/>

			<input name="submit" value="Save/Update Settings" type="submit" class="button">
			<?php echo $form->end(); ?>

			</td>

			</tr>
			</table>

<?php else: ?>

			<h3 id="about"><?php echo __('Please Log In'); ?> <?php if (isset($url)) echo __('To Access That Page'); ?></h3>

			<?php if (isset($auth_msg)): ?>
				<div style="color: red;"><?php echo $auth_msg; ?></div>
			<?php endif; ?>

			<br/>

			<?php echo $form->create('User', array('action' => 'login'));?>
			<?php echo $form->input('User.username', array('id' => 'user_username', 'size' => '15', 'style' => 'margin-left: 2em;')) ?>
			<?php echo $form->input('User.password', array('id' => 'user_passwd', 'size' => '15', 'type' => 'password', 'style' => 'margin-left: 2.4em;')) ?>
			<?php echo $form->submit('Login', array('class' => 'submitButton', 'style' => 'margin-top: 1em;')); ?>
			<?php echo $form->hidden('User.cookie', array('value' => 'true')); ?>
			<?php echo $form->end(); ?>

<?php endif; ?>

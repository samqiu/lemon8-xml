<!-- REUSABLE CITATION SCORE BLOCK -->
<?php

	if (	$citation['Citation']['lookup_score'] >= $session->read('Settings.lookup_margin')
		&&	strtotime($citation['Citation']['validated']) >= strtotime($citation['Citation']['modified'])
		// TODO: fix date checking 
//		&&	strtotime($citation['Citation']['modified']) > strtotime($citation['Citation']['created'])
//		&&	$citation['Citation']['parse_score'] >= $session->read('Settings.parse_margin')
		) {
		$image = 'check.gif';
		$bg = 'background-color: #E6FFE6;';

	} elseif (	$citation['Citation']['parse_score'] >= $session->read('Settings.parse_margin')
		// TODO: fix date checking 
//		&&	$citation['Citation']['lookup_score'] < $lookup_margin
//		&&	 strtotime($citation['Citation']['modified']) > strtotime($citation['Citation']['created'])
		) {
		$image = 'alert.gif';
		$bg = 'background-color: #FFFAE6;';

	} else {
		$image = 'x.gif';
		$bg = 'background-color: #FFE6E6;';
	}
?>
<div class="citation" style="font-size: 13px; padding: 0.3em 0em 0.3em 0.4em; margin-top: 1px; margin-bottom: 0px; <?php echo $bg; ?>">
	<?php echo $html->image($image, array('style' => 'height: 15px; margin-right: 5px; vertical-align: text-top;')); ?>
	<?php
		if ($citation['Citation']['lookup_score'] >= $session->read('Settings.lookup_margin')) {
			echo $citation['Citation']['lookup_score'].'%';
		} else {
			echo $citation['Citation']['parse_score'].'%';
		}
	?>
</div>
<?php
	/**
	 * default.ctp
	 *
	 * Copyright (c) 2008-2009 MJ Suhonos
	 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
	 *
	**/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Lemon8 > <?php echo $this->name; ?> > <?php echo $title_for_layout; ?></title>
	<link rel="icon" href="<?php echo $this->webroot . 'favicon.ico';?>" type="image/x-icon" />
	<link rel="shortcut icon" href="<?php echo $this->webroot . 'favicon.ico';?>" type="image/x-icon" />
	<?php echo $html->charset('utf-8'); ?>
	<?php echo $html->css('style'); ?>

	<!-- This is for sections_edit -->
	<?php echo $html->css('folder-tree-static'); ?>

	<!-- This is for citations_edit -->
	<?php echo $html->css('citation-hilight'); ?>

	<!-- This is so we get fancy things like AJAX and effects-->
	<?php
		echo $javascript->link('prototype');
		echo $javascript->link('scriptaculous');
		echo $javascript->link('functions');
	?>
</head>
<body>

	<!-- top navigation -->

	<div id="top">
		<div id="nav" class="frame">

			<div style="margin-right: 100px;">
				<h1 style="display: inline;"><?php echo $html->link('Lemon8-XML', '/'); ?></h1>

				<span class="loginform" style="position: absolute; width: 635px; text-align: right;">
					<?php if ($othAuth->sessionValid()): ?>
						<span style="vertical-align: bottom;">
						<?php echo __('Logged in as:'); ?>
						<?php echo $html->link($othAuth->user('name'), '/users', array('style' => 'font-size: 1.2em; font-style: normal; letter-spacing: 0.05em; border: 0;')); ?>
						</span>
						<?php echo $form->create('User', array('action' => 'logout', 'style' => 'display: inline;'));?>
						<?php echo $form->submit(__('Logout', true), array('class' => 'button')); ?>
						<?php echo $form->end(); ?>
					<?php else: ?>
						<?php echo $form->create('User', array('action' => 'login', 'style' => 'display: inline;'));?>
						<?php echo $form->input('User.username', array('label' => __('Username', true), 'size' => '10', 'style' => 'border: 1px solid #ddd; margin-left: 0.5em;')) ?>
						<?php echo $form->input('User.password', array('label' => __('Password', true), true, 'size' => '10', 'type'=> 'password', 'style' => 'border: 1px solid #ddd; margin-left: 0.5em;')) ?>
						<?php echo $form->submit(__('Login', true), array('class' => 'button')); ?>
						<?php echo $form->hidden('User.cookie', array('value' => 'true')); ?>
						<?php echo $form->end(); ?>
					<?php endif; ?>
				</span>

			</div>

			<div id="navlinks" style="margin-top: 0.8em; margin-right: 100px;">
				<span id="langlinks" >
					<?php echo ($session->read('Config.language') == 'en' || $session->read('Config.language') == '') ? '<i>english</i>' : $html->link('english', '/lang/en'); ?>
					<?php echo ($session->read('Config.language') == 'es') ? '<i>español</i>' : $html->link('español', '/lang/es'); ?>
					<?php echo ($session->read('Config.language') == 'fr') ? '<i>français</i>' : $html->link('français', '/lang/fr'); ?>
				</span>

			</div>

		</div>

		<!-- document navigation -->

		<div id="content">
			<div id="items" class="frame">

				<?php if (isset($document)){ ?>

				<!-- link to document metadata edit page -->
				<span class="frame topnav <?php if (stripos($title_for_layout, 'metadata') !== false) echo 'selectednav'; ?>" id="navlinks">
				<?php
					echo $html->link('1. '.__('Metadata', true), '/documents/metadata_edit/'.$document['Document']['id']);
					echo $html->image($valid_metadata ? 'check.gif' : 'x.gif', array('class' => 'edit-link'));
				?>
				</span>

				<!-- link to section ordering/edit page -->
				<span class="frame topnav <?php if (stripos($title_for_layout, 'section') !== false) echo 'selectednav'; ?>" id="navlinks">
				<?php
					echo $html->link('2. '.__('Sections', true), '/documents/sections_edit/'.$document['Document']['id']);
					echo $html->image($valid_sections ? 'check.gif' : 'x.gif', array('class' => 'edit-link'));
				?>
				</span>

				<!-- link to citation list page -->
				<span class="frame topnav <?php if (stripos($title_for_layout, 'citation') !== false) echo 'selectednav'; ?>" id="navlinks">
				<?php
					echo $html->link('3. '.__('Citations', true), '/documents/citations_edit/'.$document['Document']['id']);
					echo $html->image($valid_citations ? 'check.gif' : 'x.gif', array('class' => 'edit-link'));
				?>
				</span>
				<?php echo $html->image('right_arrow.png', array('style' => 'vertical-align: bottom;')); ?>

				<!-- link to preview / export page -->
				<span class="frame topnav <?php if (stripos($title_for_layout, 'preview') !== false) echo 'selectednav'; ?>" id="navlinks">
				<?php
					echo $html->link('4. '.__('Preview/Save', true), '/documents/preview_save/'.$document['Document']['id']);
					echo $html->image($valid_document ? 'check.gif' : 'x.gif', array('class' => 'edit-link'));
				?>
				</span>

				<?php } ?>

				<?php if ($session->check('Message.flash')) { ?>
					<span class="required" style="font-size: 1em; text-align: center;"><?php $session->flash(); ?></span>
					<br/>
				<?php } ?>

				<!-- page content -->
				<?php echo $content_for_layout; ?>

			</div>
		</div>

		<div id="footer" class="frame">
			<span><?php echo $html->link(__('TOP', true), '#top'); ?></span>
			<span><?php echo $html->link('LEMON8-XML', 'http://pkp.sfu.ca/lemon8'); ?></span>

			<div style="display: inline; float: right; margin-top: -1.2em;">
				<a href="http://www.cakephp.org/" target="_new">
					<?php echo $html->image('cake.power.gif', array('alt'=>"CakePHP(tm) : Rapid Development Framework", 'style' => 'border: 0;'));?>
				</a>
			</div>
		</div>
	</div>

	<?php echo $cakeDebug?>
</body>
</html>
<?php
	/**
	 * edit.ctp
	 *
	 * Copyright (c) 2008-2009 MJ Suhonos
	 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
	 *
	**/

// TEMPORARY FOR AJAXY STUFF
error_reporting('E_NONE');
Configure::write('debug', 0);

	// define some arrays of values for various enumerated elements
	$article_genres = array('journal' => 'Journal Article',
							'book' => 'Book',
							'proceedings' => 'Conference Proceedings',
							'web' => 'Web Citation',
							'other' => 'Other');

	// remove duplicate values from different parsers and re-index
	foreach ($options as $element => $array) {
		$options2[$element] = array_unique($array);
	}
	$options = $options2;

	// mark citation to show parsed elements
	if (reset($options['marked'])) {
		// already marked by one of the citation parsers
		$marked_contents = preg_replace(array('/<([^\/][^>]+)>/', '/<\/[^>]+>/'), array('<span class="\1">', '</span>'), reset($options['marked']));
	} elseif (!empty($metadata)) {
		$marked_contents = $citation['Citation']['contents'];
		foreach ($metadata as $element => $data) {
			// if this element is in the citation, mark it
			// FIXME: this regex has some issues
			$test = preg_replace('/<([a-z]*)\b[^>]*>.*<\/\1>/', '', $marked_contents, 1);
			if (!empty($metadata[$element]['value']) && stripos($test, $metadata[$element]['value']) !== false) {
				$marked_contents = preg_replace('/\b'.preg_quote($metadata[$element]['value'], '/').'\b/i', '<span class="'.$element.'">'.$metadata[$element]['value'].'</span>', $marked_contents, 1);
			}
		}
	} else {
		$metadata = array();
		$marked_contents = null;
	}

	if (!isset($options)) $options = array();

	// helper function for pre-selection;
	function has_punc($array) {
		$test = array_filter($array, create_function('$o', 'return !preg_match("/[,;!\(\)\.\?]/", $o);'));
		if (1 == count($test)) return reset($test);
		else return false;
	}

	// combo box code to reduce duplication
	function combobox ($element, $metadata, $options, $form, $width = '81%') {
		$id = $metadata[$element]['id'];
		$value = $metadata[$element]['value'];

		// if we have choices for this element, display appropriately
		if (isset($options[$element])) {

			if (1 == count($options[$element]) && (empty($value) || strtolower($value) == strtolower(reset($options[$element])))) {
				// if there is only one option populate the text field
				echo $form->text('Metadata.'.$element.'_'.$id, array('class' => 'text', 'style' => 'width: '.$width, 'value' => reset($options[$element])));
			} else {
				// otherwise create a combobox
				echo $form->select('Metadata.'.$element.'_'.$id, array($value => $value) + array_combine(array_values($options[$element]), array_values($options[$element])), has_punc($options[$element]) ? has_punc($options[$element]) : $value, array('class' => 'comboBox', 'style' => 'border: 1px solid #ddd; padding: 3px; width: '.$width), false);
			}

			// identify that this field has options or has been changed
			echo ' <span style="color: #f00; text-decoration: blink;">*</span>';
		} else {
			// otherwise just populate the text field
			echo $form->text('Metadata.'.$element.'_'.$id, array('class' => 'text', 'style' => 'width: '.$width, 'value' => !empty($id) ? $value : null));
		}
	}
?>

<?php echo $form->create('Citation', array('action' => 'save/'.$citation['Citation']['id'], 'class' => 'editbibtex', 'name' => 'citations_'.$citation['Citation']['id']));?>
<?php echo $form->hidden('Citation.parse_score', array('value' => $citation['Citation']['parse_score'])); ?>
<?php echo $form->hidden('Citation.lookup_score', array('value' => $citation['Citation']['lookup_score'])); ?>
<?php echo $form->hidden('Citation.marked_contents', array('value' => $marked_contents, 'id' => 'input-marked-contents-'.$citation['Citation']['id'])); ?>

<div style="text-align:center; margin-left: auto; color: #FF0000;"><?php if (!empty($message)) echo $message; ?></div>

     <table class="editbibtex" id="citations" width="100%">
         <tbody>
         <tr class="genre">
             <td class="label">
                 <label for="genre">Genre: </label><br>
                 <span class="required">(required)</span>
             </td>
             <td class="field">
				<?php
				$id = $metadata['genre']['id'];
				$value = $metadata['genre']['value'];
				echo $form->select('Metadata.genre_'.$id, $article_genres, !empty($id) ? $value : (isset($options['genre']) ? reset($options['genre']) : 'other'), array('style' => 'vertical-align: 25%; border: 1px solid #ddd; padding: 3px;', 'onchange' => 'updateFields(this);'), null); ?>
				<div class="field_description"></div>
             </td>
         </tr>

         <tr class="authors journal book proceedings web other">
             <td class="label">
                 <label for="authors">Author(s): </label><br>
                 <span class="required">(required)</span>
             </td>

             <td class="field">
             	<?php combobox('authors', $metadata, $options, $form); ?>
                <div class="field_description">The author(s) of the work; required unless there are editors or an institution.</div>
             </td>
         </tr>

         <tr class="editor journal book proceedings other">
<!-- TODO: refactor into authors -->
             <td class="label"><label for="book">Editors: </label></td>
             <td class="field">
				 <?php combobox('editor', $metadata, $options, $form); ?>
                 <div class="field_description">The editor(s) of the book or proceedings</div>
             </td>
         </tr>

         <tr class="atitle journal book proceedings other">
             <td class="label">
                 <label for="atitle">Title: </label><br>
                 <span class="required">(required)</span>
             </td>
             <td class="field">
				 <?php combobox('atitle', $metadata, $options, $form); ?>
                 <div class="field_description">The title of the article, chapter, or conference presentation.</div>
             </td>
         </tr>

         <tr class="title journal book proceedings web other">
             <td class="label">
             	<label for="title">Source Title: </label><br/>
                <span class="required">(required)</span>
             </td>
             <td class="field">
				 <?php combobox('title', $metadata, $options, $form); ?>
                 <div class="field_description">The title of the journal, book, proceedings, or website. Please write the <strong>full name</strong>.</div>
             </td>
         </tr>

         <tr class="edition book other">
             <td class="label"><label for="edition">Edition: </label></td>
             <td class="field">
				 <?php combobox('edition', $metadata, $options, $form, '21%'); ?>
                 <div class="field_description">The edition of the book. (e.g. 2nd, 3rd)</div>
             </td>
         </tr>

         <tr class="year journal book proceedings web other">
             <td class="label">
                 <label for="year">Year: </label><br>
                 <span class="required">(required)</span>
             </td>
             <td class="field">
				 <?php combobox('year', $metadata, $options, $form, '21%'); ?>
                <div class="field_description">The year of the work. If forthcoming, try to predict the year of publication.</div>
             </td>
         </tr>

         <tr class="date journal proceedings other">
<!-- TODO: refactor into year -->
             <td class="label"><label for="date">Date: </label></td>
             <td class="field">
				 <?php combobox('date', $metadata, $options, $form, '21%'); ?>
                 <div class="field_description">The full date the conference took place or the work was published.</div>
             </td>
         </tr>

         <tr class="pages journal book proceedings other">
             <td class="label"><label for="pages">Pages: </label></td>
             <td class="field">
				 <?php combobox('spage', $metadata, $options, $form, '21%'); ?>
             	-
				 <?php combobox('epage', $metadata, $options, $form, '21%'); ?>
                 <div class="field_description">The page numbers of the work.</div>
             </td>
         </tr>

         <tr class="volume journal other">
             <td class="label"><label for="volume">Volume: </label><br>
                 <span class="required">(required)</span></td>
             <td class="field">
				 <?php combobox('volume', $metadata, $options, $form, '21%'); ?>
                 <div class="field_description">The volume of the journal in which the article was published.</div>
             </td>
         </tr>

         <tr class="issue journal other">
             <td class="label"><label for="issue">Issue: </label><br>
                 <span class="required">(required)</span></td>
             <td class="field">
				 <?php combobox('issue', $metadata, $options, $form, '21%'); ?>
                 <div class="field_description">The issue of the journal in which the article was published.</div>
             </td>
         </tr>

         <tr class="publoc book proceedings web other">
             <td class="label"><label for="publoc">Location: </label></td>
             <td class="field">
				 <?php combobox('publoc', $metadata, $options, $form); ?>
                 <div class="field_description">The location (e.g. city) where the conference or event took place or where the work was published.</div>
             </td>
         </tr>

         <tr class="publisher book proceedings web other">
             <td class="label"><label for="publisher">Publisher: </label></td>
             <td class="field">
				 <?php combobox('publisher', $metadata, $options, $form); ?>
                 <div class="field_description">The publisher of the work.</div>
             </td>
         </tr>

         <tr class="sponsor other">
             <td class="label"><label for="sponsor">Sponsor: </label></td>
             <td class="field">
				 <?php combobox('sponsor', $metadata, $options, $form); ?>
                 <div class="field_description">The institution that sponsored the thesis or technical report.</div>
             </td>
         </tr>

         <tr class="artnum other">
             <td class="label"><label for="artnum">Number: </label></td>
             <td class="field">
				 <?php combobox('artnum', $metadata, $options, $form, '21%'); ?>
                 <div class="field_description">If the report belongs to a series, its number in that series.</div>
             </td>
         </tr>

         <tr class="isbn book other">
             <td class="label"><label for="isbn">ISBN/ISSN: </label></td>
             <td class="field">
				 <?php combobox('isbn', $metadata, $options, $form, '21%'); ?>
                 <div class="field_description">The ISBN or ISSN of this work.</div>
             </td>
         </tr>

         <tr class="pmid journal">
             <td class="label"><label for="pmid">PMID: </label></td>
             <td class="field">
				 <?php combobox('pmid', $metadata, $options, $form, '21%'); ?>
                 <div class="field_description">The PubMed (MEDLINE) ID of this work.</div>
             </td>
         </tr>

         <tr class="doi journal book proceedings web other">
             <td class="label"><label for="doi">DOI: </label></td>
             <td class="field">
				 <?php combobox('doi', $metadata, $options, $form); ?>
                 <div class="field_description">The Digital Object Identifier of this work.</div>
             </td>
         </tr>

         <tr class="targetURL journal book proceedings web other">
             <td class="label"><label for="targetURL">Web page: </label></td>
             <td class="field">
				 <?php combobox('targetURL', $metadata, $options, $form); ?>
                 <div class="field_description">A web page where this work is available, if any.</div>
             </td>
         </tr>

         <tr class="access-date web">
             <td class="label"><label for="access-date">Access date: </label></td>
             <td class="field">
				 <?php combobox('access-date', $metadata, $options, $form, '21%'); ?>
                 <div class="field_description">The date the web page was accessed for reference.</div>
             </td>
         </tr>

         <tr class="comment journal book proceedings web other">
             <td class="label"><label for="comment">Comment: </label></td>
             <td class="field">
				 <?php combobox('comment', $metadata, $options, $form); ?>
                 <div class="field_description">Any additional comments in the citation.</div>
             </td>
         </tr>

         <tr class="buttons">
             <td class="label"></td>
             <td class="field">
        		<?php echo $ajax->submit('Save Details', array('url' => 'save/'.$citation['Citation']['id'],
					'complete' => "$('marked-contents-".$citation['Citation']['id']."').replace('<span id=\"marked-contents-\"".$citation['Citation']['id'].">' + $('input-marked-contents-".$citation['Citation']['id']."').value + '</span>');
					Effect.ScrollTo('citation-".$citation['Citation']['id']."', {offset: -50});"));
        		?>
             </td>
         </tr>
         </tbody>
		</table>

		<?php echo $form->end(); ?>

		<script type="text/javascript">
		// the naming on this genre field is an example of Cakes JS woes
		updateFields(document.citations_<?php echo $citation['Citation']['id']; ?>.elements['data[Metadata][genre_<?php echo $metadata['genre']['id']; ?>]']);
		setCombobox(true);
		</script>
<?php
	/**
	 * lookup.ctp
	 *
	 * Copyright (c) 2008-2009 MJ Suhonos
	 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
	 *
	**/

// TEMPORARY FOR AJAXY STUFF
error_reporting('E_NONE');
Configure::write('debug', 0);
?>
	<table>
		<tr>
			<td style="padding: 0px; width: 90px;">
			<?php
				if ($citation['Citation']['lookup_score'] >= $session->read('Settings.lookup_margin')) echo 'Looked up.';
				elseif ($citation['Citation']['parse_score'] >= $session->read('Settings.parse_margin')) echo 'Parsed.';
				else echo 'Not parsed.';
			?>&nbsp;</td>
			<td style="width: 75px;">
				<?php echo $this->renderElement('citations_edit_status', array('citation' => $citation)); ?>
			</td>
		</tr>
	</table>
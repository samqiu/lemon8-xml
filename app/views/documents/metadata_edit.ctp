<?php
	/**
	 * metadata_edit.ctp
	 *
	 * Copyright (c) 2008-2009 MJ Suhonos
	 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
	 *
	**/

	// create id-element and id-value arrays
	$elements = array_combine(Set::extract($metadata, '{n}.Metadata.id'), Set::extract($metadata, '{n}.Metadata.element'));
	$values = array_combine(Set::extract($metadata, '{n}.Metadata.id'), Set::extract($metadata, '{n}.Metadata.value'));

	// get keys for authors & affiliations
	$author_ids = array_keys($elements, 'author');
	$aff_ids = array_keys($elements, 'affiliation');

	// build array of author elements
	foreach ($metadata as $meta) {
		if (in_array($meta['Metadata']['id'], $author_ids)) $authors[$meta['Metadata']['id']] = $meta['children'];
	}

	// build array of affiliation elements
	foreach ($metadata as $meta) {
		if (in_array($meta['Metadata']['id'], $aff_ids)) $affiliations[$meta['Metadata']['id']] = $meta['children'];
	}

	// define some arrays of values for various enumerated elements
	$pub_id_type = array('publisher-id' => 'Publisher ID', 'pmid' => 'PubMed ID', 'doi' => "DOI");
	$article_types = array( 'abstract' => 'Abstract',
							'brief-report' => 'Brief Report',
							'article-commentary' => 'Commentary',
							'correction' => 'Correction',
							'editorial' => 'Editorial',
							'letter' => 'Letter',
							'research-article' => 'Original Research',
							'review-article' => 'Review',
							'other' => 'Other');

	echo $javascript->link('tiny_mce/tiny_mce_gzip');

	// TEMPORARY -- SUPPRESS ERROR REPORTING IN DEV
	$errorlevel = error_reporting(0);
?>
	<script language="javascript" type="text/javascript">
	tinyMCE_GZ.init({
		plugins : "paste, heading",
		themes : "advanced",
		languages : "en",
		disk_cache : true
	});
	</script>
	<script language="javascript" type="text/javascript">
	tinyMCE.init({
		plugins : "paste, heading",
		mode : "specific_textareas",
		textarea_trigger: "tinymce",
		language : "en",
		theme : "advanced",
		heading_clear_tag : "p",
		theme_advanced_buttons1 : "h1,h2,h3,h4,h5,h6,bold,italic,underline,bullist,numlist,link,unlink,separator,pasteword,help,code",
		theme_advanced_buttons2 : "",
		theme_advanced_buttons3 : ""
	});
	</script>

<br/><br/>

<div id="items" class="frame mainframe">

	<div class="editbibtex">
			<table class="editbibtex">
				<tbody>

		<?php echo $form->create('Documents', array('action' => 'metadata_save/'.$document['Document']['id'], 'class' => 'editbibtex', 'name' => 'editbibtex'));?>

         <tr class="article-title bibtex">
             <td class="label" nowrap>
                 <label for="article-title">Title: </label><br>
                 <span class="required">(required)</span>
             </td>
             <td class="field">
             	<?php
             	$key = array_search('article-title', $elements);
             	echo $form->text('Metadata.article-title_'.$key, array('class' => 'text', 'style' => 'width: 90%', 'value' => ($key) ? $values[$key] : null));
             	?>
				<div class="field_description">The full title of the article</div>
             </td>
         </tr>

		<?php
		// TODO: allow add/remove/reorder authors, ensure proper sequence
		$i = 1;
		foreach ($authors as $authorId => $author) {
			$author_elements = array_combine(Set::extract($author, '{n}.Metadata.id'), Set::extract($author, '{n}.Metadata.element'));
			$author_values = array_combine(Set::extract($author, '{n}.Metadata.id'), Set::extract($author, '{n}.Metadata.value'));
		?>

         <tr class="author">
             <td class="label" nowrap>
                 <label for="author">Author #<?php echo $i++; ?>:</label>
				 <br/>

             	<?php echo $html->link($html->image('up_arrow.png', array('style' => 'border: 1px solid #ccc; margin: 5px 0 5px 0;')), '/metadatas/moveup/'.$authorId, array(), false, false); ?>
             	<?php echo $html->link($html->image('down_arrow.png', array('style' => 'border: 1px solid #ccc; margin: 5px 0 5px 0;')), '/metadatas/movedown/'.$authorId, array(), false, false); ?>
				<?php echo $html->link($html->image('delete.png', array('style' => 'border: 1px solid #ccc; margin: 5px 0 5px 0;')), '/metadatas/delete/'.$authorId, array(), 'Are you sure you wish to delete this author?\n\nThis action cannot be undone.', false); ?>

             </td>
             <td class="field">

             	<?php
             	$key = array_search('forename', $author_elements);
             	echo $form->text('Metadata.author_'.$authorId.'_forename_'.$key, array('class' => 'text', 'style' => 'width: 30%', 'value' => ($key) ? $author_values[$key] : null));
             	?>
				<span class="field_description">The author's full first name, followed by any initials. (eg. Author AB)</span><span class="required"> *</span><br/>

             	<?php
             	$key = array_search('surname', $author_elements);
             	echo $form->text('Metadata.author_'.$authorId.'_surname_'.$key, array('class' => 'text', 'style' => 'width: 30%', 'value' => ($key) ? $author_values[$key] : null));
             	?>
				<span class="field_description">The author's full surname (eg. Authorington).</span><span class="required"> *</span><br/>

             	<?php
             	$key = array_search('degrees', $author_elements);
             	echo $form->text('Metadata.author_'.$authorId.'_degrees_'.$key, array('class' => 'text', 'style' => 'width: 30%', 'value' => ($key) ? $author_values[$key] : null));
             	?>
				<span class="field_description">A list of degrees for this author, separated by a semicolon.</span><br/>

             	<?php
             	$key = array_search('email', $author_elements);
             	echo $form->text('Metadata.author_'.$authorId.'_email_'.$key, array('class' => 'text', 'style' => 'width: 30%', 'value' => ($key) ? $author_values[$key] : null));
             	?>
				<span class="field_description">Contact email address for this author.</span><span class="required"> *</span><br/>

             	<?php
             	$key = array_search('phone', $author_elements);
             	echo $form->text('Metadata.author_'.$authorId.'_phone_'.$key, array('class' => 'text', 'style' => 'width: 30%', 'value' => ($key) ? $author_values[$key] : null));
             	?>
				<span class="field_description">Contact phone number for this author.</span><br/>

             	<?php
             	$key = array_search('fax', $author_elements);
             	echo $form->text('Metadata.author_'.$authorId.'_fax_'.$key, array('class' => 'text', 'style' => 'width: 30%', 'value' => ($key) ? $author_values[$key] : null));
             	?>
				<span class="field_description">Contact fax number for this author.</span><br/>

				<span class="field_description" style="margin-left: 35%; vertical-align: sub;">Select the affiliation(s) for this author.</span><span class="required"> *</span><br/>
				<?php
				$j = 1; $selected_affs = array();
				foreach ($affiliations as $affId => $affiliation) {
					$aff_elements = array_combine(Set::extract($affiliation, '{n}.Metadata.id'), Set::extract($affiliation, '{n}.Metadata.element'));
					$aff_values = array_combine(Set::extract($affiliation, '{n}.Metadata.id'), Set::extract($affiliation, '{n}.Metadata.value'));
					foreach (array_keys($author_elements, 'aff') as $key) $selected_affs[$key] = $author_values[$key];

					echo $form->checkbox('Metadata.author_'.$authorId.'_aff_'.array_search($affId, $selected_affs).'-'.$affId, array('checked' => in_array($affId, $selected_affs) ? 'true' : null ));
					echo "<span style='font-size: 0.9em; color: #336;'> ".$j++.'. "';
					echo preg_replace("/(.*)[\s](.*)$/", "$1", substr($aff_values[array_search('contents', $aff_elements)], 0, 30));
					echo ' ..."</span><br/>';
				}
				?>
				<br/>
             </td>
         </tr>

		<?php } ?>

         <tr class="bibtex">
             <td class="field"><input name="add_author" value="Add Author" type="submit" class="button" style="float: right; margin-right: 1em;"></td>
             <td class="field">
             <?php if (!count($authors)) { ?><span class="required">(at least one author is required)</span><?php } ?>
             </td>
         </tr>

		<?php
		$i = 1;
		foreach ($affiliations as $affId => $affiliation) {
			$aff_elements = array_combine(Set::extract($affiliation, '{n}.Metadata.id'), Set::extract($affiliation, '{n}.Metadata.element'));
			$aff_values = array_combine(Set::extract($affiliation, '{n}.Metadata.id'), Set::extract($affiliation, '{n}.Metadata.value'));
		?>

         <tr class="affiliation">
             <td class="label" nowrap>
                 <label for="affiliation">Affiliation #<?php echo $i++; ?>:</label>
				<br/>

             	<?php echo $html->link($html->image('up_arrow.png', array('style' => 'border: 1px solid #ccc; margin: 5px 0 5px 0;')), '/metadatas/moveup/'.$affId, array(), false, false); ?>
             	<?php echo $html->link($html->image('down_arrow.png', array('style' => 'border: 1px solid #ccc; margin: 5px 0 5px 0;')), '/metadatas/movedown/'.$affId, array(), false, false); ?>
				<?php echo $html->link($html->image('delete.png', array('style' => 'border: 1px solid #ccc; margin: 5px 0 5px 0;')), '/metadatas/delete/'.$affId, array(), 'Are you sure you wish to delete this affiliaton?\n\nThis action cannot be undone.', false); ?>

             </td>
             <td class="field">
				<?php
				$key = array_search('contents', $aff_elements);
				echo $form->textarea('Metadata.affiliation_'.$affId.'_contents_'.$key, array('class' => 'text',
									 'style' => 'font-size: 0.75em; font-family: Verdana, Arial, Sans-Serif; width: 50%; height: 6em;',
									 'value' => ($key) ? $aff_values[$key] : null));
				?>
				<span class="field_description" style="vertical-align: 400%;">Full address of the affiliation.</span><br/>

				<?php
				$key = array_search('country', $aff_elements);
				echo $countryList->select('Metadata.affiliation_'.$affId.'_country_'.$key, ' ',
											$aff_values[$key],
											array('style' => 'border: 1px solid #ddd; padding: 3px; width: 51%'));
				?>
				<span class="field_description">Country</span><span class="required"> *</span><br/>
				<br/>
             </td>
         </tr>

		<?php } ?>

         <tr class="bibtex">
             <td class="field"><input name="add_affiliation" value="Add Affiliation" type="submit" class="button" style="float: right; margin-right: 1em;"></td>
             <td class="field">
             <?php if (!count($affiliations)) { ?><span class="required">(at least one affiliation is required)</span><?php } ?>
             </td>
         </tr>

         <tr class="article-type">
             <td class="label" nowrap>
                 <label for="article-type">Article Type: </label><br>
                 <span class="required">(required)</span>
             </td>
             <td class="field">
				<?php
                $key = array_search('article-type', $elements);
				echo $form->select('Metadata.article-type_'.$key, $article_types, ($key) ? $values[$key] : null, array('style' => 'vertical-align: 25%; border: 1px solid #ddd; padding: 3px;'), null); ?>
                <div class="field_description">Indicates what kind of article this is, for example, a research article, a commentary, an editorial, etc.</div>
             </td>
         </tr>

         <tr class="keywords">
             <td class="label" nowrap>
                 <label for="keywords">Keywords: </label><br>
             </td>
             <td class="field">

             	<?php
				$key = array_search('keywords', $elements);
             	echo $form->text('Metadata.keywords_'.$key, array('class' => 'text', 'style' => 'width: 80%', 'value' => ($key) ? $values[$key] : null));
             	?>
                <div class="field_description">Choose terms that best describe the content of the article. Separate keywords with a semi-colon (keyword1; keyword2; keyword3).</div>
             </td>
         </tr>

         <tr class="article-id">
             <td class="label" nowrap>
                 <label for="article-id">Article ID: </label><br>
             </td>
             <td class="field">
             	 <?php
				$key = array_search('article-id', $elements);
				echo $form->text('Metadata.article-id_'.$key, array('class' => 'text', 'style' => 'width: 50%', 'value' => ($key) ? $values[$key] : null));
				?>

                 <?php
                 $key = array_search('pub-id-type', $elements);
                 echo $form->select('Metadata.pub-id-type_'.$key, $pub_id_type, ($key) ? $values[$key] : null, array('style' => 'border: 1px solid #ddd; padding: 3px;', 'value' => ($key) ? $values[$key] : null));
                 ?>
                 <div class="field_description">Unique ID to locate or identify this article in a cataloguing or indexing system.</div>
             </td>
         </tr>

         <tr class="abstract">
             <td class="label" nowrap>
                 <label for="abstract">Abstract: </label><br>
             </td>
             <td class="field">
             	<div style="width: 100%";>
				<?php
                 $key = array_search('abstract', $elements);
                 echo $form->textarea('Metadata.abstract_'.$key, array('tinymce' => 'true', 'rows' => '20', 'value' => ($key) ? $values[$key] : null));
                 ?>
                </div>
             </td>
         </tr>

         <tr class="conflicts">
             <td class="label" nowrap>
                 <label for="conflicts">Conflicts: </label><br>
             </td>
             <td class="field">
             	<div style="width: 100%";>
				<?php
                 $key = array_search('conflicts', $elements);
                 echo $form->textarea('Metadata.conflicts_'.$key, array('tinymce' => 'true', 'rows' => '10', 'value' => ($key) ? $values[$key] : null));
                 ?>
                </div>
             </td>
         </tr>

         <tr class="buttons">
             <td class="label" nowrap></td>
             <td class="field">
                 <input name="submit" value="Save/Update" type="submit" class="button">
             </td>
         </tr>
		<?php echo $form->end(); ?>
			</tbody>
		</table>
	</div>

</div>

<?php
	// TEMPORARY -- SUPPRESS ERROR REPORTING IN DEV
	error_reporting($errorlevel);
?>
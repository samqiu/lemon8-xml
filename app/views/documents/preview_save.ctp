<?php
	/**
	 * preview_save.ctp
	 *
	 * Copyright (c) 2008-2009 MJ Suhonos
	 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
	 *
	**/
?>

<br/><br/>

<div id="items" class="frame mainframe">

	<div style="background: url(<?php echo $html->url('/img/25black.png'); ?>) repeat;">

	<div style="position: relative; padding: 10px; top: -3px; left: -3px; background: #FFF; border: 1px solid #aaa">

	<?php echo $form->create('Documents', array('action' => 'export/'.$document['Document']['id'])); ?>

	<p style="margin: 0 0 2em 0.5em; text-align: center;">
		<span class="button">
		<?php echo $html->image('pdfdoc.gif'); ?>
		<?php echo $html->link('Preview PDF', '/documents/previewPDF/'.$document['Document']['id']); ?>
		</span>
		&nbsp;
		<span class="button">
		<?php echo $html->image('xmldoc.gif'); ?>
		<input name="export" value="Export As:"  class="button" type="submit" style="color: #009; font-size: 1em; border: 0; padding: 1px;">
		<?php echo $form->select('schema', array('nlm' => 'NLM-XML'), null, array('style' => 'vertical-align: 15%; border: 1px solid #ddd; padding: 2px;'), 'Lemon8-XML'); ?>
		</span>
	</p>

	<?php echo $form->end(); ?>

	<?php echo $previewHTML; ?>
		</div>
	</div>

</div>
<?php
	/**
	 * parse.ctp
	 *
	 * Copyright (c) 2008-2009 MJ Suhonos
	 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
	 *
	**/
?>

	<script language = "Javascript">
	<!--
	function switchMenu(obj) {
		var el = document.getElementById(obj);
		if ( el.style.display != 'none' ) {
			el.style.display = 'none';
		} else {
			el.style.display = '';
		}
	}

	function remoteCall() {
	<?php
		foreach ($citations as $id) {
			echo $ajax->remoteFunction(array('url' => array( 'controller' => 'citations', 'action' => 'parse', $id . '/1' ), 'update' => $id, 'type' => 'synchronous')).";\n";
		}
	?>
	}

	-->
	</script>


<br/><br/>

<div id="items" class="frame mainframe">

	<div class="editbibtex">
			<table class="editbibtex"  width="100%">
				<tbody>

<?php
if ($session->read('Settings.debug')) {
	// display a bunch of debugging info when it's being used for such

	foreach ($results as $name => $object) {
		echo '<h2 style="display: inline;">'.$name.'</h2> ';
		echo '<span style="background-color: #ddd">[<a style="color: #DC143C;" onclick="switchMenu(\''.$name.'\');">+/-</a>]</span>';
		echo '<div id="'.$name.'" style="display: none;">';

		foreach ($object as $key => $value) {
			echo $key.' => '.nl2br(strtr(htmlspecialchars(print_r($value, true)), array(' ' => '&nbsp;'))).'<br/>';
		}

		echo '</div><br/>';
	}
}
?>
		<tr class="bibtex" >
			<td colspan="2"><h2>&nbsp;&nbsp;Parse Results</h2><br/></td>
		</tr>
         <tr>
             <td class="label"  nowrap>Parse Score:</td>
             <td style="color: <?php echo (round($results['statistics']['parse_score']) >= $session->read('Settings.doc_parse_margin')) ? '#43B823' : '#DC143C' ?>">
             	<?php echo round($results['statistics']['parse_score']); ?>%</td>
         </tr>
         <tr>
             <td class="label"  nowrap>Content Statistics:</td>
             <td>
             	<?php echo round($results['statistics']['para_count']); ?> paragraphs;
             	<?php echo round($results['statistics']['word_count']); ?> words;
             	<?php echo round($results['statistics']['char_count']); ?> characters
             </td>
         </tr>
         <tr>
             <td class="label"  nowrap>Metadata Elements:</td>
             <td><?php echo rcount($results['metadata']); ?></td>
         </tr>
         <tr>
             <td class="label" >Sections:</td>
             <td>
             	<?php echo count($results['sections']); ?>
             	(<?php
					$levels = array_count_values(Set::extract($results['sections'], '{n}.level'));
					$output = '';
					foreach ($levels as $level => $count) $output .= $count.'xH'.$level.'; ';
					echo trim_punc($output);
				?>)
             </td>
         </tr>
         <tr>
             <td class="label" >Figures:</td>
             <td><?php echo $results['statistics']['table_count']; ?></td>
         </tr>
         <tr>
             <td class="label" >Tables:</td>
             <td><?php echo $results['statistics']['figure_count']; ?></td>
         </tr>
         <tr>
             <td class="label" >Citations:</td>
             <td>
             	<div id="citations_count">
             		<?php echo count($citations); ?> Citations
					<a onclick="switchMenu('citations_count'); switchMenu('citations_process'); remoteCall();" class="button">Process (may take a while)</a>
             	</div>

				<table id="citations_process" style="display: none;">
					<?php foreach (Set::extract($citations, '{n}.Citation.id') as $key => $id) { ?>
						<tr style="height: 2em;">
							<td><?php echo ($key + 1); ?>.</td>
							<td <?php echo 'id="'.$id.'"';?> valign="middle">Parsing...</td>
						</tr>
					<?php } ?>
				</table>
             </td>
         </tr>
         <tr>
             <td class="label" ></td>
             <td id="citations_process"><!--  AJAX RESULTS GO HERE --></td>
         </tr>

         <tr class="buttons">
             <td class="label"  style="background-color: #FFBFBF;" >
             	<center>
				<?php echo $form->create('Documents', array('action' => 'delete/'.$document['Document']['id'], 'class' => 'editbibtex', 'name' => 'editbibtex'));?>
                 <input name="submit" value="Discard" type="submit" class="button">
                 <?php echo $form->end(); ?>
             	</center>
             </td>
             <td class="field">
				<?php echo $form->create('Documents', array('action' => 'metadata_edit/'.$document['Document']['id'], 'class' => 'editbibtex', 'name' => 'editbibtex'));?>
                 <input name="submit" value="Continue" type="submit" class="button">
                 <?php echo $form->end(); ?>
             </td>
         </tr>

			</tbody>
		</table>
	</div>

</div>

<?php
	/**
	 * citations_edit.ctp
	 *
	 * Copyright (c) 2008-2009 MJ Suhonos
	 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
	 *
	**/
?>

<?php echo $javascript->link('combobox'); ?>

<script type="text/javascript">
function toggleCitationBlocks(action, num) {
	switch(action) {
		case 'edit':
			Element.hide('citation-content-'+num);
			Element.show('citation-edit-'+num);
			Effect.BlindDown('citation-form-'+num);
			break;
		case 'cancel':
			Effect.BlindUp('citation-form-'+num);
			Element.show('citation-content-'+num);
			Element.hide('citation-edit-'+num);
			break;

		case 'content':
			Effect.BlindDown('citation-content-'+num);
			Effect.BlindUp('citation-edit-'+num);
			Effect.BlindUp('citation-form-'+num);
			break;
	}
}
</script>

<br/><br/>

<div id="items" class="frame mainframe">
	<table class="editbibtex" style="width: 100%; border: 1px solid #ccc;">
		<tbody>

		<?php $i = 1; foreach ($citations as $citation) { ?>
		<?php $citationId = $citation['Citation']['id']; ?>

		<tr>
			<td valign="top" align="right" style="padding-top: 0.5em;">
				<strong><?php echo $i ?>. </strong>
			</td>
			<td valign="top" style="padding: 0.5em;">

				<div class="citation" id="citation-<?php echo $citationId?>">

					<div id="citation-content-<?php echo $citationId?>" position="absolute">
						<?php echo $ajax->link($html->image('plus.jpg', array('style' => 'vertical-align: sub; border: none;')),
							'/citations/edit/'.$citationId,
							array(	'update' => 'citation-form-'.$citationId,
									'complete' => 'toggleCitationBlocks(\'edit\', '. $citationId . ')'),
							null, false);
						?>
						<span id="marked-contents-<?php echo $citationId?>"><?php echo $citation['Citation']['marked_contents'] ? $citation['Citation']['marked_contents'] : $citation['Citation']['contents'];; ?></span>
					</div>

					<div id="citation-edit-<?php echo $citationId?>" position="absolute" style="display: none">
						<?php echo $html->link($html->image('minus.jpg', array('style' => 'border: none; vertical-align: top;')), 'javascript://', array('onclick' => 'toggleCitationBlocks(\'cancel\', '.$citationId.');'), false, false); ?>
						<textarea style="width: 95%; font-size: 1em; font-family: sans-serif;" name="citation-edit-textarea-<?php echo $citationId; ?>" id="citation-edit-textarea-<?php echo $citationId; ?>"><?php echo $citation['Citation']['contents']; ?></textarea>

						<!-- these are in-place AJAX actions -->
						<div style="margin: 10px 0px 10px 30px;" class="citation-actions" id="citation-actions-<?php echo $citationId; ?>"
				  		<?php
						echo $ajax->link('save citation text', '/citations/updateContent/'.$citationId,
								array(  //'update' => 'citation-content-'.$citationId,
										'complete' => 'toggleCitationBlocks(\'edit\', '.$citationId.')',
										'with' => '\'full_reference=\'+encodeURIComponent($F(\'citation-edit-textarea-'.$citationId.'\'))',
										'class' => 'button'
									));
						echo '&nbsp;';

						echo $ajax->link('parse citation', '/citations/parse/'.$citationId,
								array(	'update' => 'citation-form-'.$citationId,
										'complete' => 'toggleCitationBlocks(\'edit\', '. $citationId . ')',
										'with' => '\'full_reference=\'+encodeURIComponent($F(\'citation-edit-textarea-'.$citationId.'\'))',
										'class' => 'button'
									));
						echo '&nbsp;';

						echo $ajax->link('lookup citation', '/citations/lookup/'.$citationId,
								array(	'update' => 'citation-form-'.$citationId,
										'complete' => 'toggleCitationBlocks(\'edit\', '. $citationId . ')',
										'with' => '\'full_reference=\'+encodeURIComponent($F(\'citation-edit-textarea-'.$citationId.'\'))',
										'class' => 'button'
									));
						?>
						</div>
					</div>

					<div id="citation-form-<?php echo $citationId; ?>" style="display: none" position="absolute"></div>

				</div>

			</td>

			<td valign="top" nowrap="true" style="padding-top: 0.5em; padding-right: 0.5em;">
             	<?php
				echo $html->link($html->image('up_arrow.png', array('style' => 'border: 1px solid #ccc; margin-right: 3px;')), '/citations/moveup/'.$citation['Citation']['id'], array('style=' => 'vertical-align: sub;'), false, false);
				echo $html->link($html->image('down_arrow.png', array('style' => 'border: 1px solid #ccc;')), '/citations/movedown/'.$citation['Citation']['id'], array('style=' => 'vertical-align: sub;'), false, false);
				echo $html->link($html->image('delete.png', array('style' => 'border: 1px solid #ccc; margin-left: 3px;')), '/citations/remove/'.$citation['Citation']['id'], array('style=' => 'vertical-align: sub;'), 'Are you sure you wish to delete this citation?\n\nThis action cannot be undone.', false);

				echo $this->renderElement('citations_edit_status', array('citation' => $citation));
             	?>
			</td>
		</tr>

		<?php $i++; } ?>

		<tr id="new-citation">
			<td />
			<td align="center">
				<span class="button"><?php echo $html->link('Add New Citation', '/documents/citation_add/'.$document['Document']['id']); ?></span>
				<br/>&nbsp;
			</td>
		</tr>

		</tbody>
	</table>

</div>
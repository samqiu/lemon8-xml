<?php
	/**
	 * sections_edit.ctp
	 *
	 * Copyright (c) 2008-2009 MJ Suhonos
	 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
	 *
	**/
?>

<?php echo $javascript->link('folder-tree-static'); ?>

<br/><br/>

<div id="items" class="frame mainframe">
	<div class="editbibtex">

		<?php echo $form->create('Documents', array('action' => 'sections_save/'.$document['Document']['id'], 'class' => 'editbibtex', 'name' => 'editbibtex', 'type' => 'file'));?>

	<p style="text-align: center;">
		<span class="button">
		<a href="#" onclick="expandAll('dhtmlgoodies_tree');return false">Expand all</a>
		</span>
		&nbsp;
		<span class="button">
		<a href="#" onclick="collapseAll('dhtmlgoodies_tree');return false">Collapse all</a>
		</span>
	</p>

	<ul id="dhtmlgoodies_tree" class="dhtmlgoodies_tree">
		<?php foreach ($sections as $section) drawNode($section, $html, $form); ?>
	</ul>

		<table class="editbibtex" style="width: 100%;">
			<tbody>
         	<tr class="buttons">
            	 <td class="label" nowrap/>
	             <td class="field"><input name="submit" value="Save/Update" type="submit" class="button"></td>
			</tr>
			</tbody>
		</table>

		<?php echo $form->end(); ?>

	</div>
</div>

<?php	function drawNode($section, $html, $form) 	{ ?>

	<li class="<?php echo $section['Section']['type']; ?>.png">
		<a href="#" id="node_<?php echo $section['Section']['id']; ?>"></a>

			<?php echo ucfirst($section['Section']['type']); ?>

			<?php if ($section['Section']['type'] == 'section') {
				$num_words = str_word_count(strip_tags($section['Section']['contents']));
				if ($num_words) echo ' <span style="font-size: small;">('.$num_words.' words)</span>';
			} ?>

			<?php echo $form->text('Section.'.$section['Section']['id'], array('class' => 'text', 'style' => 'margin-left: 1em; width: 45%', 'value' => $section['Section']['title'])); ?>
			<?php echo $html->link($html->image('up_arrow.png', array('style' => 'vertical-align: sub; border: 1px solid #ccc; margin-bottom: 2px;')), '/sections/moveup/'.$section['Section']['id'], array('style' => 'margin: 0px; padding: 0px;'), false, false); ?>
			<?php echo $html->link($html->image('down_arrow.png', array('style' => 'vertical-align: sub; border: 1px solid #ccc; margin-bottom: 2px;')), '/sections/movedown/'.$section['Section']['id'], array('style' => 'margin: 0px; padding: 0px;'), false, false); ?>
			<?php echo $html->link($html->image('delete.png', array('style' => 'vertical-align: sub; border: 1px solid #ccc; margin-bottom: 2px;')), '/sections/remove/'.$section['Section']['id'], array('style' => 'margin: 0px; padding: 0px;'), 'Are you sure you wish to delete this section?\n\nThis action cannot be undone.', false); ?>

			<?php if ($section['Section']['type'] == 'figure') { ?>
				<div style="margin-left: 75px;">
				<?php echo $form->file('Image.'.$section['Section']['id'], array('style' => 'width: 250px; margin-right: 1em;', 'class' => 'text')); ?>
				<input type="submit" value="Upload"  class="button" onclick="return confirm('Are you sure you wish to replace this image?\n\nThis action cannot be undone.')"/>
				<?php echo $html->image('../files/'.$section['Section']['contents'], array('style' => 'max-height: 100px; max-width: 100px; vertical-align: top; border: 1px solid #ccc;')); ?>
				</div>
			<?php } ?>

			<?php if ($section['children']) {
				echo '<ul>';
				foreach ($section['children'] as $childSection) drawNode($childSection, $html, $form);
				echo '</ul>';
			}
} ?>

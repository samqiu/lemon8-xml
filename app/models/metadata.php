<?php

/**
 * metadata.php
 *
 * Copyright (c) 2008-2009 MJ Suhonos
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Model for document and citation metadata
 */

class Metadata extends AppModel
{
    var $name = 'Metadata';
    var $belongsTo = array('Document', 'Citation');
    var $actsAs = array('Tree');

	// multi-level metadata binding
	// TODO: this might be applicable to sections as well, saving the lastInsertID approach
	function beforeSave() {

		// if we are saving an array, use a multilevel save
		if (isset($this->data['Metadata']['value']) && is_array($this->data['Metadata']['value'])) {

			// store the child array to save in a moment
			$childArray = $this->data['Metadata']['value'];
			$this->data['Metadata']['value'] = '';

			// save the parent row
			$this->save();

			// get the ID of the row we just inserted
			$id = $this->id;

			// loop through the child array and save those
			foreach ($childArray as $element => $value) {
					$this->create();

					$data['document_id'] = $this->Document->id;
					$data['element'] = $element;
					$data['value'] = $value;
					$data['parent_id'] = $id;

					$this->save($data);
			}
			return false;

		} else return true;
	}

}

?>
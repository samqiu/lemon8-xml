<?php

/**
 * permission.php
 *
 * Copyright (c) 2008-2009 MJ Suhonos
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 */

class Permission extends AppModel
{
    var $name = 'Permission';
    var $hasAndBelongsToMany = array('Group' =>
														 array('className' => 'Group',
                                              						'joinTable' => 'groups_permissions'));
} 

?>

<?php

/**
 * setting.php
 *
 * Copyright (c) 2008-2009 MJ Suhonos
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 */

class Setting extends AppModel
{
    var $name = 'Setting';
    var $belongsTo = array('User', 'Group');
}

?>

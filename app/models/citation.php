<?php

/**
 * citation.php
 *
 * Copyright (c) 2008-2009 MJ Suhonos
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Model for document citations
 */

class Citation extends AppModel
{
    var $name = 'Citation';
    var $belongsTo = 'Document';
    var $hasMany = 'Metadata';
    var $actsAs = array('Tree');
    
	// take an array of key->value metadata pairs and save them to the database
	function saveMetadata($metadata) {

		foreach ($metadata as $element => $value) {

			// metadata should come in the form element_ID when submitted through the GUI
			// FIXME: handle prefixing a bit better than this
			if (preg_match("/([^\d]+)_(\d+)?/", $element, $matches) && $matches[1] != 'diff' && $matches[1] != 'parse' && $matches[1] != 'lookup') {
				$element = $matches[1];
				$elementId = (isset($matches[2])) ? $matches[2] : null;
				$newMeta['id'] = $elementId;

			} else {
				// otherwise we're populating from the parser
				$newMeta['id'] = null;
			}

			$newMeta['citation_id'] = $this->id;
			$newMeta['parent_id'] = null;
			$newMeta['element'] = $element;
			$newMeta['value'] = $value;

			$this->Metadata->save($newMeta);
		}
	}


	// load metadata into an element-based array for a citation ID
	function loadMetadata($citationId) {
		$metadata_array = $this->Metadata->findAllByCitationId($citationId, array('id', 'element', 'value'), 'id');

		// if no metadata has been saved, return an empty array
		if (empty($metadata_array)) return array();
		$id_elements = array_combine(Set::extract($metadata_array, '{n}.Metadata.element'), Set::extract($metadata_array, '{n}.Metadata.id'));
		$id_values = array_combine(Set::extract($metadata_array, '{n}.Metadata.id'), Set::extract($metadata_array, '{n}.Metadata.value'));

		// map metadata cleanly into an array based on element name
		foreach ($id_elements as $element => $id) {
			$metadata[$element] = array('id' => $id, 'value' => $id_values[$id]);
		}

		return $metadata;
	}
	
}

?>
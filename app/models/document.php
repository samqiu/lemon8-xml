<?php

/**
 * document.php
 *
 * Copyright (c) 2008-2009 MJ Suhonos
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Model for documents
 */

class Document extends AppModel
{
    var $name = 'Document';
    var $belongsTo = 'User';
    var $hasMany = array('Metadata', 'Section', 'Citation');

    var $validate = array(
        'doc_content'  => VALID_NOT_EMPTY,
        'doc_styles'   => VALID_NOT_EMPTY,
        'doc_meta'     => VALID_NOT_EMPTY
    );

	// take an array of key->value metadata pairs and save them to the database
	function saveMetadata($metadata) {

		foreach ($metadata as $element => $value) {

			// metadata should come in the form element_ID_subelement_subID
			if (preg_match("/([^\d]+)_(\d+)?(_([^\d]+)_(\d+)?)?(-(\d+))?/", $element, $matches)) {
				$element = $matches[1];
				$elementId = (isset($matches[2])) ? $matches[2] : null;
				$subElement = (isset($matches[4])) ? $matches[4] : null;
				$subId = (isset($matches[5])) ? $matches[5] : null;
				$affId = (isset($matches[7])) ? $matches[7] : null;

				// create a metadata object to save
				$newMeta['document_id'] = $this->id;

				if ($subElement == 'aff') {	// special case for affiliation linking
					$newMeta['id'] = $subId;
					$newMeta['element'] = $subElement;
					$newMeta['parent_id'] = $elementId;
					if ($value) $newMeta['value'] = $affId;
					else $newMeta['value'] = '';

				} elseif ($subElement) {		// we are modifying a subelement
					$newMeta['id'] = $subId;
					$newMeta['element'] = $subElement;
					$newMeta['parent_id'] = $elementId;
					$newMeta['value'] = $value;

				} else {										// we are modifying a top element
					$newMeta['id'] = $elementId;
					$newMeta['element'] = $element;
					$newMeta['parent_id'] = null;
					$newMeta['value'] = $value;
				}

				if ($newMeta['id']) {
					$this->Metadata->save($newMeta);
				} else {
					// FIXME: not sure why this isn't auto-creating through Cake
					$this->Metadata->create($newMeta);
					$this->Metadata->save($newMeta);
				}
			}
		}
	}
}

?>
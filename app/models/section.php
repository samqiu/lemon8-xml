<?php

/**
 * section.php
 *
 * Copyright (c) 2008-2009 MJ Suhonos
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Model for document sections
 */

class Section extends AppModel
{
    var $name = 'Section';
    var $belongsTo = 'Document';
    var $actsAs = array('Tree');
}

?>
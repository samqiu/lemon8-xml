<?php

/**
 * group.php
 *
 * Copyright (c) 2008-2009 MJ Suhonos
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 */

class Group extends AppModel
{
    var $name = 'Group';
    var $hasMany = 'User';
    var $hasAndBelongsToMany = array('Permission' =>
                                      					 array('className' => 'Permission',
                                              						'joinTable' => 'groups_permissions'));
}

?>

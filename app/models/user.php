<?php

/**
 * user.php
 *
 * Copyright (c) 2008-2009 MJ Suhonos
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 */

class User extends AppModel
{
    var $name = 'User';
    var $belongsTo = 'Group';
    var $hasMany = array('Document', 'Setting');
}

?>
